DROP TABLE IF exists `got`.`article`, `got`.`attachment`, `got`.`badge`, `got`.`badgenorm`, `got`.`badgerequest`, `got`.`employee`, `got`.`leader`, `got`.`leader_group`, `got`.`loggeduser`, `got`.`mountaingroup`, `got`.`mountainsubgroup`, `got`.`nopointroute`, `got`.`normdecision`, `got`.`point`, `got`.`pointroute`, `got`.`tourist`, `got`.`touristbadge`, `got`.`trip`, `got`.`trip_nopointroute`, `got`.`trip_pointroute`, `got`.`tripdecision`;

CREATE TABLE Trip
(
    trip_id             int(10)                             NOT NULL AUTO_INCREMENT,
    decision_trip_id    int(10),
    norm_id             int(10)                             NOT NULL,
    is_leader_present   bit(1)    DEFAULT 0                 NOT NULL,
    name                varchar(255)                        NOT NULL,
    description         varchar(255),
    create_date         timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
    verification_status int(10)                             NOT NULL,
    tourist_id          int(10)                             NOT NULL,
    PRIMARY KEY (trip_id)
) CHARACTER SET UTF8;

CREATE TABLE PointRoute
(
    point_route_id int(10)          NOT NULL AUTO_INCREMENT,
    subgroup_id    int(10)          NOT NULL,
    points         int(10)          NOT NULL,
    length         float,
    height_diff    float,
    is_available   bit(1) DEFAULT 1 NOT NULL,
    description    varchar(255),
    employee_id    int(10)          NOT NULL,
    start_point    int(10)          NOT NULL,
    end_point      int(10)          NOT NULL,
    PRIMARY KEY (point_route_id)
) CHARACTER SET UTF8;

CREATE TABLE Tourist
(
    tourist_id  int(10)          NOT NULL AUTO_INCREMENT,
    is_disabled bit(1) DEFAULT 0 NOT NULL,
    user_id     int(10)          NOT NULL,
    age         int(10),
    PRIMARY KEY (tourist_id),
    CONSTRAINT min_age CHECK (age >= 5)
) CHARACTER SET UTF8;

CREATE TABLE Leader
(
    card_nr  int(10)      NOT NULL AUTO_INCREMENT,
    phone_nr varchar(255) NOT NULL,
    user_id  int(10)      NOT NULL,
    PRIMARY KEY (card_nr),
    CONSTRAINT uniq_phone UNIQUE (phone_nr)
) CHARACTER SET UTF8;

CREATE TABLE TouristBadge
(
    tourist_badge_id  int(10)                             NOT NULL AUTO_INCREMENT,
    badge_id          int(10)                             NOT NULL,
    request_id        int(10),
    attainment_date   date,
    start_attain_date timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
    tourist_id        int(10)                             NOT NULL,
    PRIMARY KEY (tourist_badge_id)
) CHARACTER SET UTF8;

CREATE TABLE MountainGroup
(
    group_id    int(10)      NOT NULL AUTO_INCREMENT,
    name        varchar(255) NOT NULL,
    description varchar(255),
    PRIMARY KEY (group_id)
) CHARACTER SET UTF8;

CREATE TABLE Attachment
(
    attachment_id int(10)      NOT NULL AUTO_INCREMENT,
    trip_id       int(10)      NOT NULL,
    filepath      varchar(255) NOT NULL,
    PRIMARY KEY (attachment_id)
) CHARACTER SET UTF8;

CREATE TABLE Employee
(
    employee_id int(10) NOT NULL AUTO_INCREMENT,
    user_id     int(10) NOT NULL,
    PRIMARY KEY (employee_id)
) CHARACTER SET UTF8;

CREATE TABLE BadgeNorm
(
    norm_id               int(10)           NOT NULL AUTO_INCREMENT,
    norm_decision_id      int(10),
    attainment_date       date,
    required_point_amount int(10),
    gained_points         int(10) DEFAULT 0 NOT NULL,
    is_verified           bit(1)  DEFAULT 0 NOT NULL,
    tourist_badge_id      int(10)           NOT NULL,
    PRIMARY KEY (norm_id)
) CHARACTER SET UTF8;

CREATE TABLE LoggedUser
(
    user_id     int(10)      NOT NULL AUTO_INCREMENT,
    login       varchar(255) NOT NULL,
    password    varchar(60)  NOT NULL,
    surname     varchar(50)  NOT NULL,
    name        varchar(50)  NOT NULL,
    birth_date  date         NOT NULL,
    street      varchar(100),
    home_nr     varchar(5),
    housing_nr  varchar(5),
    postal_code varchar(2555),
    city        varchar(255),
    PRIMARY KEY (user_id),
    CONSTRAINT uniq_password UNIQUE (password),
    CONSTRAINT uniq_login UNIQUE (login)
) CHARACTER SET UTF8;

CREATE TABLE Article
(
    article_id  int(10)                                                         NOT NULL AUTO_INCREMENT,
    title       varchar(255)                                                    NOT NULL,
    add_date    timestamp DEFAULT CURRENT_TIMESTAMP                             NOT NULL,
    text        varchar(1000)                                                   NOT NULL,
    keywords    varchar(255),
    update_date timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL,
    employee_id int(10)                                                         NOT NULL,
    PRIMARY KEY (article_id)
) CHARACTER SET UTF8;

CREATE TABLE MountainSubgroup
(
    subgroup_id int(10)      NOT NULL AUTO_INCREMENT,
    group_id    int(10)      NOT NULL,
    name        varchar(255) NOT NULL,
    PRIMARY KEY (subgroup_id)
) CHARACTER SET UTF8;

CREATE TABLE NormDecision
(
    norm_decision_id int(10)                             NOT NULL AUTO_INCREMENT,
    add_date         timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
    response_date    date,
    is_accepted      bit(1)    DEFAULT 0                 NOT NULL,
    justification    varchar(255),
    employee_id      int(10)                             NOT NULL,
    PRIMARY KEY (norm_decision_id)
) CHARACTER SET UTF8;

CREATE TABLE BadgeRequest
(
    request_id      int(10)                             NOT NULL AUTO_INCREMENT,
    send_date       timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
    acceptance_date date,
    tourist_id      int(10)                             NOT NULL,
    PRIMARY KEY (request_id)
) CHARACTER SET UTF8;

CREATE TABLE TripDecision
(
    trip_decision_id int(10)                             NOT NULL AUTO_INCREMENT,
    send_date        timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
    acceptance_date  date,
    is_accepted      bit(1)    DEFAULT 0                 NOT NULL,
    justification    varchar(255),
    card_nr          int(10),
    PRIMARY KEY (trip_decision_id)
) CHARACTER SET UTF8;

CREATE TABLE Badge
(
    badge_id                int(10)      NOT NULL AUTO_INCREMENT,
    type                    varchar(255) NOT NULL,
    degree                  varchar(255) NOT NULL,
    description             varchar(255),
    required_point_amount   int(10),
    additional_requirements varchar(255),
    PRIMARY KEY (badge_id)
) CHARACTER SET UTF8;

CREATE TABLE NopointRoute
(
    nopoint_route_id int(10) NOT NULL AUTO_INCREMENT,
    subgroup_id      int(10) NOT NULL,
    points           int(10),
    length           float   NOT NULL,
    height_diff      float,
    start_point      int(10) NOT NULL,
    end_point        int(10) NOT NULL,
    PRIMARY KEY (nopoint_route_id)
) CHARACTER SET UTF8;

CREATE TABLE Trip_PointRoute
(
    trip_id        int(10) NOT NULL,
    point_route_id int(10) NOT NULL,
    walk_date      date    NOT NULL,
    lp             int(10) NOT NULL,
    points         int(10),
    PRIMARY KEY (trip_id, point_route_id)
) CHARACTER SET UTF8;

CREATE TABLE Leader_Group
(
    group_id int(10) NOT NULL,
    card_nr  int(10) NOT NULL,
    PRIMARY KEY (group_id, card_nr)
) CHARACTER SET UTF8;

CREATE TABLE Trip_NopointRoute
(
    trip_id          int(10) NOT NULL,
    nopoint_route_id int(10) NOT NULL,
    walk_date        date    NOT NULL,
    lp               int(10) NOT NULL,
    PRIMARY KEY (trip_id,
                 nopoint_route_id)
) CHARACTER SET UTF8;

CREATE TABLE Point
(
    point_id    int(10)      NOT NULL AUTO_INCREMENT,
    name        varchar(255) NOT NULL,
    longitude   real         NOT NULL,
    latitude    real         NOT NULL,
    height      real,
    description varchar(255),
    CONSTRAINT point_id_pk PRIMARY KEY (point_id)
) CHARACTER SET UTF8;

ALTER TABLE Tourist
    ADD CONSTRAINT tourist_user_fk FOREIGN KEY (user_id) REFERENCES LoggedUser (user_id);
ALTER TABLE Leader
    ADD CONSTRAINT leader_user_fk FOREIGN KEY (user_id) REFERENCES LoggedUser (user_id);
ALTER TABLE Employee
    ADD CONSTRAINT employee_user_fk FOREIGN KEY (user_id) REFERENCES LoggedUser (user_id);
ALTER TABLE Trip_PointRoute
    ADD CONSTRAINT trippointroute_trip_fk FOREIGN KEY (trip_id) REFERENCES Trip (trip_id);
ALTER TABLE Trip_PointRoute
    ADD CONSTRAINT trippointroute_route_fk FOREIGN KEY (point_route_id) REFERENCES PointRoute (point_route_id);
ALTER TABLE Trip
    ADD CONSTRAINT trip_tourist_fk FOREIGN KEY (tourist_id) REFERENCES Tourist (tourist_id);
ALTER TABLE PointRoute
    ADD CONSTRAINT pointroute_subgroup_fk FOREIGN KEY (subgroup_id) REFERENCES MountainSubgroup (subgroup_id);
ALTER TABLE Leader_Group
    ADD CONSTRAINT leadergroup_leader_fk FOREIGN KEY (card_nr) REFERENCES Leader (card_nr);
ALTER TABLE Leader_Group
    ADD CONSTRAINT leadergroup_group_fk FOREIGN KEY (group_id) REFERENCES MountainGroup (group_id);
ALTER TABLE Attachment
    ADD CONSTRAINT attachment_trip_fk FOREIGN KEY (trip_id) REFERENCES Trip (trip_id);
ALTER TABLE TouristBadge
    ADD CONSTRAINT touristbadge_tourist_fk FOREIGN KEY (tourist_id) REFERENCES Tourist (tourist_id);
ALTER TABLE Trip
    ADD CONSTRAINT trip_norm_fk FOREIGN KEY (norm_id) REFERENCES BadgeNorm (norm_id);
ALTER TABLE Article
    ADD CONSTRAINT article_employee_fk FOREIGN KEY (employee_id) REFERENCES Employee (employee_id);
ALTER TABLE MountainSubgroup
    ADD CONSTRAINT subgroup_group_fk FOREIGN KEY (group_id) REFERENCES MountainGroup (group_id);
ALTER TABLE BadgeRequest
    ADD CONSTRAINT badgereq_tourist_fk FOREIGN KEY (tourist_id) REFERENCES Tourist (tourist_id);
ALTER TABLE TouristBadge
    ADD CONSTRAINT touristbadge_badgereq_fk FOREIGN KEY (request_id) REFERENCES BadgeRequest (request_id);
ALTER TABLE NormDecision
    ADD CONSTRAINT normdec_employee_fk FOREIGN KEY (employee_id) REFERENCES Employee (employee_id);
ALTER TABLE BadgeNorm
    ADD CONSTRAINT badgenorm_normdec_fk FOREIGN KEY (norm_decision_id) REFERENCES NormDecision (norm_decision_id);
ALTER TABLE TouristBadge
    ADD CONSTRAINT touristbadge_badge_fk FOREIGN KEY (badge_id) REFERENCES Badge (badge_id);
ALTER TABLE Trip_NopointRoute
    ADD CONSTRAINT tripnopointroute_trip_fk FOREIGN KEY (trip_id) REFERENCES Trip (trip_id);
ALTER TABLE Trip_NopointRoute
    ADD CONSTRAINT tripnopointroute_route_fk FOREIGN KEY (nopoint_route_id) REFERENCES NopointRoute (nopoint_route_id);
ALTER TABLE PointRoute
    ADD CONSTRAINT pointroute_employee_fk FOREIGN KEY (employee_id) REFERENCES Employee (employee_id);
ALTER TABLE TripDecision
    ADD CONSTRAINT tripdec_leader_fk FOREIGN KEY (card_nr) REFERENCES Leader (card_nr);
ALTER TABLE Trip
    ADD CONSTRAINT trip_tripdec_fk FOREIGN KEY (decision_trip_id) REFERENCES TripDecision (trip_decision_id);
ALTER TABLE NopointRoute
    ADD CONSTRAINT nopointroute_subgroup_fk FOREIGN KEY (subgroup_id) REFERENCES MountainSubgroup (subgroup_id);
ALTER TABLE NopointRoute
    ADD CONSTRAINT nopointroute_startpoint_fk FOREIGN KEY (start_point) REFERENCES Point (point_id);
ALTER TABLE NopointRoute
    ADD CONSTRAINT nopointroute_endpoint_fk FOREIGN KEY (end_point) REFERENCES Point (point_id);
ALTER TABLE PointRoute
    ADD CONSTRAINT pointroute_startpoint_fk FOREIGN KEY (start_point) REFERENCES Point (point_id);
ALTER TABLE PointRoute
    ADD CONSTRAINT pointroute_endpoint_fk FOREIGN KEY (end_point) REFERENCES Point (point_id);
ALTER TABLE BadgeNorm
    ADD CONSTRAINT badgenorm_touristbadge_fk FOREIGN KEY (tourist_badge_id) REFERENCES TouristBadge (tourist_badge_id);

INSERT INTO LoggedUser (user_id, login, password, surname, name, birth_date, street, home_nr, housing_nr,
                        postal_code, city)
VALUES (1, 'andrzej_nowak', '$2a$10$EblZqNptyYvcLm/VwDCVAuBjzZOI7khzdyGPBr08PpIi0na624b8.', 'Nowak', 'Andrzej',
        '1980-06-12', 'Kamieńskiego', '219a', '6', '51-126', 'Wrocław');
INSERT INTO LoggedUser (user_id, login, password, surname, name, birth_date, street, home_nr, housing_nr,
                        postal_code, city)
VALUES (2, 'jolanta_polanska', '$2a$10$teJrCEnsxNT49ZpXU7n22O27aCGbVYYe/RG6/XxdWPJbOLZubLIi2', 'Polańska', 'Jolanta',
        '1995-03-02', 'Czekoladowa', '23', '4', '47-400', 'Racibórz');
INSERT INTO LoggedUser (user_id, login, password, surname, name, birth_date, street, home_nr, housing_nr,
                        postal_code, city)
VALUES (3, 'jan_987', '$2a$10$W9oRWeFmOT0bByL5fmAceucetmEYFg2yzq3e50mcu.CO7rUDb/poG', 'Kowalski', 'Jan', '1998-09-24',
        null, null, null, null, null);
INSERT INTO LoggedUser (user_id, login, password, surname, name, birth_date, street, home_nr, housing_nr,
                        postal_code, city)
VALUES (4, 'michalek_236', '$2a$10$GYCkBzp2Nlp, pointsGS/qjp5f6NOWHeF56ENAlHNuSssSJpE1MM', 'Opolony', 'Michał',
        '1988-03-27', 'Długa', '50', null, '47-400', 'Racibórz');
INSERT INTO LoggedUser (user_id, login, password, surname, name, birth_date, street, home_nr, housing_nr,
                        postal_code, city)
VALUES (5, 'marysia_brzezinska', '$2a$10$5uKS72xK2ArGDgb2CwjYnOzQcOmB7CPxK6fz2MGcDBM9vJ4rUql36', 'Brzezińska', 'Maria',
        '1993-12-30', 'Kolorowa', '32', null, '90-008', 'Łódź');
INSERT INTO LoggedUser (user_id, login, password, surname, name, birth_date, street, home_nr, housing_nr,
                        postal_code, city)
VALUES (6, 'kamil_plich', '$2a$10$6TajU85/gVrGUm5fv5Z8beVF37rlENohyLk3BEpZJFi6Av9JNkw9O', 'Plich', 'Kamil',
        '1992-09-15', 'Piłsudzkiego', '12', '4', '97-200', 'Tomaszów Mazowiecki');
INSERT INTO MountainGroup (group_id, name, description)
VALUES (1, 'Tatry i Podtatrze', null);
INSERT INTO MountainGroup (group_id, name, description)
VALUES (2, 'Beskidy Zachodnie', null);
INSERT INTO MountainGroup (group_id, name, description)
VALUES (3, 'Beskidy Wschodnie', null);
INSERT INTO MountainGroup (group_id, name, description)
VALUES (4, 'Góry Świętokrzyskie', null);
INSERT INTO MountainGroup (group_id, name, description)
VALUES (5, 'Sudety', null);
INSERT INTO Leader (card_nr, phone_nr, user_id)
VALUES (123456, '698563012', 1);
INSERT INTO Leader (card_nr, phone_nr, user_id)
VALUES (123467, '712345976', 5);
INSERT INTO Tourist (tourist_id, is_disabled, user_id)
VALUES (1, false, 1);
INSERT INTO Tourist (tourist_id, is_disabled, user_id)
VALUES (2, true, 3);
INSERT INTO Tourist (tourist_id, is_disabled, user_id)
VALUES (3, false, 2);
INSERT INTO Tourist (tourist_id, is_disabled, user_id)
VALUES (4, false, 5);
INSERT INTO Employee (employee_id, user_id)
VALUES (1, 4);
INSERT INTO Employee (employee_id, user_id)
VALUES (2, 6);
INSERT INTO MountainSubgroup (subgroup_id, group_id, name)
VALUES (1, 2, 'Beskid Śląski');
INSERT INTO MountainSubgroup (subgroup_id, group_id, name)
VALUES (2, 2, 'Beskid Żywiecki');
INSERT INTO MountainSubgroup (subgroup_id, group_id, name)
VALUES (3, 2, 'Beskid Mały');
INSERT INTO MountainSubgroup (subgroup_id, group_id, name)
VALUES (4, 2, 'Beskid Średni');
INSERT INTO MountainSubgroup (subgroup_id, group_id, name)
VALUES (5, 2, 'Gorce');
INSERT INTO MountainSubgroup (subgroup_id, group_id, name)
VALUES (6, 2, 'Beskid Wyspowy');
INSERT INTO MountainSubgroup (subgroup_id, group_id, name)
VALUES (7, 2, 'Orawa');
INSERT INTO MountainSubgroup (subgroup_id, group_id, name)
VALUES (8, 2, 'Spisz i Pieniny');
INSERT INTO MountainSubgroup (subgroup_id, group_id, name)
VALUES (9, 2, 'Beskid Sądecki');
INSERT INTO MountainSubgroup (subgroup_id, group_id, name)
VALUES (10, 2, 'Pogórze Wielickie');
INSERT INTO BadgeRequest (request_id, send_date, acceptance_date, tourist_id)
VALUES (1, '2015-09-24 00:00:00', '2015-09-25', 1);
INSERT INTO BadgeRequest (request_id, send_date, acceptance_date, tourist_id)
VALUES (2, '2016-06-20 00:00:00', '2016-06-24', 1);
INSERT INTO NormDecision (norm_decision_id, add_date, response_date, is_accepted, justification, employee_id)
VALUES (1, '2015-09-20 00:00:00', '2015-09-23', true, null, 1);
INSERT INTO NormDecision (norm_decision_id, add_date, response_date, is_accepted, justification, employee_id)
VALUES (2, '2016-06-18 00:00:00', '2016-06-20', true, null, 2);
INSERT INTO NormDecision (norm_decision_id, add_date, response_date, is_accepted, justification, employee_id)
VALUES (3, '2013-09-27 00:00:00', '2013-09-30', true, null, 1);
INSERT INTO NormDecision (norm_decision_id, add_date, response_date, is_accepted, justification, employee_id)
VALUES (4, '2015-01-02 00:00:00', '2015-01-05', true, null, 2);
INSERT INTO NormDecision (norm_decision_id, add_date, response_date, is_accepted, justification, employee_id)
VALUES (5, '2015-11-25 00:00:00', '2015-11-29', true, null, 1);
INSERT INTO NormDecision (norm_decision_id, add_date, response_date, is_accepted, justification, employee_id)
VALUES (6, '2016-05-12 00:00:00', '2016-05-16', false, null, 2);
INSERT INTO NormDecision (norm_decision_id, add_date, response_date, is_accepted, justification, employee_id)
VALUES (7, '2016-10-23 00:00:00', '2016-10-29', true, null, 1);
INSERT INTO NormDecision (norm_decision_id, add_date, response_date, is_accepted, justification, employee_id)
VALUES (8, '2019-12-30 23:58:49', null, false, null, 2);
INSERT INTO NormDecision (norm_decision_id, add_date, response_date, is_accepted, justification, employee_id)
VALUES (9, '2019-12-31 12:15:52', null, false, null, 2);
INSERT INTO Badge (badge_id, type, degree, description, required_point_amount, additional_requirements)
VALUES (1, '"w góry"', 'brązowy', null, 15, 'można zdobywać w wieku 5-7 lat');
INSERT INTO Badge (badge_id, type, degree, description, required_point_amount, additional_requirements)
VALUES (2, '"w góry"', 'srebrny', null, 30, 'można zdobywać w wieku 5-7 lat');
INSERT INTO Badge (badge_id, type, degree, description, required_point_amount, additional_requirements)
VALUES (3, '"w góry"', 'złoty', null, 45, 'można zdobywać w wieku 5-7 lat');
INSERT INTO Badge (badge_id, type, degree, description, required_point_amount, additional_requirements)
VALUES (4, 'popularna', '-', null, 60, null);
INSERT INTO Badge (badge_id, type, degree, description, required_point_amount, additional_requirements)
VALUES (5, 'mała', 'brązowa', null, 120, null);
INSERT INTO Badge (badge_id, type, degree, description, required_point_amount, additional_requirements)
VALUES (6, 'mała', 'srebrna', null, 340, '');
INSERT INTO Badge (badge_id, type, degree, description, required_point_amount, additional_requirements)
VALUES (7, 'mała', 'złota', null, 720, '');
INSERT INTO Badge (badge_id, type, degree, description, required_point_amount, additional_requirements)
VALUES (8, 'duża', 'brązowa', null, 50, '2 regulaminowe wycieczki na dwóch terenach górskich I-IV');
INSERT INTO Badge (badge_id, type, degree, description, required_point_amount, additional_requirements)
VALUES (9, 'duża', 'srebrna', null, 100,
        '2 regulaminowe wycieczki na dwóch terenach I-IV, punkty w Górach Świętokrzyskich');
INSERT INTO Badge (badge_id, type, degree, description, required_point_amount, additional_requirements)
VALUES (10, 'duża', 'złota', null, 150, '2 regulaminowy wycieczki na terenach I-IV, punkty w Tatrach i Podtatrzu');
INSERT INTO Badge (badge_id, type, degree, description, required_point_amount, additional_requirements)
VALUES (11, '"za wytrwałość"', 'mała', null, 7200,
        'posiadanie małej lub dużej złotej  zdobycie siedmiokrotnej normy na dużą złotą lub dziesięciokrotnej normy na małą złotą');
INSERT INTO Badge (badge_id, type, degree, description, required_point_amount, additional_requirements)
VALUES (12, '"za wytrwałość"', 'duża', null, 8440,
        'posiadanie "za wytrwałość" w stopniu małym, zdobycie wszystkich norm na odznaki po kolei od popularnej począwszy do małej lub dużej złotej, następnie zdobyć "za wytrwałość" w stopniu małym');
INSERT INTO TouristBadge (tourist_badge_id, badge_id, request_id, attainment_date, start_attain_date, tourist_id)
VALUES (1, 4, 1, '2015-09-23', '2015-03-24 00:00:00', 1);
INSERT INTO TouristBadge (tourist_badge_id, badge_id, request_id, attainment_date, start_attain_date, tourist_id)
VALUES (2, 5, 2, '2016-06-20', '2016-01-12 00:00:00', 1);
INSERT INTO TouristBadge (tourist_badge_id, badge_id, request_id, attainment_date, start_attain_date, tourist_id)
VALUES (3, 6, null, null, '2019-03-05 00:00:00', 1);
INSERT INTO TouristBadge (tourist_badge_id, badge_id, request_id, attainment_date, start_attain_date, tourist_id)
VALUES (4, 4, null, '2013-09-30', '2013-03-12 00:00:00', 2);
INSERT INTO TouristBadge (tourist_badge_id, badge_id, request_id, attainment_date, start_attain_date, tourist_id)
VALUES (5, 5, null, '2015-01-05', '2014-01-07 00:00:00', 2);
INSERT INTO TouristBadge (tourist_badge_id, badge_id, request_id, attainment_date, start_attain_date, tourist_id)
VALUES (6, 6, null, '2015-11-29', '2015-08-10 00:00:00', 2);
INSERT INTO TouristBadge (tourist_badge_id, badge_id, request_id, attainment_date, start_attain_date, tourist_id)
VALUES (7, 7, null, '2016-10-29', '2016-01-25 00:00:00', 2);
INSERT INTO TouristBadge (tourist_badge_id, badge_id, request_id, attainment_date, start_attain_date, tourist_id)
VALUES (8, 11, null, null, '2019-08-30 00:00:00', 2);
INSERT INTO TouristBadge (tourist_badge_id, badge_id, request_id, attainment_date, start_attain_date, tourist_id)
VALUES (9, 2, null, null, '2019-05-12 00:00:46', 3);
INSERT INTO TouristBadge (tourist_badge_id, badge_id, request_id, attainment_date, start_attain_date, tourist_id)
VALUES (10, 2, null, null, '2019-06-27 12:14:36', 4);
INSERT INTO Article (article_id, title, add_date, text, keywords, update_date, employee_id)
VALUES (1, 'Sympozjum Sudeckie', '2019-11-06 00:00:00',
        'W związku ze zbliżającym się zakończeniem terminu rejestracji uczestników Sympozjum Sudeckiego "Ziemia Kłodzka w kulturze, architekturze i przyrodzie", które odbędzie się w dniach 16-17 listopada 2019 r. w hotelu "Metro" w Boguszynie k/Kłodzka, w imieniu organizatorów, informujemy, że istnieje jeszcze możliwość wzięcia udziału w dwóch wariantach:    Wariant I: 120 zł (obejmuje: materiały z sympozjum, dwie wycieczki, 3 przerwy kawowe, przejazdy) - NIE obejmuje posiłków oraz publikacji po sympozjum.  Wariant II: 170 zł (obejmuje: materiały z sympozjum, dwie wycieczki, 3 przerwy kawowe, przejazdy, 2 obiady, NIE obejmuje publikacji po sympozjum).   Zgłoszenie ważne wyłącznie z wpłatą na konto Oddziału PTTK, najpóźniej do 8 listopada 2019 r.',
        'sudety', '2019-12-01 00:00:00', 1);
INSERT INTO Article (article_id, title, add_date, text, keywords, update_date, employee_id)
VALUES (2, '58. Zlot Sudeckich Przodowników Turystyki Górskiej', '2019-11-05 00:00:00',
        'Komisja Turystyki Górskiej ZG PTTK, Oddziałowa Komisja Turystyki Górskiej O/PTTK „Ziemi Kłodzkiej“ w Kłodzku oraz Kłodzkie Koło PTTK zapraszają na 58. Ogólnopolski Zlot Sudeckich Przodowników Turystyki Górskiej PTTK, który odbędzie się w Górach Stołowych, w DW Szczelinka w Pasterce w terminie 18-20 października 2019 roku.',
        '#zlot, #przodownicy', '2019-12-23 16:48:01', 2);
INSERT INTO Article (article_id, title, add_date, text, keywords, update_date, employee_id)
VALUES (3, 'Czasowe wyłączenie z eksploatacji "Murowańca" 4.11.-20.12.2019', '2019-10-20 00:00:00',
        'Uprzejmie informujemy, że Schronisko PTTK „Murowaniec” na Hali Gąsienicowej będzie czasowo wyłączone z eksploatacji pomiędzy 4 listopada a 20 grudnia 2019 r.',
        '#schronisko, #wyłączenie, #sudety', '2019-12-23 16:48:01', 1);
INSERT INTO Article (article_id, title, add_date, text, keywords, update_date, employee_id)
VALUES (4, 'Ukazał się nowy tom "Wierchów"!', '2019-10-05 00:00:00',
        'Z radością informujemy, że ukazał się kolejny, 82. tom Wierchów!  Ponadto pragniemy przypomnieć, że w sklepie COTG (link poniżej) można nabywać Wierchy w wersji papierowej, a ostatnie dwa tomy również w wersji e-book (pdf).  sklepcotg.pttk.pl',
        '#wierchy', '2019-12-23 16:48:01', 2);
INSERT INTO Article (article_id, title, add_date, text, keywords, update_date, employee_id)
VALUES (5, 'New article test', '2019-12-24 01:24:10', 'Lorem ipsum test', '#test', '2019-12-24 01:24:10', 1);
INSERT INTO Article (article_id, title, add_date, text, keywords, update_date, employee_id)
VALUES (6, 'New article test', '2019-12-24 01:25:10', 'Lorem ipsum test', '#test', '2019-12-24 01:25:10', 1);
INSERT INTO Article (article_id, title, add_date, text, keywords, update_date, employee_id)
VALUES (7, 'New article test', '2019-12-24 01:34:51', 'Lorem ipsum test', '#test', '2019-12-24 01:34:51', 1);
INSERT INTO Article (article_id, title, add_date, text, keywords, update_date, employee_id)
VALUES (8, 'Lorem ipsum', '2019-12-24 22:08:58', 'hello test', '#test', '2019-12-25 15:38:01', 1);
INSERT INTO Point (point_id, name, longitude, latitude, height, description)
VALUES (1, 'Dzięgielów Zamek', 18.71761, 49.716083, 400, null);
INSERT INTO Point (point_id, name, longitude, latitude, height, description)
VALUES (2, 'Jasieniowa', 18.738841, 49.705636, 320, null);
INSERT INTO Point (point_id, name, longitude, latitude, height, description)
VALUES (3, 'Schronisko PTTK Tuł', 18.736398, 49.707468, 350, null);
INSERT INTO Point (point_id, name, longitude, latitude, height, description)
VALUES (4, 'Cieszyn ', 18.638563, 49.757476, 293, null);
INSERT INTO Point (point_id, name, longitude, latitude, height, description)
VALUES (5, 'Bażanowice', 18.704154, 49.738573, 304, null);
INSERT INTO Point (point_id, name, longitude, latitude, height, description)
VALUES (6, 'Leszna Górna', 18.72982, 49.692685, 445, null);
INSERT INTO Point (point_id, name, longitude, latitude, height, description)
VALUES (7, 'Goleszowo', 23.590613, 41.431762, 757, null);
INSERT INTO Point (point_id, name, longitude, latitude, height, description)
VALUES (8, 'Cisownica', 18.761799, 49.723422, 358, null);
INSERT INTO Point (point_id, name, longitude, latitude, height, description)
VALUES (9, 'Ustroń', 18.825312, 49.714722, 359, null);
INSERT INTO Point (point_id, name, longitude, latitude, height, description)
VALUES (10, 'Ostry', 18.656772, 49.602107, 628, null);
INSERT INTO Point (point_id, name, longitude, latitude, height, description)
VALUES (11, 'Mała Czantoria', 18.77393, 49.692692, 864, null);
INSERT INTO Point (point_id, name, longitude, latitude, height, description)
VALUES (12, 'Czantoria Wielka', 18.804915, 49.678936, 986, null);
INSERT INTO Point (point_id, name, longitude, latitude, height, description)
VALUES (13, 'Wisła Jawornik', 18.832943, 49.66143, 434, null);
INSERT INTO Point (point_id, name, longitude, latitude, height, description)
VALUES (14, 'Przełęcz Beskidek', 23.31455, 48.758616, 967, null);
INSERT INTO Point (point_id, name, longitude, latitude, height, description)
VALUES (15, 'Wisła Obłaziec', 18.848349, 49.677013, 500, null);
INSERT INTO Point (point_id, name, longitude, latitude, height, description)
VALUES (16, 'Wisła Uzdrowisko', 18.8552, 49.658503, 425, null);
INSERT INTO Point (point_id, name, longitude, latitude, height, description)
VALUES (17, 'Schronisko PTTK Stożek', 18.823404, 49.604314, 535, null);
INSERT INTO Point (point_id, name, longitude, latitude, height, description)
VALUES (18, 'Wisła Jurzyków', 18.838723, 49.630947, 670, null);
INSERT INTO Point (point_id, name, longitude, latitude, height, description)
VALUES (19, 'Soszów Wielki', 18.809746, 49.648399, 700, null);
INSERT INTO TripDecision (trip_decision_id, send_date, acceptance_date, is_accepted, justification, card_nr)
VALUES (1, '2015-04-02 00:00:00', '2015-04-07', true, null, 123456);
INSERT INTO TripDecision (trip_decision_id, send_date, acceptance_date, is_accepted, justification, card_nr)
VALUES (2, '2019-12-26 19:43:54', null, false, null, 123456);
INSERT INTO TripDecision (trip_decision_id, send_date, acceptance_date, is_accepted, justification, card_nr)
VALUES (3, '2019-12-28 12:03:13', '2019-12-30', true, null, 123456);
INSERT INTO TripDecision (trip_decision_id, send_date, acceptance_date, is_accepted, justification, card_nr)
VALUES (4, '2019-10-28 12:05:34', '2019-10-31', true, null, 123467);
INSERT INTO TripDecision (trip_decision_id, send_date, acceptance_date, is_accepted, justification, card_nr)
VALUES (5, '2019-12-18 12:07:03', '2019-12-19', true, null, 123456);
INSERT INTO TripDecision (trip_decision_id, send_date, acceptance_date, is_accepted, justification, card_nr)
VALUES (6, '2019-12-28 12:07:31', '2019-12-30', true, null, 123467);
INSERT INTO TripDecision (trip_decision_id, send_date, acceptance_date, is_accepted, justification, card_nr)
VALUES (7, '2019-10-27 12:08:00', '2019-10-28', true, null, 123467);
INSERT INTO TripDecision (trip_decision_id, send_date, acceptance_date, is_accepted, justification, card_nr)
VALUES (8, '2019-12-22 12:08:20', '2019-12-23', true, null, 123467);
INSERT INTO BadgeNorm (norm_id, norm_decision_id, attainment_date, required_point_amount, gained_points,
                       is_verified, tourist_badge_id)
VALUES (1, 1, '2015-09-23', 60, 65, true, 1);
INSERT INTO BadgeNorm (norm_id, norm_decision_id, attainment_date, required_point_amount, gained_points,
                       is_verified, tourist_badge_id)
VALUES (2, 2, '2016-06-20', 120, 120, true, 2);
INSERT INTO BadgeNorm (norm_id, norm_decision_id, attainment_date, required_point_amount, gained_points,
                       is_verified, tourist_badge_id)
VALUES (3, null, null, 340, 200, false, 3);
INSERT INTO BadgeNorm (norm_id, norm_decision_id, attainment_date, required_point_amount, gained_points,
                       is_verified, tourist_badge_id)
VALUES (4, 3, '2013-09-30', 60, 60, true, 4);
INSERT INTO BadgeNorm (norm_id, norm_decision_id, attainment_date, required_point_amount, gained_points,
                       is_verified, tourist_badge_id)
VALUES (5, 4, '2015-01-05', 120, 130, true, 5);
INSERT INTO BadgeNorm (norm_id, norm_decision_id, attainment_date, required_point_amount, gained_points,
                       is_verified, tourist_badge_id)
VALUES (6, 5, '2015-11-29', 340, 350, true, 6);
INSERT INTO BadgeNorm (norm_id, norm_decision_id, attainment_date, required_point_amount, gained_points,
                       is_verified, tourist_badge_id)
VALUES (7, 6, null, 720, 728, true, 7);
INSERT INTO BadgeNorm (norm_id, norm_decision_id, attainment_date, required_point_amount, gained_points,
                       is_verified, tourist_badge_id)
VALUES (8, 7, '2016-10-29', 720, 720, true, 7);
INSERT INTO BadgeNorm (norm_id, norm_decision_id, attainment_date, required_point_amount, gained_points,
                       is_verified, tourist_badge_id)
VALUES (9, null, null, 7200, 600, false, 8);
INSERT INTO BadgeNorm (norm_id, norm_decision_id, attainment_date, required_point_amount, gained_points,
                       is_verified, tourist_badge_id)
VALUES (10, 8, null, 30, 30, false, 9);
INSERT INTO BadgeNorm (norm_id, norm_decision_id, attainment_date, required_point_amount, gained_points,
                       is_verified, tourist_badge_id)
VALUES (11, 9, null, 30, 30, false, 10);
INSERT INTO Trip (trip_id, decision_trip_id, norm_id, is_leader_present, name, description, create_date,
                  verification_status, tourist_id)
VALUES (1, 1, 6, false, 'Wypad z misiaczkami', 'Niezapomniana wycieczka z najleszymi przyjaciółmi ze studiów :)',
        '2015-04-01 00:00:00', 3, 2);
INSERT INTO Trip (trip_id, decision_trip_id, norm_id, is_leader_present, name, description, create_date,
                  verification_status, tourist_id)
VALUES (2, null, 3, false, 'Beskidy', null, '2019-07-05 00:00:00', 1, 1);
INSERT INTO Trip (trip_id, decision_trip_id, norm_id, is_leader_present, name, description, create_date,
                  verification_status, tourist_id)
VALUES (3, null, 3, false, 'Beskidy śląskie ', null, '2019-10-12 00:00:00', 1, 1);
INSERT INTO Trip (trip_id, decision_trip_id, norm_id, is_leader_present, name, description, create_date,
                  verification_status, tourist_id)
VALUES (4, null, 9, false, 'Wycieczka testowa', 'Lorem ipsum lorem ipsum', '2019-12-25 15:23:51', 1, 2);
INSERT INTO Trip (trip_id, decision_trip_id, norm_id, is_leader_present, name, description, create_date,
                  verification_status, tourist_id)
VALUES (5, 2, 3, true, 'Wycieczka testowa', 'Lorem ipsum', '2019-12-26 15:40:48', 2, 1);
INSERT INTO Trip (trip_id, decision_trip_id, norm_id, is_leader_present, name, description, create_date,
                  verification_status, tourist_id)
VALUES (6, 3, 10, false, 'Wypad z przyjaciółmi', 'Wycieczka w Beskidy :)', '2019-12-26 23:56:44', 3, 3);
INSERT INTO Trip (trip_id, decision_trip_id, norm_id, is_leader_present, name, description, create_date,
                  verification_status, tourist_id)
VALUES (7, 4, 10, false, 'Beskidy', 'Wycieczka w Beskidy', '2019-10-05 00:00:00', 3, 3);
INSERT INTO Trip (trip_id, decision_trip_id, norm_id, is_leader_present, name, description, create_date,
                  verification_status, tourist_id)
VALUES (8, 5, 10, false, 'Beskidy śląskie ', 'Wycieczka w Beskidy', '2019-12-12 00:00:00', 3, 3);
INSERT INTO Trip (trip_id, decision_trip_id, norm_id, is_leader_present, name, description, create_date,
                  verification_status, tourist_id)
VALUES (9, 6, 11, true, 'Wypad z przyjaciółmi', 'Wycieczka w Beskidy :)', '2019-12-27 12:00:44', 3, 4);
INSERT INTO Trip (trip_id, decision_trip_id, norm_id, is_leader_present, name, description, create_date,
                  verification_status, tourist_id)
VALUES (10, 7, 11, true, 'Beskidy', 'Wycieczka w Beskidy', '2019-10-05 00:00:00', 3, 4);
INSERT INTO Trip (trip_id, decision_trip_id, norm_id, is_leader_present, name, description, create_date,
                  verification_status, tourist_id)
VALUES (11, 8, 11, true, 'Beskidy śląskie ', 'Wycieczka w Beskidy', '2019-12-12 00:00:00', 3, 4);
INSERT INTO Attachment (attachment_id, trip_id, filepath)
VALUES (1, 1, 'C:/eKsiazeczka/turysta2/wycieczka1a');
INSERT INTO Attachment (attachment_id, trip_id, filepath)
VALUES (2, 1, 'C:/eKsiazeczka/turysta2/wycieczka1b');
INSERT INTO Attachment (attachment_id, trip_id, filepath)
VALUES (3, 1, 'C:/eKsiazeczka/turysta2/wycieczka1c');
INSERT INTO Attachment (attachment_id, trip_id, filepath)
VALUES (4, 2, 'C:/eKsiazeczka/turysta1/wycieczka2a');
INSERT INTO Attachment (attachment_id, trip_id, filepath)
VALUES (5, 2, 'C:/eKsiazeczka/turysta1/wycieczka2b');
INSERT INTO Attachment (attachment_id, trip_id, filepath)
VALUES (6, 2, 'C:/eKsiazeczka/turysta1/wycieczka2c');
INSERT INTO Attachment (attachment_id, trip_id, filepath)
VALUES (7, 3, 'C:/eKsiazeczka/turysta1/wycieczka3a');
INSERT INTO Attachment (attachment_id, trip_id, filepath)
VALUES (8, 3, 'C:/eKsiazeczka/turysta1/wycieczka3b');
INSERT INTO Attachment (attachment_id, trip_id, filepath)
VALUES (9, 3, 'C:/eKsiazeczka/turysta1/wycieczka3c');
INSERT INTO Attachment (attachment_id, trip_id, filepath)
VALUES (10, 6, 'C:/eKsiazeczka/turysta3/wycieczka1a');
INSERT INTO Attachment (attachment_id, trip_id, filepath)
VALUES (11, 6, 'C:/eKsiazeczka/turysta3/wycieczka1b');
INSERT INTO Attachment (attachment_id, trip_id, filepath)
VALUES (12, 6, 'C:/eKsiazeczka/turysta3/wycieczka1c');
INSERT INTO Attachment (attachment_id, trip_id, filepath)
VALUES (13, 7, 'C:/eKsiazeczka/turysta3/wycieczka2a');
INSERT INTO Attachment (attachment_id, trip_id, filepath)
VALUES (14, 7, 'C:/eKsiazeczka/turysta3/wycieczka2b');
INSERT INTO Attachment (attachment_id, trip_id, filepath)
VALUES (15, 7, 'C:/eKsiazeczka/turysta3/wycieczka2c');
INSERT INTO Attachment (attachment_id, trip_id, filepath)
VALUES (16, 8, 'C:/eKsiazeczka/turysta3/wycieczka3a');
INSERT INTO Attachment (attachment_id, trip_id, filepath)
VALUES (17, 8, 'C:/eKsiazeczka/turysta3/wycieczka3b');
INSERT INTO Attachment (attachment_id, trip_id, filepath)
VALUES (18, 8, 'C:/eKsiazeczka/turysta3/wycieczka3c');
INSERT INTO Attachment (attachment_id, trip_id, filepath)
VALUES (19, 9, 'C:/eKsiazeczka/turysta4/wycieczka1a');
INSERT INTO Attachment (attachment_id, trip_id, filepath)
VALUES (20, 9, 'C:/eKsiazeczka/turysta4/wycieczka1b');
INSERT INTO Attachment (attachment_id, trip_id, filepath)
VALUES (21, 9, 'C:/eKsiazeczka/turysta4/wycieczka1c');
INSERT INTO Attachment (attachment_id, trip_id, filepath)
VALUES (22, 10, 'C:/eKsiazeczka/turysta4/wycieczka2a');
INSERT INTO Attachment (attachment_id, trip_id, filepath)
VALUES (23, 10, 'C:/eKsiazeczka/turysta4/wycieczka2b');
INSERT INTO Attachment (attachment_id, trip_id, filepath)
VALUES (24, 10, 'C:/eKsiazeczka/turysta4/wycieczka2c');
INSERT INTO Attachment (attachment_id, trip_id, filepath)
VALUES (25, 11, 'C:/eKsiazeczka/turysta4/wycieczka3a');
INSERT INTO Attachment (attachment_id, trip_id, filepath)
VALUES (26, 11, 'C:/eKsiazeczka/turysta4/wycieczka3b');
INSERT INTO Attachment (attachment_id, trip_id, filepath)
VALUES (27, 11, 'C:/eKsiazeczka/turysta4/wycieczka3c');
INSERT INTO PointRoute (point_route_id, subgroup_id, points, length, height_diff, is_available, description,
                        employee_id, start_point, end_point)
VALUES (1, 1, 6, null, null, true, null, 1, 4, 1);
INSERT INTO PointRoute (point_route_id, subgroup_id, points, length, height_diff, is_available, description,
                        employee_id, start_point, end_point)
VALUES (2, 1, 2, 3.2, null, true, null, 2, 5, 1);
INSERT INTO PointRoute (point_route_id, subgroup_id, points, length, height_diff, is_available, description,
                        employee_id, start_point, end_point)
VALUES (3, 1, 4, null, null, true, null, 1, 6, 1);
INSERT INTO PointRoute (point_route_id, subgroup_id, points, length, height_diff, is_available, description,
                        employee_id, start_point, end_point)
VALUES (4, 1, 5, null, null, true, null, 2, 7, 1);
INSERT INTO PointRoute (point_route_id, subgroup_id, points, length, height_diff, is_available, description,
                        employee_id, start_point, end_point)
VALUES (5, 1, 5, null, null, true, null, 1, 5, 2);
INSERT INTO PointRoute (point_route_id, subgroup_id, points, length, height_diff, is_available, description,
                        employee_id, start_point, end_point)
VALUES (6, 1, 3, 2.3, null, true, null, 1, 1, 2);
INSERT INTO PointRoute (point_route_id, subgroup_id, points, length, height_diff, is_available, description,
                        employee_id, start_point, end_point)
VALUES (7, 1, 4, null, null, true, null, 2, 7, 2);
INSERT INTO PointRoute (point_route_id, subgroup_id, points, length, height_diff, is_available, description,
                        employee_id, start_point, end_point)
VALUES (8, 1, 4, null, null, true, null, 1, 8, 2);
INSERT INTO PointRoute (point_route_id, subgroup_id, points, length, height_diff, is_available, description,
                        employee_id, start_point, end_point)
VALUES (9, 1, 6, 0.5, null, true, null, 2, 2, 3);
INSERT INTO PointRoute (point_route_id, subgroup_id, points, length, height_diff, is_available, description,
                        employee_id, start_point, end_point)
VALUES (10, 1, 9, null, null, true, null, 1, 7, 3);
INSERT INTO PointRoute (point_route_id, subgroup_id, points, length, height_diff, is_available, description,
                        employee_id, start_point, end_point)
VALUES (11, 1, 4, null, null, true, null, 1, 6, 3);
INSERT INTO PointRoute (point_route_id, subgroup_id, points, length, height_diff, is_available, description,
                        employee_id, start_point, end_point)
VALUES (12, 1, 5, null, null, true, null, 2, 8, 3);
INSERT INTO PointRoute (point_route_id, subgroup_id, points, length, height_diff, is_available, description,
                        employee_id, start_point, end_point)
VALUES (13, 1, 11, null, null, true, null, 2, 9, 3);
INSERT INTO PointRoute (point_route_id, subgroup_id, points, length, height_diff, is_available, description,
                        employee_id, start_point, end_point)
VALUES (14, 1, 6, null, null, true, null, 2, 6, 10);
INSERT INTO PointRoute (point_route_id, subgroup_id, points, length, height_diff, is_available, description,
                        employee_id, start_point, end_point)
VALUES (15, 1, 5, null, null, true, null, 1, 6, 10);
INSERT INTO PointRoute (point_route_id, subgroup_id, points, length, height_diff, is_available, description,
                        employee_id, start_point, end_point)
VALUES (16, 1, 6, 5.1, null, true, null, 1, 3, 11);
INSERT INTO PointRoute (point_route_id, subgroup_id, points, length, height_diff, is_available, description,
                        employee_id, start_point, end_point)
VALUES (17, 1, 5, null, null, true, null, 2, 10, 11);
INSERT INTO PointRoute (point_route_id, subgroup_id, points, length, height_diff, is_available, description,
                        employee_id, start_point, end_point)
VALUES (18, 1, 9, null, null, true, null, 2, 8, 11);
INSERT INTO PointRoute (point_route_id, subgroup_id, points, length, height_diff, is_available, description,
                        employee_id, start_point, end_point)
VALUES (19, 1, 10, null, null, true, null, 1, 9, 11);
INSERT INTO PointRoute (point_route_id, subgroup_id, points, length, height_diff, is_available, description,
                        employee_id, start_point, end_point)
VALUES (20, 1, 9, null, null, true, null, 1, 1, 11);
INSERT INTO PointRoute (point_route_id, subgroup_id, points, length, height_diff, is_available, description,
                        employee_id, start_point, end_point)
VALUES (21, 1, 9, null, null, true, null, 2, 6, 11);
INSERT INTO PointRoute (point_route_id, subgroup_id, points, length, height_diff, is_available, description,
                        employee_id, start_point, end_point)
VALUES (22, 1, 4, 3, null, true, null, 1, 11, 12);
INSERT INTO PointRoute (point_route_id, subgroup_id, points, length, height_diff, is_available, description,
                        employee_id, start_point, end_point)
VALUES (23, 1, 11, null, null, true, null, 2, 9, 12);
INSERT INTO PointRoute (point_route_id, subgroup_id, points, length, height_diff, is_available, description,
                        employee_id, start_point, end_point)
VALUES (24, 1, 3, 3, null, true, null, 1, 12, 11);
INSERT INTO PointRoute (point_route_id, subgroup_id, points, length, height_diff, is_available, description,
                        employee_id, start_point, end_point)
VALUES (25, 1, 3, 4.3, null, true, null, 2, 11, 10);
INSERT INTO PointRoute (point_route_id, subgroup_id, points, length, height_diff, is_available, description,
                        employee_id, start_point, end_point)
VALUES (26, 1, 3, 2, null, true, null, 1, 10, 6);
INSERT INTO Leader_Group (group_id, card_nr)
VALUES (2, 123456);
INSERT INTO Leader_Group (group_id, card_nr)
VALUES (2, 123467);
INSERT INTO Trip_PointRoute (trip_id, point_route_id, walk_date, lp, points)
VALUES (1, 2, '2015-08-15', 1, 2);
INSERT INTO Trip_PointRoute (trip_id, point_route_id, walk_date, lp, points)
VALUES (1, 6, '2015-08-16', 2, 3);
INSERT INTO Trip_PointRoute (trip_id, point_route_id, walk_date, lp, points)
VALUES (1, 9, '2015-08-16', 3, 6);
INSERT INTO Trip_PointRoute (trip_id, point_route_id, walk_date, lp, points)
VALUES (2, 16, '2019-03-10', 1, 6);
INSERT INTO Trip_PointRoute (trip_id, point_route_id, walk_date, lp, points)
VALUES (2, 22, '2019-03-10', 2, 4);
INSERT INTO Trip_PointRoute (trip_id, point_route_id, walk_date, lp, points)
VALUES (3, 24, '2019-06-20', 1, 3);
INSERT INTO Trip_PointRoute (trip_id, point_route_id, walk_date, lp, points)
VALUES (3, 25, '2019-06-21', 2, 3);
INSERT INTO Trip_PointRoute (trip_id, point_route_id, walk_date, lp, points)
VALUES (3, 26, '2019-06-22', 3, 3);
INSERT INTO Trip_PointRoute (trip_id, point_route_id, walk_date, lp, points)
VALUES (5, 24, '2019-12-26', 1, 3);
INSERT INTO Trip_PointRoute (trip_id, point_route_id, walk_date, lp, points)
VALUES (5, 25, '2019-12-26', 2, 3);
INSERT INTO Trip_PointRoute (trip_id, point_route_id, walk_date, lp, points)
VALUES (6, 2, '2019-10-05', 1, 2);
INSERT INTO Trip_PointRoute (trip_id, point_route_id, walk_date, lp, points)
VALUES (6, 6, '2019-10-05', 2, 3);
INSERT INTO Trip_PointRoute (trip_id, point_route_id, walk_date, lp, points)
VALUES (6, 9, '2019-10-05', 3, 6);
INSERT INTO Trip_PointRoute (trip_id, point_route_id, walk_date, lp, points)
VALUES (7, 16, '2019-11-05', 1, 6);
INSERT INTO Trip_PointRoute (trip_id, point_route_id, walk_date, lp, points)
VALUES (7, 22, '2019-11-05', 2, 4);
INSERT INTO Trip_PointRoute (trip_id, point_route_id, walk_date, lp, points)
VALUES (8, 24, '2019-11-20', 1, 3);
INSERT INTO Trip_PointRoute (trip_id, point_route_id, walk_date, lp, points)
VALUES (8, 25, '2019-11-21', 2, 3);
INSERT INTO Trip_PointRoute (trip_id, point_route_id, walk_date, lp, points)
VALUES (8, 26, '2019-11-22', 3, 3);
INSERT INTO Trip_PointRoute (trip_id, point_route_id, walk_date, lp, points)
VALUES (9, 2, '2019-10-05', 1, 2);
INSERT INTO Trip_PointRoute (trip_id, point_route_id, walk_date, lp, points)
VALUES (9, 6, '2019-10-05', 2, 3);
INSERT INTO Trip_PointRoute (trip_id, point_route_id, walk_date, lp, points)
VALUES (9, 9, '2019-10-05, 3', 3, 6);
INSERT INTO Trip_PointRoute (trip_id, point_route_id, walk_date, lp, points)
VALUES (10, 16, '2019-11-05', 1, 6);
INSERT INTO Trip_PointRoute (trip_id, point_route_id, walk_date, lp, points)
VALUES (10, 22, '2019-11-05', 2, 4);
INSERT INTO Trip_PointRoute (trip_id, point_route_id, walk_date, lp, points)
VALUES (11, 24, '2019-11-20', 1, 3);
INSERT INTO Trip_PointRoute (trip_id, point_route_id, walk_date, lp, points)
VALUES (11, 25, '2019-11-21', 2, 3);
INSERT INTO Trip_PointRoute (trip_id, point_route_id, walk_date, lp, points)
VALUES (11, 26, '2019-11-22', 3, 3);
