package gotback.config;

import org.springframework.context.annotation.Bean;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.sql.Timestamp;

@org.springframework.context.annotation.Configuration
@EnableSwagger2
public class SpringFoxConfig {
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build()
                .apiInfo(getApiInfo())
                .directModelSubstitute(Timestamp.class, Long.class);
    }

    private ApiInfo getApiInfo() {
        return new ApiInfoBuilder()
                .title("eKsiazeczka GOT PTTK")
                .description("REST API for university project about collecting GOT PTTK badges")
                .version("1.0")
                .license("Apache License Version 2.0")
                .licenseUrl("https://www.apache.org/licenses/LICENSE-2.0")
                .contact(new Contact("Joanna Chlebus, Kamil Plich",
                        "https://gitlab.com/Joanna065/gorska-odznaka-turystyczna",
                        ""))
                .build();
    }

}
