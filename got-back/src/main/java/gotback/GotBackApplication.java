package gotback;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GotBackApplication {

	public static void main(String[] args) {
		SpringApplication.run(GotBackApplication.class, args);
	}

}
