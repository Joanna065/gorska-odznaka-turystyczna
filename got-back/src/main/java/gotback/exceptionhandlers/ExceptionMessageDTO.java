package gotback.exceptionhandlers;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@ApiModel(description = "Response model of exception")
public class ExceptionMessageDTO {
    @ApiModelProperty(notes = "Message of exception", example = "An error occurred")
    private  String message;
}
