package gotback.exceptionhandlers;

import gotback.exceptions.GOTException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.List;

@Slf4j
@ControllerAdvice
public class RestResponseExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({Exception.class})
    public ResponseEntity handleAnyException(Exception ex) {
        return errorResponse(new ExceptionMessageDTO(ex.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler({DataAccessException.class})
    public ResponseEntity handlePersistenceException(DataAccessException ex) {
        return errorResponse(new ExceptionMessageDTO(ex.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler({EmptyResultDataAccessException.class})
    public ResponseEntity handleEmptyResultException(DataAccessException ex) {
        return errorResponse(new ExceptionMessageDTO(ex.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({IllegalArgumentException.class})
    public ResponseEntity handleIllegalArgumentException(IllegalArgumentException ex) {
        return errorResponse(new ExceptionMessageDTO(ex.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({GOTException.class})
    public ResponseEntity handleGOTException(GOTException ex) {
        switch (ex.getExceptionType()) {
            case INVALID_RESOURCE_ID:
            case INVALID_DAILY_POINTS_NORM:
            case INVALID_LEADER_RIGHTS:
            case LEADER_RESOURCE_RIGHTS:
            case INVALID_DELETE:
            case MISSING_DATA:
            case INVALID_VERIFY:
            case ALREADY_VERIFIED_NORM:
            case REPEATED_DATA:
            case WRONG_BELONGING:
                return errorResponse(ex, HttpStatus.BAD_REQUEST);
            default:
                return errorResponse(ex, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ExceptionHandler({ConstraintViolationException.class})
    public ResponseEntity handleConstraintException(ConstraintViolationException ex) {

        FieldValidationDTO result = new FieldValidationDTO(
                ex.getConstraintViolations().size());
        for (ConstraintViolation v : ex.getConstraintViolations()) {
            result.addError(v.getPropertyPath().toString(), v.getMessage());
        }
        return errorResponse(result, HttpStatus.BAD_REQUEST);
    }

    @Override
    public ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                               HttpHeaders headers, HttpStatus status, WebRequest request) {

        List<FieldError> fieldErrors = ex.getBindingResult().getFieldErrors();
        List<ObjectError> globalErrors = ex.getBindingResult().getGlobalErrors();

        FieldValidationDTO result = new FieldValidationDTO(
                ex.getBindingResult().getAllErrors().size());

        for (FieldError fieldError : fieldErrors) {
            result.addError(fieldError.getField(), fieldError.getDefaultMessage());
        }
        for (ObjectError objectError : globalErrors) {
            result.addError(objectError.getObjectName(), objectError.getDefaultMessage());
        }
        return errorResponse(result, status);
    }

    private ResponseEntity errorResponse(ExceptionMessageDTO msg, HttpStatus status) {
        log.error("Error caught: " + msg.getMessage());
        return response(msg, status);
    }

    private ResponseEntity errorResponse(GOTException ex, HttpStatus status) {
        log.error("Error caught: " + ex.getMessage());
        return response(GotExceptionResponseDTO.payloadOf(ex), status);
    }

    private <T> ResponseEntity<T> response(T body, HttpStatus status) {
        return new ResponseEntity<>(body, status);
    }

}
