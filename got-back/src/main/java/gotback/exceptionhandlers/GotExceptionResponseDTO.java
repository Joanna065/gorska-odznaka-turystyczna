package gotback.exceptionhandlers;

import com.fasterxml.jackson.annotation.JsonInclude;
import gotback.exceptions.ExceptionType;
import gotback.exceptions.GOTException;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.FieldDefaults;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@ApiModel(description = "Response model of GOT custom exception")
public class GotExceptionResponseDTO {
    @ApiModelProperty(notes = "Message of exception", example = "Invalid id number")
    String message;
    @ApiModelProperty(notes = "Enum type of custom exception", example = "INVALID_RESOURCE_ID")
    ExceptionType excType;

    public static GotExceptionResponseDTO payloadOf(GOTException ex) {
        return GotExceptionResponseDTO.builder().message(ex.getMessage())
                .excType(ex.getExceptionType()).build();
    }
}
