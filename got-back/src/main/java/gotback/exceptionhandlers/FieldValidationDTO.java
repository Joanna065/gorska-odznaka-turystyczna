package gotback.exceptionhandlers;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.HashMap;
import java.util.Map;

@EqualsAndHashCode(callSuper = true)
@Data
public class FieldValidationDTO extends ExceptionMessageDTO {

    private Map<String, String> violations;

    public FieldValidationDTO(int messagesSize) {
        super("Field(s) validation error");
        violations = new HashMap<>(messagesSize);
    }

    public void addError(String field, String message) {
        if (violations.containsKey(field)) {
            message = violations.get(field) + System.lineSeparator() + message;
        }
        violations.put(field, message);
    }
}