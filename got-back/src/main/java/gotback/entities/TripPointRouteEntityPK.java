package gotback.entities;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TripPointRouteEntityPK implements Serializable {
    @Column(name = "trip_id", nullable = false, insertable = false, updatable = false)
    @Id
    Integer tripId;
    @Column(name = "point_route_id", nullable = false, insertable = false, updatable = false)
    @Id
    Integer pointRouteId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TripPointRouteEntityPK that = (TripPointRouteEntityPK) o;
        return tripId.equals(that.tripId) && pointRouteId.equals(that.pointRouteId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tripId, pointRouteId);
    }
}
