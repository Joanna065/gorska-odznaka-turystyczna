package gotback.entities;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TripNopointRouteEntityPK implements Serializable {
    @Column(name = "trip_id", nullable = false, insertable = false, updatable = false)
    @Id
    Integer trip;
    @Column(name = "nopoint_route_id", nullable = false, insertable = false, updatable = false)
    @Id
    Integer nopointRoute;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TripNopointRouteEntityPK that = (TripNopointRouteEntityPK) o;
        return trip.equals(that.trip) && nopointRoute.equals(that.nopointRoute);
    }

    @Override
    public int hashCode() {
        return Objects.hash(trip, nopointRoute);
    }
}
