package gotback.entities;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "Leader", schema = "got")
@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LeaderEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "card_nr", nullable = false)
    Integer cardNr;
    @Column(name = "phone_nr", nullable = false, unique = true)
    String phoneNr;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", referencedColumnName = "user_id", nullable = false)
    LoggedUserEntity loggedUserByUserId;
    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "Leader_Group",
            joinColumns = @JoinColumn(name = "card_nr"),
            inverseJoinColumns = @JoinColumn(name = "group_id"))
    Set<MountainGroupEntity> mountaingGroups = new HashSet<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LeaderEntity that = (LeaderEntity) o;
        return cardNr.equals(that.cardNr) && Objects.equals(phoneNr, that.phoneNr);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cardNr, phoneNr);
    }
}
