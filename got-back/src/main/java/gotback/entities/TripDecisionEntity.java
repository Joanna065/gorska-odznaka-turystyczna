package gotback.entities;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "TripDecision", schema = "got")
@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TripDecisionEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "trip_decision_id", nullable = false)
    Integer tripDecisionId;
    @CreationTimestamp
    @Column(name = "send_date", nullable = false)
    Timestamp sendDate;
    @Column(name = "acceptance_date")
    Date acceptanceDate;
    @Column(name = "is_accepted", nullable = false, columnDefinition = "bit default 0")
    Boolean isAccepted;
    @Column(name = "justification")
    String justification;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "card_nr", referencedColumnName = "card_nr")
    LeaderEntity leader;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TripDecisionEntity that = (TripDecisionEntity) o;
        return tripDecisionId.equals(that.tripDecisionId) &&
                isAccepted == that.isAccepted &&
                Objects.equals(sendDate, that.sendDate) &&
                Objects.equals(acceptanceDate, that.acceptanceDate) &&
                Objects.equals(justification, that.justification);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tripDecisionId, sendDate, acceptanceDate, isAccepted, justification);
    }
}
