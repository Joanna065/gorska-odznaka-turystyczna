package gotback.entities;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "Trip", schema = "got")
@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TripEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "trip_id", nullable = false)
    Integer tripId;
    @Column(name = "is_leader_present", nullable = false)
    Boolean isLeaderPresent;
    @Column(name = "name", nullable = false)
    String name;
    @Column(name = "description")
    String description;
    @Column(name = "create_date", nullable = false)
    @CreationTimestamp
    Timestamp createDate;
    @Column(name = "verification_status", nullable = false)
    Integer verificationStatus;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "norm_id", referencedColumnName = "norm_id", nullable = false)
    BadgeNormEntity badgeNorm;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tourist_id", referencedColumnName = "tourist_id", nullable = false)
    TouristEntity tourist;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "decision_trip_id", referencedColumnName = "trip_decision_id")
    TripDecisionEntity tripDecision;
    @OneToMany(mappedBy = "trip", cascade = CascadeType.ALL, orphanRemoval = true)
    List<AttachmentEntity> attachments = new ArrayList<>();

    @OneToMany(mappedBy = "trip", cascade = CascadeType.ALL, orphanRemoval = true)
    List<TripPointRouteEntity> tripPointRoutes = new ArrayList<>();

    @OneToMany(mappedBy = "trip", cascade = CascadeType.ALL, orphanRemoval = true)
    List<TripNopointRouteEntity> tripNopointRoutes = new ArrayList<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TripEntity that = (TripEntity) o;
        return tripId.equals(that.tripId) &&
                isLeaderPresent == that.isLeaderPresent &&
                verificationStatus.equals(that.verificationStatus) &&
                Objects.equals(name, that.name) &&
                Objects.equals(description, that.description) &&
                Objects.equals(createDate, that.createDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tripId, isLeaderPresent, name, description, createDate, verificationStatus);
    }
}
