package gotback.entities;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "NopointRoute", schema = "got")
@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class NopointRouteEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "nopoint_route_id", nullable = false)
    Integer nopointRouteId;
    @Column(name = "points")
    Integer points;
    @Column(name = "length", nullable = false)
    Double length;
    @Column(name = "height_diff")
    Double heightDiff;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "subgroup_id", referencedColumnName = "subgroup_id", nullable = false)
    MountainSubgroupEntity mountainSubgroup;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "start_point", referencedColumnName = "point_id", nullable = false)
    PointEntity startPoint;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "end_point", referencedColumnName = "point_id", nullable = false)
    PointEntity endPoint;

    @OneToMany(mappedBy = "nopointRoute", cascade = CascadeType.ALL, orphanRemoval = true)
    List<TripNopointRouteEntity> nopointRoutes = new ArrayList<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NopointRouteEntity that = (NopointRouteEntity) o;
        return nopointRouteId.equals(that.nopointRouteId) &&
                Double.compare(that.length, length) == 0 &&
                Objects.equals(points, that.points) &&
                Objects.equals(heightDiff, that.heightDiff);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nopointRouteId, points, length, heightDiff);
    }
}
