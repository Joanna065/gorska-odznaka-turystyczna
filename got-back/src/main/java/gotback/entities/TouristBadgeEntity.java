package gotback.entities;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "TouristBadge", schema = "got")
@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TouristBadgeEntity {
    @Id
    @Column(name = "tourist_badge_id", nullable = false)
    Integer touristBadgeId;
    @Column(name = "attainment_date")
    Date attainmentDate;
    @Column(name = "start_attain_date", nullable = false)
    Timestamp startAttainDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "badge_id", referencedColumnName = "badge_id", nullable = false)
    BadgeEntity badge;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "request_id", referencedColumnName = "request_id")
    BadgeRequestEntity badgeRequest;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tourist_id", referencedColumnName = "tourist_id", nullable = false)
    TouristEntity tourist;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TouristBadgeEntity that = (TouristBadgeEntity) o;
        return touristBadgeId.equals(that.touristBadgeId) &&
                Objects.equals(attainmentDate, that.attainmentDate) &&
                Objects.equals(startAttainDate, that.startAttainDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(touristBadgeId, attainmentDate, startAttainDate);
    }
}
