package gotback.entities;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

@Entity
@Table(name = "LoggedUser", schema = "got")
@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoggedUserEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id", nullable = false)
    Integer userId;
    @Column(name = "login", nullable = false, unique = true)
    String login;
    @Column(name = "password", nullable = false, length = 60)
    String password;
    @Column(name = "surname", nullable = false, length = 50)
    String surname;
    @Column(name = "name", nullable = false, length = 50)
    String name;
    @Column(name = "birth_date", nullable = false)
    Date birthDate;
    @Column(name = "street", length = 100)
    String street;
    @Column(name = "home_nr", length = 5)
    String homeNr;
    @Column(name = "housing_nr", length = 5)
    String housingNr;
    @Column(name = "postal_code", length = 2555)
    String postalCode;
    @Column(name = "city")
    String city;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LoggedUserEntity that = (LoggedUserEntity) o;
        return userId.equals(that.userId) &&
                Objects.equals(login, that.login) &&
                Objects.equals(password, that.password) &&
                Objects.equals(surname, that.surname) &&
                Objects.equals(name, that.name) &&
                Objects.equals(birthDate, that.birthDate) &&
                Objects.equals(street, that.street) &&
                Objects.equals(homeNr, that.homeNr) &&
                Objects.equals(housingNr, that.housingNr) &&
                Objects.equals(postalCode, that.postalCode) &&
                Objects.equals(city, that.city);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, login, password, surname, name, birthDate, street, homeNr, housingNr, postalCode, city);
    }
}
