package gotback.entities;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "Trip_PointRoute", schema = "got")
@IdClass(TripPointRouteEntityPK.class)
public class TripPointRouteEntity {
    @Id
    @Column(name = "trip_id", nullable = false)
    Integer tripId;
    @Id
    @Column(name = "point_route_id", nullable = false)
    Integer pointRouteId;
    @Column(name = "walk_date")
    Date walkDate;
    @Column(name = "lp")
    Integer lp;
    @Column(name = "points")
    Integer points;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "trip_id", referencedColumnName = "trip_id", nullable = false)
    TripEntity trip;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "point_route_id", referencedColumnName = "point_route_id", nullable = false)
    PointRouteEntity pointRoute;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TripPointRouteEntity that = (TripPointRouteEntity) o;
        return tripId.equals(that.tripId) &&
                pointRouteId.equals(that.pointRouteId) &&
                Objects.equals(walkDate, that.walkDate) &&
                lp.equals(that.lp) &&
                points.equals(that.points);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tripId, pointRouteId, walkDate, lp, points);
    }
}
