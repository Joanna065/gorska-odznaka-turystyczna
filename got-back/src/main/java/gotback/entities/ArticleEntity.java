package gotback.entities;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "Article", schema = "got")
@FieldDefaults(level = AccessLevel.PRIVATE)
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ArticleEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "article_id", nullable = false, updatable = false)
    Integer articleId;
    @Column(name = "title", nullable = false)
    String title;
    @Column(name = "add_date", nullable = false)
    @CreationTimestamp
    Timestamp addDate;
    @Column(name = "text", nullable = false, length = 1000)
    String text;
    @Column(name = "keywords")
    @Pattern(
            regexp = "^$|(#[a-zA-Z0-9A-z]+,? *)*#[a-zA-Z0-9A-z]+",
            message = "Keywords should match format '#word, #word, #word'."
    )
    String keyWords;
    @Column(name = "update_date", nullable = false)
    @UpdateTimestamp
    Timestamp updateDate;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "employee_id", nullable = false)
    EmployeeEntity employeeEntity;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ArticleEntity that = (ArticleEntity) o;
        return articleId.equals(that.articleId) &&
                Objects.equals(title, that.title) &&
                Objects.equals(addDate, that.addDate) &&
                Objects.equals(text, that.text) &&
                Objects.equals(keyWords, that.keyWords) &&
                Objects.equals(updateDate, that.updateDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(articleId, title, addDate, text, keyWords, updateDate);
    }
}
