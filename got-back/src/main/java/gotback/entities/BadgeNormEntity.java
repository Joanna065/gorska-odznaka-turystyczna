package gotback.entities;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "BadgeNorm", schema = "got")
@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BadgeNormEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "norm_id", nullable = false)
    Integer normId;
    @Column(name = "attainment_date")
    Date attainmentDate;
    @Column(name = "required_point_amount")
    Integer requiredPointAmount;
    @Column(name = "gained_points", nullable = false)
    Integer gainedPoints;
    @Column(name = "is_verified", nullable = false)
    Boolean isVerified;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "norm_decision_id", referencedColumnName = "norm_decision_id")
    NormDecisionEntity normDecision;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tourist_badge_id", referencedColumnName = "tourist_badge_id", nullable = false)
    TouristBadgeEntity touristBadge;
    @OneToMany(mappedBy = "badgeNorm")
    List<TripEntity> trips;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BadgeNormEntity that = (BadgeNormEntity) o;
        return normId.equals(that.normId) &&
                gainedPoints.equals(that.gainedPoints) &&
                isVerified == that.isVerified &&
                Objects.equals(attainmentDate, that.attainmentDate) &&
                Objects.equals(requiredPointAmount, that.requiredPointAmount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(normId, attainmentDate, requiredPointAmount, gainedPoints, isVerified);
    }
}
