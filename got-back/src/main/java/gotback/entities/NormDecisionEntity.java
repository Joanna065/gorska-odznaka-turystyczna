package gotback.entities;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "NormDecision", schema = "got")
@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class NormDecisionEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "norm_decision_id", nullable = false)
    Integer normDecisionId;
    @Column(name = "add_date", nullable = false)
    @CreationTimestamp
    Timestamp addDate;
    @Column(name = "response_date")
    Date responseDate;
    @Column(name = "is_accepted", nullable = false)
    Boolean isAccepted;
    @Column(name = "justification")
    String justification;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "employee_id", referencedColumnName = "employee_id", nullable = false)
    EmployeeEntity employee;
    @OneToOne(mappedBy = "normDecision")
    BadgeNormEntity badgeNorm;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NormDecisionEntity that = (NormDecisionEntity) o;
        return normDecisionId.equals(that.normDecisionId) &&
                isAccepted == that.isAccepted &&
                Objects.equals(addDate, that.addDate) &&
                Objects.equals(responseDate, that.responseDate) &&
                Objects.equals(justification, that.justification);
    }

    @Override
    public int hashCode() {
        return Objects.hash(normDecisionId, addDate, responseDate, isAccepted, justification);
    }
}
