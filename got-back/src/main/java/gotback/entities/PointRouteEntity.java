package gotback.entities;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "PointRoute", schema = "got")
@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PointRouteEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "point_route_id", nullable = false)
    Integer pointRouteId;
    @Column(name = "points", nullable = false)
    Integer points;
    @Column(name = "length")
    Double length;
    @Column(name = "height_diff")
    Double heightDiff;
    @Column(name = "is_available", nullable = false)
    Boolean isAvailable;
    @Column(name = "description")
    String description;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "subgroup_id", referencedColumnName = "subgroup_id", nullable = false)
    MountainSubgroupEntity mountainSubgroup;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "start_point", referencedColumnName = "point_id", nullable = false)
    PointEntity startPoint;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "end_point", referencedColumnName = "point_id", nullable = false)
    PointEntity endPoint;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "employee_id", referencedColumnName = "employee_id", nullable = false)
    EmployeeEntity employee;

    @OneToMany(mappedBy = "pointRoute", cascade = CascadeType.ALL, orphanRemoval = true)
    List<TripPointRouteEntity> tripPointRoutes = new ArrayList<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PointRouteEntity that = (PointRouteEntity) o;
        return pointRouteId.equals(that.pointRouteId) &&
                points.equals(that.points) &&
                isAvailable.equals(that.isAvailable) &&
                Objects.equals(length, that.length) &&
                Objects.equals(heightDiff, that.heightDiff) &&
                Objects.equals(description, that.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pointRouteId, points, length, heightDiff, isAvailable, description);
    }
}
