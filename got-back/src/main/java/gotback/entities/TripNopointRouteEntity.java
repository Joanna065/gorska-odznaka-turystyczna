package gotback.entities;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

@Entity
@Table(name = "Trip_NopointRoute", schema = "got")
@IdClass(TripNopointRouteEntityPK.class)
public class TripNopointRouteEntity {
    @Id
    @Column(name = "trip_id", nullable = false)
    Integer tripId;
    @Id
    @Column(name = "nopoint_route_id", nullable = false)
    Integer nopointRouteId;
    @Column(name = "walk_date")
    Date walkDate;
    @Column(name = "lp")
    Integer lp;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "trip_id", referencedColumnName = "trip_id", nullable = false)
    TripEntity trip;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "nopoint_route_id", referencedColumnName = "nopoint_route_id", nullable = false)
    NopointRouteEntity nopointRoute;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TripNopointRouteEntity that = (TripNopointRouteEntity) o;
        return tripId.equals(that.tripId) &&
                nopointRouteId.equals(that.nopointRouteId) &&
                Objects.equals(walkDate, that.walkDate) &&
                lp.equals(that.lp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tripId, nopointRouteId, walkDate, lp);
    }
}
