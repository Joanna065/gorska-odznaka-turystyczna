package gotback.entities;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "Tourist", schema = "got")
@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TouristEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "tourist_id", nullable = false)
    Integer touristId;
    @Column(name = "is_disabled", nullable = false)
    Boolean isDisabled;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", referencedColumnName = "user_id", nullable = false)
    LoggedUserEntity loggedUserByUserId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TouristEntity that = (TouristEntity) o;
        return touristId.equals(that.touristId) &&
                isDisabled == that.isDisabled;
    }

    @Override
    public int hashCode() {
        return Objects.hash(touristId, isDisabled);
    }
}
