package gotback.entities;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "BadgeRequest", schema = "got")
@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BadgeRequestEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "request_id", nullable = false)
    Integer requestId;
    @Column(name = "send_date", nullable = false)
    Timestamp sendDate;
    @Column(name = "acceptance_date")
    Date acceptanceDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tourist_id", referencedColumnName = "tourist_id", nullable = false)
    TouristEntity tourist;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BadgeRequestEntity that = (BadgeRequestEntity) o;
        return requestId.equals(that.requestId) &&
                Objects.equals(sendDate, that.sendDate) &&
                Objects.equals(acceptanceDate, that.acceptanceDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(requestId, sendDate, acceptanceDate);
    }
}
