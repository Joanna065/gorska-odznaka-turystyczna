package gotback.entities;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "Badge", schema = "got")
@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BadgeEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "badge_id", nullable = false)
    Integer badgeId;
    @Column(name = "type", nullable = false)
    String type;
    @Column(name = "degree", nullable = false)
    String degree;
    @Column(name = "description")
    String description;
    @Column(name = "required_point_amount")
    Integer requiredPointAmount;
    @Column(name = "additional_requirements")
    String additionalRequirements;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BadgeEntity that = (BadgeEntity) o;
        return badgeId.equals(that.badgeId) &&
                Objects.equals(type, that.type) &&
                Objects.equals(degree, that.degree) &&
                Objects.equals(description, that.description) &&
                Objects.equals(requiredPointAmount, that.requiredPointAmount) &&
                Objects.equals(additionalRequirements, that.additionalRequirements);
    }

    @Override
    public int hashCode() {
        return Objects.hash(badgeId, type, degree, description, requiredPointAmount, additionalRequirements);
    }
}
