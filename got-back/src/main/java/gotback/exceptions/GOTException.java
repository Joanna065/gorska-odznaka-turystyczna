package gotback.exceptions;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Getter
public class GOTException extends RuntimeException {
    final ExceptionType exceptionType;

    public GOTException(String cause, Throwable ex, ExceptionType exceptionType) {
        super(cause, ex);
        this.exceptionType = exceptionType;
    }

    public GOTException(String msg, ExceptionType excType) {
        super(msg);
        exceptionType = excType;
    }

}
