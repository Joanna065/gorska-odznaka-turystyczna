package gotback.controllers;

import gotback.dto.ArticleCreateDTO;
import gotback.dto.ArticleResponseDTO;
import gotback.dto.ArticleUpdateDTO;
import gotback.exceptionhandlers.ExceptionMessageDTO;
import gotback.exceptionhandlers.GotExceptionResponseDTO;
import gotback.services.ArticleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/articles")
@RequiredArgsConstructor
@Api(tags = "Article management")
public class ArticleController {

    private final ArticleService articleService;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "View a list of available articles")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = ArticleResponseDTO.class, responseContainer = "List"),
            @ApiResponse(code = 500, message = "Internal server error", response = ExceptionMessageDTO.class)})
    public List<ArticleResponseDTO> getAllArticles() {
        return articleService.getAllArticles();
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "View an article by id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = ArticleResponseDTO.class),
            @ApiResponse(code = 500, message = "Internal server error", response = ExceptionMessageDTO.class)})
    public ResponseEntity<ArticleResponseDTO> getArticleById(@PathVariable Integer id) {
        return ResponseEntity.ok(articleService.getArticleById(id));
    }

    @PatchMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Update an existing article by id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful update", response = Integer.class),
            @ApiResponse(code = 400, message = "Bad Request", response = GotExceptionResponseDTO.class),
            @ApiResponse(code = 500, message = "Internal server error", response = ExceptionMessageDTO.class)})
    public ResponseEntity<Integer> updateArticle(@PathVariable Integer id,
                                                 @Valid @RequestBody ArticleUpdateDTO dto) {
        return ResponseEntity.ok(articleService.updateArticle(id, dto));
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Create a new article")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = Integer.class),
            @ApiResponse(code = 400, message = "Bad Request", response = GotExceptionResponseDTO.class),
            @ApiResponse(code = 500, message = "Internal server error", response = ExceptionMessageDTO.class)})
    public ResponseEntity<Integer> addArticle(@Valid @RequestBody ArticleCreateDTO dto) {
        return new ResponseEntity<>(articleService.addArticle(dto), HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Delete an article by id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful delete", response = Integer.class),
            @ApiResponse(code = 400, message = "Bad Request", response = GotExceptionResponseDTO.class),
            @ApiResponse(code = 500, message = "Internal server error", response = ExceptionMessageDTO.class)})
    public ResponseEntity<Integer> deleteArticle(@PathVariable Integer id) {
        return ResponseEntity.ok(articleService.deleteArticle(id));
    }

    @DeleteMapping(value = "/resources", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Delete selected articles by id list")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful delete", response = Integer.class,
                    responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad Request", response = GotExceptionResponseDTO.class),
            @ApiResponse(code = 500, message = "Internal server error", response = ExceptionMessageDTO.class)})
    public ResponseEntity<List<Integer>> deleteChosenArticles(@RequestParam(required = true) List<Integer> ids) {
        List<Integer> deletedIds = articleService.deleteChosenArticles(ids);
        return ResponseEntity.ok(deletedIds);
    }
}
