package gotback.controllers;

import gotback.dto.NormDecisionResponseDTO;
import gotback.dto.NormDecisionUpdateDTO;
import gotback.exceptionhandlers.ExceptionMessageDTO;
import gotback.exceptionhandlers.GotExceptionResponseDTO;
import gotback.services.NormDecisionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/normDecisions")
@RequiredArgsConstructor
@Api(tags = "Norm decision verification management")
public class NormDecisionController {
    private final NormDecisionService normDecisionService;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "View a list of available unverified norm decisions")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = NormDecisionResponseDTO.class, responseContainer = "List"),
            @ApiResponse(code = 500, message = "Internal server error", response = ExceptionMessageDTO.class)})
    public List<NormDecisionResponseDTO> getAllUnverifiedNormDecisions() {
        return normDecisionService.getAllUnverifiedNormDecisions();
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "View a norm decision by id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = NormDecisionResponseDTO.class),
            @ApiResponse(code = 500, message = "Internal server error", response = ExceptionMessageDTO.class)})
    public ResponseEntity<NormDecisionResponseDTO> getNormDecisionById(@PathVariable Integer id) {
        return ResponseEntity.ok(normDecisionService.getNormDecisionById(id));
    }

    @PatchMapping(value = "/{id}/verify", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Verify norm decision by norm id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = Integer.class),
            @ApiResponse(code = 400, message = "Bad Request", response = GotExceptionResponseDTO.class),
            @ApiResponse(code = 500, message = "Internal server error", response = ExceptionMessageDTO.class)})
    public ResponseEntity<Integer> verifyNormDecision(@PathVariable Integer id,
                                                      @Valid @RequestBody NormDecisionUpdateDTO dto) {
        return ResponseEntity.ok(normDecisionService.verifyNormDecision(id, dto));
    }
}
