package gotback.controllers;

import gotback.dto.TripCreateDTO;
import gotback.dto.TripResponseDTO;
import gotback.dto.TripUpdateDTO;
import gotback.exceptionhandlers.ExceptionMessageDTO;
import gotback.exceptionhandlers.GotExceptionResponseDTO;
import gotback.services.TripService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/trips")
@RequiredArgsConstructor
@Api(tags = "Trip management")
public class TripController {

    private final TripService tripService;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "View a list of available trips")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = TripResponseDTO.class, responseContainer = "List"),
            @ApiResponse(code = 500, message = "Internal server error", response = ExceptionMessageDTO.class)})
    public List<TripResponseDTO> getAllTrips() {
        return tripService.getAllTrips();
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "View a trip by id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = TripResponseDTO.class),
            @ApiResponse(code = 500, message = "Internal server error", response = ExceptionMessageDTO.class)})
    public ResponseEntity<TripResponseDTO> getTripById(@PathVariable Integer id) {
        return ResponseEntity.ok(tripService.getTripById(id));
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Delete a trip by id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful delete", response = Integer.class),
            @ApiResponse(code = 400, message = "Bad Request", response = GotExceptionResponseDTO.class),
            @ApiResponse(code = 500, message = "Internal server error", response = ExceptionMessageDTO.class)})
    public ResponseEntity<Integer> deleteTrip(@PathVariable Integer id) {
        return ResponseEntity.ok(tripService.deleteTrip(id));
    }

    @DeleteMapping(value = "/resources", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Delete selected trips by id list")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful delete", response = Integer.class,
                    responseContainer = "List"),
            @ApiResponse(code = 400, message = "Bad Request", response = GotExceptionResponseDTO.class),
            @ApiResponse(code = 500, message = "Internal server error", response = ExceptionMessageDTO.class)})
    public ResponseEntity<List<Integer>> deleteChosenTrips(@RequestParam(required = true) List<Integer> ids) {
        List<Integer> deletedIds = tripService.deleteChosenTrips(ids);
        return ResponseEntity.ok(deletedIds);
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Create a new trip")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = Integer.class),
            @ApiResponse(code = 400, message = "Bad Request", response = GotExceptionResponseDTO.class),
            @ApiResponse(code = 500, message = "Internal server error", response = ExceptionMessageDTO.class)})
    public ResponseEntity<Integer> addTrip(@Valid @RequestBody TripCreateDTO dto) {
        return new ResponseEntity<>(tripService.addTrip(dto), HttpStatus.CREATED);
    }

    @PatchMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Update an existing trip by given id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful update", response = Integer.class),
            @ApiResponse(code = 400, message = "Bad Request", response = GotExceptionResponseDTO.class),
            @ApiResponse(code = 500, message = "Internal server error", response = ExceptionMessageDTO.class)})
    public ResponseEntity<Integer> updateTrip(@PathVariable Integer id,
                                              @Valid @RequestBody TripUpdateDTO dto) {
        return ResponseEntity.ok(tripService.updateTrip(id, dto));
    }

    @GetMapping(value = "/{id}/verify", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Request trip verification by trip id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = Integer.class),
            @ApiResponse(code = 400, message = "Bad Request", response = GotExceptionResponseDTO.class),
            @ApiResponse(code = 500, message = "Internal server error", response = ExceptionMessageDTO.class)})
    public ResponseEntity<Integer> requestTripVerification(@PathVariable Integer id,
                                                           @RequestParam(required = false) Integer leaderCardNr) {
        return ResponseEntity.ok(tripService.requestTripVerify(id, leaderCardNr));
    }


}
