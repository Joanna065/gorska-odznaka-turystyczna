package gotback.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import gotback.entities.TouristBadgeEntity;
import gotback.util.ModelMapperUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.sql.Date;
import java.sql.Timestamp;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@ApiModel(description = "Response tourist badge model")
public class TouristBadgeResponseDTO {
    @ApiModelProperty(notes = "Unique tourist badge id", example = "1")
    Integer touristBadgeId;
    @ApiModelProperty(notes = "Tourist badge attainment date", example = "2019-10-05")
    Date attainmentDate;
    @ApiModelProperty(notes = "Tourist badge start date of badge attainment", example = "2019-08-15")
    Timestamp startAttainDate;
    @ApiModelProperty(notes = "Badge which applies to a tourist badge")
    BadgeResponseDTO badge;

    public static TouristBadgeResponseDTO of(TouristBadgeEntity touristBadgeEntity) {
        return TouristBadgeResponseDTO.builder().touristBadgeId(touristBadgeEntity.getTouristBadgeId()).build();
    }

    public static TouristBadgeResponseDTO payloadOf(TouristBadgeEntity touristBadgeEntity) {
        TouristBadgeResponseDTO res = ModelMapperUtil.map(touristBadgeEntity, TouristBadgeResponseDTO.class);
        res.setBadge(ModelMapperUtil.map(touristBadgeEntity.getBadge(), BadgeResponseDTO.class));
        return res;
    }
}
