package gotback.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Getter
@Builder
@ApiModel(description = "Model to update a trip")
public class TripUpdateDTO {
    @ApiModelProperty(notes = "Name of trip", example = "Trip name")
    String name;
    @ApiModelProperty(notes = "Description of trip", example = "Lorem ipsum")
    String description;
}
