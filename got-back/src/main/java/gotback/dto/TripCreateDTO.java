package gotback.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotNull;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Getter
@Builder
@ApiModel(description = "Model to create a new trip")
public class TripCreateDTO {
    @NotNull
    @ApiModelProperty(notes = "Name of trip", example = "Trip name")
    String name;
    @NotNull
    @ApiModelProperty(notes = "Description of trip", example = "Lorem ipsum")
    String description;
    @NotNull
    @ApiModelProperty(notes = "Tourist id to which trip belongs", example = "1")
    Integer touristId;
    @NotNull
    @ApiModelProperty(notes = "Badge norm id to which trip applies", example = "1")
    Integer badgeNormId;
}
