package gotback.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import gotback.entities.AttachmentEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.FieldDefaults;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@ApiModel(description = "Response attachment model")
public class AttachmentResponseDTO {
    @ApiModelProperty(notes = "Unique attachment id", example = "1")
    Integer attachmentId;
    @ApiModelProperty(notes = "Attachment filepath on server", example = "C:/eKsiazeczka/tourist/trip")
    String filepath;

    public static AttachmentResponseDTO of(AttachmentEntity attachmentEntity) {
        return AttachmentResponseDTO.builder().attachmentId(attachmentEntity.getAttachmentId()).build();
    }
}
