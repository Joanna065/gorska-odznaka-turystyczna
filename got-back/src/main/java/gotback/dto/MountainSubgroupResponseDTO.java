package gotback.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import gotback.entities.MountainSubgroupEntity;
import gotback.util.ModelMapperUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.FieldDefaults;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@ApiModel(description = "Response mountain subgroup model")
public class MountainSubgroupResponseDTO {
    @ApiModelProperty(notes = "Unique mountain subgroup id", example = "1")
    Integer subgroupId;
    @ApiModelProperty(notes = "Name of mountain subgroup", example = "Beskid Sląski")
    String name;
    @ApiModelProperty(notes = "Mountain group to which subgroup belongs")
    MountainGroupResponseDTO mountainGroup;

    public static MountainSubgroupResponseDTO of(MountainSubgroupEntity subgroupEntity) {
        return MountainSubgroupResponseDTO.builder().subgroupId(subgroupEntity.getSubgroupId()).build();
    }

    public static MountainSubgroupResponseDTO payloadOf(MountainSubgroupEntity subgroupEntity) {
        MountainSubgroupResponseDTO res = ModelMapperUtil.map(subgroupEntity, MountainSubgroupResponseDTO.class);
        res.setMountainGroup(ModelMapperUtil.map(subgroupEntity.getMountainGroup(), MountainGroupResponseDTO.class));
        return res;
    }
}
