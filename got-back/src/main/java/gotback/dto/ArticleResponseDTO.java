package gotback.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import gotback.entities.ArticleEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.sql.Timestamp;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@ApiModel(description = "Response model of an article")
public class ArticleResponseDTO {
    @ApiModelProperty(notes = "Unique article id", example = "1")
    Integer articleId;
    @ApiModelProperty(notes = "Title of article", example = "Article title")
    String title;
    @ApiModelProperty(notes = "Article add date", example = "2019-10-04T23:00:00.000+0000")
    Timestamp addDate;
    @ApiModelProperty(notes = "Article content text", example = "Lorem ipsum")
    String text;
    @ApiModelProperty(notes = "Article update date", example = "2019-10-14T23:00:00.000+0000")
    Timestamp updateDate;
    @ApiModelProperty(notes = "Article keywords", example = "#trip, #mountains")
    String keyWords;

    public static ArticleResponseDTO of(ArticleEntity articleEntity) {
        return ArticleResponseDTO.builder().articleId(articleEntity.getArticleId()).build();
    }
}
