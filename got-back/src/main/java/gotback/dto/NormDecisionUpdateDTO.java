package gotback.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotNull;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Getter
@Builder
@ApiModel(description = "Model to update a norm decision")
public class NormDecisionUpdateDTO {
    @NotNull
    @ApiModelProperty(notes = "Info if norm decision is accepted", example = "false")
    Boolean isAccepted;
    @ApiModelProperty(notes = "Justification of norm decision - should be written if norm was rejected",
            example = "Lorem ipsum")
    String justification;

    @AssertTrue(message = "If justification is not blank then isAccepted should be false")
    private boolean isIsAccepted() {
        if (isAccepted == null) {
            return true;
        }
        return (isAccepted && justification == null) || (!isAccepted && justification != null);
    }

}
