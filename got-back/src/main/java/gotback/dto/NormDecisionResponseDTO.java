package gotback.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import gotback.entities.NormDecisionEntity;
import gotback.util.ModelMapperUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.sql.Date;
import java.sql.Timestamp;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@ApiModel(description = "Response model of a norm decision")
public class NormDecisionResponseDTO {
    @ApiModelProperty(notes = "Unique norm decision id", example = "1")
    Integer normDecisionId;
    @ApiModelProperty(notes = "Norm decision add date", example = "2019-10-04T23:00:00.000+0000")
    Timestamp addDate;
    @ApiModelProperty(notes = "Norm decision response date", example = "2019-10-05")
    Date responseDate;
    @ApiModelProperty(notes = "Info if norm decision is accepted", example = "false")
    Boolean isAccepted;
    @ApiModelProperty(notes = "Justification of norm decision - should be written if norm was rejected",
            example = "Lorem ipsum")
    String justification;
    @ApiModelProperty(notes = "Badge norm to which norm decision applies")
    BadgeNormResponseDTO badgeNorm;

    public static NormDecisionResponseDTO of(NormDecisionEntity normDecisionEntity) {
        return NormDecisionResponseDTO.builder().normDecisionId(normDecisionEntity.getNormDecisionId()).build();
    }

    public static NormDecisionResponseDTO payloadOf(NormDecisionEntity normDecisionEntity) {
        NormDecisionResponseDTO res = ModelMapperUtil.map(normDecisionEntity, NormDecisionResponseDTO.class);
        res.setBadgeNorm(BadgeNormResponseDTO.payloadOf(normDecisionEntity.getBadgeNorm()));
        return res;
    }


}
