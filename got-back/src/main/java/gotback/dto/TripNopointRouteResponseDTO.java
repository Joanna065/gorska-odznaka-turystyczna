package gotback.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import gotback.entities.TripNopointRouteEntity;
import gotback.util.ModelMapperUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
@NoArgsConstructor
@Data
@ApiModel(description = "Response association Trip_NopointRoute model")
public class TripNopointRouteResponseDTO {
    @ApiModelProperty(notes = "Walk date of nopoint route in a trip", example = "2019-10-05")
    Date walkDate;
    @ApiModelProperty(notes = "Ordinal number of route in trip", example = "1")
    Integer lp;
    @ApiModelProperty(notes = "Nopoint route included in a trip")
    NopointRouteResponseDTO nopointRoute;

    public static TripNopointRouteResponseDTO payloadOf(TripNopointRouteEntity tripNopointRoute) {
        TripNopointRouteResponseDTO res = ModelMapperUtil.map(tripNopointRoute, TripNopointRouteResponseDTO.class);
        res.setNopointRoute(ModelMapperUtil.map(tripNopointRoute.getNopointRoute(), NopointRouteResponseDTO.class));
        return res;
    }
}
