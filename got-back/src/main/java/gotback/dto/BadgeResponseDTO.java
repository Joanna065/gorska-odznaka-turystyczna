package gotback.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import gotback.entities.BadgeEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.FieldDefaults;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@ApiModel(description = "Response badge model")
public class BadgeResponseDTO {
    @ApiModelProperty(notes = "Unique badge id", example = "1")
    Integer badgeId;
    @ApiModelProperty(notes = "Type of badge", example = "mała")
    String type;
    @ApiModelProperty(notes = "Degree of badge", example = "brązowa")
    String degree;
    @ApiModelProperty(notes = "Badge desription", example = "Lorem ipsum")
    String description;
    @ApiModelProperty(notes = "Required points amount for a badge", example = "120")
    Integer requiredPointAmount;
    @ApiModelProperty(notes = "Additional requirements for a badge", example = "add requirements")
    String additionalRequirements;

    public static BadgeResponseDTO of(BadgeEntity badgeEntity) {
        return BadgeResponseDTO.builder().badgeId(badgeEntity.getBadgeId()).build();
    }
}
