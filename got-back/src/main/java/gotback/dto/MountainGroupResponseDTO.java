package gotback.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import gotback.entities.MountainGroupEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.FieldDefaults;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@ApiModel(description = "Response mountain group model")
public class MountainGroupResponseDTO {
    @ApiModelProperty(notes = "Unique mountain group id", example = "1")
    Integer groupId;
    @ApiModelProperty(notes = "Name of a mountain group", example = "Beskidy Zachodnie")
    String name;
    @ApiModelProperty(notes = "Description of a mountain group", example = "Lorem ipsum")
    String description;

    public static MountainGroupResponseDTO of(MountainGroupEntity groupEntity) {
        return MountainGroupResponseDTO.builder().groupId(groupEntity.getGroupId()).build();
    }
}
