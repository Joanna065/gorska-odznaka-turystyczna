package gotback.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.Pattern;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Getter
@Builder
@ApiModel(description = "Model to update an article")
public class ArticleUpdateDTO {
    @ApiModelProperty(notes = "Title of article", example = "Updated article title")
    String title;
    @ApiModelProperty(notes = "Article content text", example = "Lorem ipsum")
    String text;
    @Pattern(
            regexp = "^$|(#[a-zA-Z0-9A-z]+,? *)*#[a-zA-Z0-9A-z]+",
            message = "Keywords should match format '#word, #word, #word'."
    )
    @ApiModelProperty(notes = "Article keywords", example = "#trip, #mountains")
    String keyWords;
}
