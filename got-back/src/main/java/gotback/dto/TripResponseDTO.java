package gotback.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import gotback.entities.*;
import gotback.util.ModelMapperUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@ApiModel(description = "Response model of a trip")
public class TripResponseDTO {
    @ApiModelProperty(notes = "Unique trip id", example = "1")
    Integer tripId;
    @ApiModelProperty(notes = "Name of trip", example = "Trip name")
    String name;
    @ApiModelProperty(notes = "Description of trip", example = "Lorem ipsum")
    String description;
    @ApiModelProperty(notes = "Trip create date", example = "2019-10-04T23:00:00.000+0000")
    Timestamp createDate;
    @ApiModelProperty(notes = "Info about leader presence", example = "false")
    Boolean isLeaderPresent;
    @ApiModelProperty(notes = "Verification status of trip", example = "1")
    Integer verificationStatus;
    @ApiModelProperty(notes = "Points sum of trip", example = "30")
    Integer pointSum;
    List<AttachmentResponseDTO> attachments;
    List<TripNopointRouteResponseDTO> tripNopointRoutes;
    List<TripPointRouteResponseDTO> tripPointRoutes;

    public static TripResponseDTO of(TripEntity trip) {
        return TripResponseDTO.builder().tripId(trip.getTripId()).build();
    }

    public static TripResponseDTO payloadOf(TripEntity trip) {
        TripResponseDTO res = ModelMapperUtil.map(trip, TripResponseDTO.class);

        res.setPointSum(calcPointsSum(trip));

        if (trip.getAttachments() != null) {
            res.setAttachments(trip.getAttachments()
                    .stream().map(el -> ModelMapperUtil.map(el, AttachmentResponseDTO.class)).collect(Collectors.toList()));
        }
        if (trip.getTripNopointRoutes() != null) {
            res.setTripNopointRoutes(trip.getTripNopointRoutes().stream()
                    .map(TripNopointRouteResponseDTO::payloadOf).collect(Collectors.toList()));
        }
        if (trip.getTripPointRoutes() != null) {
            res.setTripPointRoutes(trip.getTripPointRoutes().stream()
                    .map(TripPointRouteResponseDTO::payloadOf).collect(Collectors.toList()));
        }
        return res;
    }

    private static int calcPointsSum(TripEntity trip){
        int sum = 0;
        if (trip.getTripPointRoutes() != null && !trip.getTripPointRoutes().isEmpty()) {
            sum += trip.getTripPointRoutes().stream()
                    .collect(Collectors.summarizingInt(TripPointRouteEntity::getPoints)).getSum();
        }
        if (trip.getTripNopointRoutes() != null && !trip.getTripNopointRoutes().isEmpty()) {
            sum += trip.getTripNopointRoutes().stream()
                    .map(TripNopointRouteEntity::getNopointRoute)
                    .collect(Collectors.summarizingInt(NopointRouteEntity::getPoints)).getSum();
        }
        return sum;
    }
}
