package gotback.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import gotback.entities.NopointRouteEntity;
import gotback.util.ModelMapperUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.FieldDefaults;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@ApiModel(description = "Response nopoint route model - custom tourist route")
public class NopointRouteResponseDTO {
    @ApiModelProperty(notes = "Unique nopoint route id", example = "1")
    Integer nopointRouteId;
    @ApiModelProperty(notes = "Points for a nopoint route", example = "3")
    Integer points;
    @ApiModelProperty(notes = "Length of a nopoint route in kilometres", example = "3.2")
    Double length;
    @ApiModelProperty(notes = "Height difference of a nopoint route in metres", example = "30")
    Double heightDiff;
    @ApiModelProperty(notes = "Mountain subgroup to which point route belongs")
    MountainSubgroupResponseDTO mountainSubgroup;
    @ApiModelProperty(notes = "Start point of a route")
    PointResponseDTO startPoint;
    @ApiModelProperty(notes = "End point of a route")
    PointResponseDTO endPoint;

    public static NopointRouteResponseDTO of(NopointRouteEntity nopointRouteEntity) {
        return NopointRouteResponseDTO.builder().nopointRouteId(nopointRouteEntity.getNopointRouteId()).build();
    }

    public static NopointRouteResponseDTO payloadOf(NopointRouteEntity nopointRouteEntity) {
        NopointRouteResponseDTO res = ModelMapperUtil.map(nopointRouteEntity, NopointRouteResponseDTO.class);
        res.setStartPoint(ModelMapperUtil.map(nopointRouteEntity.getStartPoint(), PointResponseDTO.class));
        res.setEndPoint(ModelMapperUtil.map(nopointRouteEntity.getEndPoint(), PointResponseDTO.class));
        res.setMountainSubgroup(MountainSubgroupResponseDTO.payloadOf(nopointRouteEntity.getMountainSubgroup()));
        return res;
    }
}
