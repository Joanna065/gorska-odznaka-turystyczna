package gotback.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import gotback.entities.BadgeNormEntity;
import gotback.util.ModelMapperUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.sql.Date;
import java.util.List;
import java.util.stream.Collectors;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@ApiModel(description = "Response badge norm model")
public class BadgeNormResponseDTO {
    @ApiModelProperty(notes = "Unique badge norm id", example = "1")
    Integer normId;
    @ApiModelProperty(notes = "Badge norm attainment date", example = "2019-10-05")
    Date attainmentDate;
    @ApiModelProperty(notes = "Required points amount for a badge norm", example = "45")
    Integer requiredPointAmount;
    @ApiModelProperty(notes = "Gained points amount for a badge norm", example = "45")
    Integer gainedPoints;
    @ApiModelProperty(notes = "Info if badge norm is verified", example = "false")
    Boolean isVerified;
    @ApiModelProperty(notes = "Tourist badge to which badge norm applies")
    TouristBadgeResponseDTO touristBadge;
    @ApiModelProperty(notes = "Trips which are included in badge norm")
    List<TripResponseDTO> trips;

    public static BadgeNormResponseDTO of(BadgeNormEntity badgeNormEntity) {
        return BadgeNormResponseDTO.builder().normId(badgeNormEntity.getNormId()).build();
    }

    public static BadgeNormResponseDTO payloadOf(BadgeNormEntity badgeNormEntity) {
        BadgeNormResponseDTO res = ModelMapperUtil.map(badgeNormEntity, BadgeNormResponseDTO.class);
        if (badgeNormEntity.getTouristBadge() != null) {
            res.setTouristBadge(TouristBadgeResponseDTO.payloadOf(badgeNormEntity.getTouristBadge()));
        }
        if (badgeNormEntity.getTrips() != null) {
            res.setTrips(badgeNormEntity.getTrips().stream().map(TripResponseDTO::payloadOf).collect(Collectors.toList()));
        }
        return res;
    }
}
