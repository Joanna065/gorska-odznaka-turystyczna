package gotback.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import gotback.entities.PointEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.FieldDefaults;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@ApiModel(description = "Response point model")
public class PointResponseDTO {
    @ApiModelProperty(notes = "Unique point id", example = "1")
    Integer pointId;
    @ApiModelProperty(notes = "Name of a point", example = "Jasieniowa")
    String name;
    @ApiModelProperty(notes = "Longitude of a point", example = "18.738841")
    Double longitude;
    @ApiModelProperty(notes = "Latitude of a point", example = "49.705636")
    Double latitude;
    @ApiModelProperty(notes = "Point height in metres", example = "320")
    Double height;
    @ApiModelProperty(notes = "Point description", example = "Lorem ipsum")
    String description;

    public static PointResponseDTO of(PointEntity pointEntity) {
        return PointResponseDTO.builder().pointId(pointEntity.getPointId()).build();
    }
}
