package gotback.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import gotback.entities.PointRouteEntity;
import gotback.util.ModelMapperUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.FieldDefaults;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@ApiModel(description = "Response point route model")
public class PointRouteResponseDTO {
    @ApiModelProperty(notes = "Unique point route id", example = "1")
    Integer pointRouteId;
    @ApiModelProperty(notes = "Length of a point route in kilometres", example = "3.2")
    Double length;
    @ApiModelProperty(notes = "Height difference of a point route in metres", example = "30")
    Double heightDiff;
    @ApiModelProperty(notes = "Info if point route is available for tourists", example = "true")
    Boolean isAvailable;
    @ApiModelProperty(notes = "Description of a point route", example = "Lorem ipsum")
    String description;
    @ApiModelProperty(notes = "Mountain subgroup to which point route belongs")
    MountainSubgroupResponseDTO mountainSubgroup;
    @ApiModelProperty(notes = "Start point of a route")
    PointResponseDTO startPoint;
    @ApiModelProperty(notes = "End point of a route")
    PointResponseDTO endPoint;

    public static PointRouteResponseDTO of(PointRouteEntity pointRouteEntity) {
        return PointRouteResponseDTO.builder().pointRouteId(pointRouteEntity.getPointRouteId()).build();
    }

    public static PointRouteResponseDTO payloadOf(PointRouteEntity pointRouteEntity) {
        PointRouteResponseDTO res = ModelMapperUtil.map(pointRouteEntity, PointRouteResponseDTO.class);
        res.setStartPoint(ModelMapperUtil.map(pointRouteEntity.getStartPoint(), PointResponseDTO.class));
        res.setEndPoint(ModelMapperUtil.map(pointRouteEntity.getEndPoint(), PointResponseDTO.class));
        res.setMountainSubgroup(MountainSubgroupResponseDTO.payloadOf(pointRouteEntity.getMountainSubgroup()));
        return res;
    }
}
