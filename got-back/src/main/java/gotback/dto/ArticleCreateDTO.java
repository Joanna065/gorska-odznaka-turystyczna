package gotback.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Getter
@Builder
@ApiModel(description = "Model to create a new article")
public class ArticleCreateDTO {
    @NotNull
    @ApiModelProperty(notes = "Title of article", example = "New article title")
    String title;
    @NotNull
    @ApiModelProperty(notes = "Article content text", example = "Lorem ipsum")
    String text;
    @Pattern(
            regexp = "^$|(#[a-zA-Z0-9A-z]+,? *)*#[a-zA-Z0-9A-z]+",
            message = "Keywords should match format '#word, #word, #word'."
    )
    @ApiModelProperty(notes = "Article keywords", example = "#trip, #mountains")
    String keyWords;
    @NotNull
    @ApiModelProperty(notes = "Employee id who created an article", example = "1")
    Integer employeeId;
}
