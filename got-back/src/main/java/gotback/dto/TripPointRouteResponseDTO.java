package gotback.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import gotback.entities.TripPointRouteEntity;
import gotback.util.ModelMapperUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
@NoArgsConstructor
@Data
@ApiModel(description = "Response association Trip_PointRoute model")
public class TripPointRouteResponseDTO {
    @ApiModelProperty(notes = "Walk date of point route in a trip", example = "2019-10-05")
    Date walkDate;
    @ApiModelProperty(notes = "Ordinal number of route in trip", example = "1")
    Integer lp;
    @ApiModelProperty(notes = "Copied value of points from PointRoutes", example = "3")
    Integer points;
    @ApiModelProperty(notes = "Point route included in a trip")
    PointRouteResponseDTO pointRoute;

    public static TripPointRouteResponseDTO payloadOf(TripPointRouteEntity tripPointRoute) {
        TripPointRouteResponseDTO res = ModelMapperUtil.map(tripPointRoute, TripPointRouteResponseDTO.class);
        res.setPointRoute(ModelMapperUtil.map(tripPointRoute.getPointRoute(), PointRouteResponseDTO.class));
        return res;
    }

}
