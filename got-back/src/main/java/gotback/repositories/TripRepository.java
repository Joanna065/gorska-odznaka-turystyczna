package gotback.repositories;

import gotback.entities.BadgeNormEntity;
import gotback.entities.TripEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface TripRepository extends JpaRepository<TripEntity, Integer> {

    @Transactional
    void deleteByTripIdIn(List<Integer> ids);

    List<TripEntity> findAllByBadgeNorm(BadgeNormEntity badgeNorm);

}
