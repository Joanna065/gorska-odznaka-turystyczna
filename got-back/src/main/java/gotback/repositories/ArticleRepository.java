package gotback.repositories;

import gotback.entities.ArticleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface ArticleRepository extends JpaRepository<ArticleEntity,Integer> {

    @Transactional
    void deleteByArticleIdIn(List<Integer> ids);
}
