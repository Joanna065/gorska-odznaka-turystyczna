package gotback.repositories;

import gotback.entities.LeaderEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LeaderRepository extends JpaRepository<LeaderEntity, Integer> {
}
