package gotback.services;

import gotback.dto.ArticleCreateDTO;
import gotback.dto.ArticleResponseDTO;
import gotback.dto.ArticleUpdateDTO;
import gotback.entities.ArticleEntity;
import gotback.entities.EmployeeEntity;
import gotback.exceptions.ExceptionType;
import gotback.exceptions.GOTException;
import gotback.repositories.ArticleRepository;
import gotback.repositories.EmployeeRepository;
import gotback.util.BeanUtil;
import gotback.util.ModelMapperUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class ArticleService {

    private final ArticleRepository articleRepository;
    private final EmployeeRepository employeeRepository;

    /**
     * @return list of all articles wrapped in ArticleResponseDTO objects or blank list if no articles present
     */
    public List<ArticleResponseDTO> getAllArticles() {
        return articleRepository.findAll().stream()
                .map(el -> ModelMapperUtil.map(el, ArticleResponseDTO.class))
                .collect(Collectors.toList());
    }

    /**
     * @param id id of article to get
     * @return article wrapped in ArticleResponseDTO object or null if article id does not exist
     */
    public ArticleResponseDTO getArticleById(Integer id) {
        return articleRepository.findById(id)
                .map(el -> ModelMapperUtil.map(el, ArticleResponseDTO.class))
                .orElse(null);
    }

    /**
     * @param id  id of article to update
     * @param dto ArticleUpdateDTO object containing changed field values of updated article
     * @return id of updated article
     * @throws GOTException when article of given id does not exist
     */
    public Integer updateArticle(Integer id, ArticleUpdateDTO dto) {
        ArticleEntity article = articleRepository.findById(id)
                .orElseThrow(() -> new GOTException(String.format("Article with id %d does not exist", id),
                        ExceptionType.INVALID_RESOURCE_ID));

        BeanUtils.copyProperties(dto, article, BeanUtil.getNullPropertyNames(dto));
        ArticleEntity responseArticle = articleRepository.save(article);
        log.info("Updated article {}", responseArticle);
        return article.getArticleId();
    }

    /**
     * @param dto ArticleCreateDTO object containing values to create new article
     * @return id of created new article
     * @throws GOTException when given employee id in create article data does not exist
     */
    public Integer addArticle(ArticleCreateDTO dto) {
        ArticleEntity article = ModelMapperUtil.map(dto, ArticleEntity.class);
        Integer employeeId = dto.getEmployeeId();
        EmployeeEntity employee = employeeRepository.findById(employeeId)
                .orElseThrow(() -> new GOTException(String.format("Employee with id %d does not exist", employeeId),
                        ExceptionType.INVALID_RESOURCE_ID));
        article.setEmployeeEntity(employee);

        ArticleEntity newArticle = articleRepository.save(article);
        log.info("Created article: {}", newArticle);
        return newArticle.getArticleId();
    }

    /**
     * @param id id of article to delete
     * @return id of deleted article
     * @throws org.springframework.dao.EmptyResultDataAccessException when given article id does not exist
     */
    public Integer deleteArticle(Integer id) {
        articleRepository.findById(id)
                .orElseThrow(() -> new GOTException(String.format("Article with id %d does not exist", id),
                        ExceptionType.INVALID_RESOURCE_ID));
        articleRepository.deleteById(id);
        log.info("Deleted article id: {}", id);
        return id;
    }

    /**
     * @param articleIds list of article ids to delete
     * @return ids of deleted article ids
     * @throws GOTException when among given article ids are some which does not exist
     */
    public List<Integer> deleteChosenArticles(List<Integer> articleIds) {
        for (Integer id : articleIds) {
            articleRepository.findById(id)
                    .orElseThrow(() -> new GOTException("Among articles ids are some invalid ones",
                            ExceptionType.INVALID_RESOURCE_ID));
        }
        articleRepository.deleteByArticleIdIn(articleIds);
        log.info("Deleted articles ids: {}", articleIds);
        return articleIds;
    }
}
