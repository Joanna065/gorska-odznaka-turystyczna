package gotback.services;

import gotback.dto.NormDecisionResponseDTO;
import gotback.dto.NormDecisionUpdateDTO;
import gotback.entities.BadgeNormEntity;
import gotback.entities.NormDecisionEntity;
import gotback.exceptions.ExceptionType;
import gotback.exceptions.GOTException;
import gotback.repositories.BadgeNormRepository;
import gotback.repositories.NormDecisionRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class NormDecisionService {
    private final NormDecisionRepository normDecisionRepository;
    private final BadgeNormRepository badgeNormRepository;

    /**
     * @return list of all unverified norm decisions wrapped in NormDecisionResponseDTO object or blank list if
     * no norm decisions present
     */
    public List<NormDecisionResponseDTO> getAllUnverifiedNormDecisions() {
        return normDecisionRepository.findAll().stream().filter(norm -> norm.getResponseDate() == null)
                .map(NormDecisionResponseDTO::payloadOf).collect(Collectors.toList());
    }

    /**
     * @param id id of norm decision to get
     * @return norm decision wrapped in NormDecisionResponseDTO object or null if given id does not exist
     */
    public NormDecisionResponseDTO getNormDecisionById(Integer id) {
        NormDecisionEntity norm = normDecisionRepository.findById(id).orElse(null);
        if (norm == null) {
            return null;
        }
        return NormDecisionResponseDTO.payloadOf(norm);
    }

    /**
     * @param id  id of norm decision to verify
     * @param dto NormDecisionUpdateDTO class object containing changed fields values of verified norm decision
     * @return id of verified norm decision
     * @throws GOTException when given id of norm decision does not exist, decision was already verified and should
     *                      not be in verification process again or norm decision has invalid id of badge norm
     */
    public Integer verifyNormDecision(Integer id, NormDecisionUpdateDTO dto) {
        NormDecisionEntity normDecision = normDecisionRepository.findById(id)
                .orElseThrow(() -> new GOTException(String.format("Norm decision with id %d does not exist", id),
                        ExceptionType.INVALID_RESOURCE_ID));
        if (normDecision.getResponseDate() != null) {
            throw new GOTException("Decision was already earlier verified", ExceptionType.ALREADY_VERIFIED_NORM);
        }
        if (Boolean.TRUE.equals(dto.getIsAccepted())) {
            normDecision.setIsAccepted(true);
        } else {
            normDecision.setIsAccepted(false);
            normDecision.setJustification(dto.getJustification());
        }
        normDecision.setResponseDate(Date.valueOf(LocalDate.now()));
        BadgeNormEntity badgeNorm = badgeNormRepository.findById(normDecision.getBadgeNorm().getNormId())
                .orElseThrow(() -> new GOTException(String.format("This norm decision has badge norm with id" +
                        " %d which does not exist", id), ExceptionType.INVALID_RESOURCE_ID));
        normDecisionRepository.save(normDecision);
        badgeNorm.setIsVerified(true);
        badgeNormRepository.save(badgeNorm);
        return normDecision.getNormDecisionId();
    }
}
