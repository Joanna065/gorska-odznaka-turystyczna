package gotback.services;

import gotback.dto.TripCreateDTO;
import gotback.dto.TripResponseDTO;
import gotback.dto.TripUpdateDTO;
import gotback.entities.*;
import gotback.exceptions.ExceptionType;
import gotback.exceptions.GOTException;
import gotback.repositories.*;
import gotback.util.BeanUtil;
import gotback.util.ModelMapperUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class TripService {
    private final TripRepository tripRepository;
    private final TouristRepository touristRepository;
    private final BadgeNormRepository badgeNormRepository;
    private final LeaderRepository leaderRepository;
    private final TripDecisionRepository tripDecisionRepository;
    Random rand = new Random();
    private static final String TRIP_ID_NOT_EXISTS = "Trip with id %d does not exist";

    /**
     * @return list of all trips wrapped in TripResponseDTO objects or blank list if no articles present
     */
    public List<TripResponseDTO> getAllTrips() {
        return tripRepository.findAll().stream().map(TripResponseDTO::payloadOf).collect(Collectors.toList());
    }

    /**
     * @param id id of trip to get
     * @return trip wrapped in TripResponseDTO object or null if given trip id does not exist
     */
    public TripResponseDTO getTripById(Integer id) {
        TripEntity trip = tripRepository.findById(id).orElse(null);
        if (trip == null) {
            return null;
        }
        return TripResponseDTO.payloadOf(trip);
    }

    /**
     * @param id id of trip to delete
     * @return id of deleted trip
     * @throws GOTException when given trip id does not exist or trip cannot be deleted since it is during or after
     *                      verification process
     */
    public Integer deleteTrip(Integer id) {
        TripEntity trip = tripRepository.findById(id)
                .orElseThrow(() -> new GOTException(String.format(TRIP_ID_NOT_EXISTS, id),
                        ExceptionType.INVALID_RESOURCE_ID));
        if (!trip.getVerificationStatus().equals(1)) {
            throw new GOTException("Trip cannot be deleted, verification status is different from 1-unverified",
                    ExceptionType.INVALID_DELETE);
        }
        tripRepository.deleteById(id);
        log.info("Deleted trip id: {}", id);
        return trip.getTripId();
    }

    /**
     * @param tripIds list of trip ids to delete
     * @return ids of deleted trip ids
     * @throws GOTException when among given trip ids are some which does not exist or some of them
     *                      has been already verified or during verification process
     */
    public List<Integer> deleteChosenTrips(List<Integer> tripIds) {
        List<Integer> invalidDeleteIds = new ArrayList<>();
        for (Integer id : tripIds) {
            TripEntity trip = tripRepository.findById(id)
                    .orElseThrow(() -> new GOTException("Among trips ids are some invalid ones",
                            ExceptionType.INVALID_RESOURCE_ID));
            if (!trip.getVerificationStatus().equals(1)) {
                invalidDeleteIds.add(trip.getTripId());
            }
        }
        if (!invalidDeleteIds.isEmpty()) {
            String errorIds = invalidDeleteIds.stream()
                    .map(String::valueOf).collect(Collectors.joining(", ", "[", "]"));
            throw new GOTException("Trips cannot be deleted, some of them have verification status " +
                    "different from 1-unverified, invalid ids: " + errorIds, ExceptionType.INVALID_DELETE);
        }
        tripRepository.deleteByTripIdIn(tripIds);
        log.info("Deleted trip ids: {}", tripIds);
        return tripIds;
    }

    /**
     * @param dto TripCreateDTO object containing values to create new trip
     * @return id of created new trip
     * @throws GOTException when given tourist id or badge norm id in create trip data does not exist or when
     *                      given badge norm does not belong to given tourist
     */
    public Integer addTrip(TripCreateDTO dto) {
        Integer touristId = dto.getTouristId();
        TouristEntity tourist = touristRepository.findById(touristId)
                .orElseThrow(() -> new GOTException(String.format("Tourist with id %d does not exist", touristId),
                        ExceptionType.INVALID_RESOURCE_ID));

        Integer badgeNormId = dto.getBadgeNormId();
        BadgeNormEntity badgeNorm = badgeNormRepository.findById(badgeNormId)
                .orElseThrow(() -> new GOTException(String.format("Badge norm with id %d does not exist", badgeNormId),
                        ExceptionType.INVALID_RESOURCE_ID));

        Integer touristCheckId = badgeNorm.getTouristBadge().getTourist().getTouristId();
        if (!touristCheckId.equals(dto.getTouristId())) {
            throw new GOTException("Badge norm does not belong to that tourist", ExceptionType.WRONG_BELONGING);
        }
        TripEntity trip = ModelMapperUtil.map(dto, TripEntity.class);
        trip.setBadgeNorm(badgeNorm);
        trip.setTourist(tourist);
        trip.setVerificationStatus(1);
        trip.setIsLeaderPresent(false);
        TripEntity newTrip = tripRepository.save(trip);
        log.info("Created trip: {}", newTrip);
        return newTrip.getTripId();
    }

    /**
     * @param id  id of trip to update
     * @param dto TripUpdateDTO object containing changed field values of updated trip
     * @return id of updated trip
     * @throws GOTException when trip of given id does not exist
     */
    public Integer updateTrip(Integer id, TripUpdateDTO dto) {
        TripEntity trip = tripRepository.findById(id)
                .orElseThrow(() -> new GOTException(String.format(TRIP_ID_NOT_EXISTS, id),
                        ExceptionType.INVALID_RESOURCE_ID));

        BeanUtils.copyProperties(dto, trip, BeanUtil.getNullPropertyNames(dto));
        TripEntity responseTrip = tripRepository.save(trip);
        log.info("Updated trip {}", responseTrip);
        return trip.getTripId();
    }

    /**
     * @param id           id of trip to request verification
     * @param leaderCardNr leader card number, can be null to automatically find proper leader
     *                     with rights to verify trip
     * @return id of requested to verify trip
     * @throws GOTException when trip of given id does not exist or trip has been already in verification process or
     *                      when bossiness rules of verification are violated
     */
    public Integer requestTripVerify(Integer id, Integer leaderCardNr) {
        TripEntity trip = tripRepository.findById(id)
                .orElseThrow(() -> new GOTException(String.format(TRIP_ID_NOT_EXISTS, id),
                        ExceptionType.INVALID_RESOURCE_ID));

        // conditions check
        if (!trip.getVerificationStatus().equals(1)) {
            throw new GOTException("Tylko wycieczka o statusie 'NIEZWERYFIKOWANA' może być proszona o weryfikację",
                    ExceptionType.INVALID_VERIFY);
        }
        checkNecessaryDataToVerify(trip);
        checkNoRepeatedRoutesInNorm(trip);
        checkValidDailyNormPoints(trip);

        // handle leader presence
        LeaderEntity leader = getLeader(trip, leaderCardNr);

        // change verification status to 2-waiting and create TripDecision object
        TripDecisionEntity tripDecision = TripDecisionEntity.builder().leader(leader).isAccepted(false).build();
        tripDecisionRepository.save(tripDecision);
        trip.setVerificationStatus(2);
        trip.setTripDecision(tripDecision);
        tripRepository.save(trip);

        return trip.getTripId();
    }

    /**
     * @param trip         TripEntity object
     * @param leaderCardNr leader card number, can be null to automatically find proper leader
     *                     with rights to verify trip
     * @return leader with proper rights to verify given trip id
     * @throws GOTException when given card number does not exist or leader with that card number has no rights
     *                      to verify trip, in case when card number is null exception can be thrown if no leader
     *                      with proper rights exists
     */
    private LeaderEntity getLeader(TripEntity trip, Integer leaderCardNr) {
        List<MountainGroupEntity> tripGroups = trip.getTripPointRoutes().stream()
                .map(TripPointRouteEntity::getPointRoute).map(PointRouteEntity::getMountainSubgroup)
                .map(MountainSubgroupEntity::getMountainGroup).collect(Collectors.toList());
        tripGroups.addAll(trip.getTripNopointRoutes().stream().
                map(TripNopointRouteEntity::getNopointRoute).map(NopointRouteEntity::getMountainSubgroup)
                .map(MountainSubgroupEntity::getMountainGroup).collect(Collectors.toList()));

        LeaderEntity leader = null;
        if (leaderCardNr != null) {
            // check valid card_nr and rights for mountain groups of that leader
            leader = leaderRepository.findById(leaderCardNr)
                    .orElseThrow(() -> new GOTException(
                            String.format("Przodownika o numerze legitymacji %nr nie istnieje",
                                    leaderCardNr), ExceptionType.INVALID_RESOURCE_ID));
            if (!leader.getMountaingGroups().containsAll(tripGroups)) {
                throw new GOTException(
                        "Przodownik nie posiada uprawnień na wszystkie grupy górskie obecne w odcinkach wycieczki",
                        ExceptionType.INVALID_LEADER_RIGHTS);
            }
        } else {
            //check if proper leader exists in database
            List<LeaderEntity> leadersWithRights = leaderRepository.findAll().stream().filter(leaderEntity ->
                    leaderEntity.getMountaingGroups().containsAll(tripGroups)).collect(Collectors.toList());
            if (leadersWithRights.isEmpty()) {
                throw new GOTException(
                        "Nie znaleziono przodownika posiadającego wszystkie odpowiednie uprawnienia do weryfikacji wycieczki",
                        ExceptionType.LEADER_RESOURCE_RIGHTS);
            } else {
                leader = leadersWithRights.get(rand.nextInt(leadersWithRights.size()));
            }
        }
        return leader;
    }

    /**
     * @param trip TripEntity object
     * @throws GOTException when daily norm points business rule for trip is violated
     */
    private void checkValidDailyNormPoints(TripEntity trip) {
        int dailyNorm = 50;
        String badgeType = trip.getBadgeNorm().getTouristBadge().getBadge().getType();
        if (badgeType.equals("w góry")) {
            dailyNorm = 15;
        }
        List<TripPointRouteEntity> tripPointRoutes = trip.getTripPointRoutes();
        List<TripNopointRouteEntity> tripNopointRoutes = trip.getTripNopointRoutes();

        Map<Date, IntSummaryStatistics> pointRouteSummary = tripPointRoutes.stream()
                .collect(Collectors.groupingBy(TripPointRouteEntity::getWalkDate,
                        Collectors.mapping(TripPointRouteEntity::getPointRoute,
                                Collectors.summarizingInt(PointRouteEntity::getPoints))));

        Map<Date, IntSummaryStatistics> nopointRouteSummary = tripNopointRoutes.stream()
                .collect(Collectors.groupingBy(TripNopointRouteEntity::getWalkDate,
                        Collectors.mapping(TripNopointRouteEntity::getNopointRoute,
                                Collectors.summarizingInt(NopointRouteEntity::getPoints))));

        int finalDailyNorm = dailyNorm;
        if (pointRouteSummary.entrySet().stream().anyMatch(r -> r.getValue().getSum() > finalDailyNorm) ||
                nopointRouteSummary.entrySet().stream().anyMatch(r -> r.getValue().getSum() > finalDailyNorm)) {
            throw new GOTException(
                    "Wycieczka narusza maksymalną punktową normę dzienną", ExceptionType.INVALID_DAILY_POINTS_NORM);
        }
    }

    /**
     * @param trip TripEntity object
     * @throws GOTException when trip has no attachments or no routes included in its data
     */
    private void checkNecessaryDataToVerify(TripEntity trip) {
        List<PointRouteEntity> pointRoutes = trip.getTripPointRoutes().stream()
                .map(TripPointRouteEntity::getPointRoute).collect(Collectors.toList());
        List<NopointRouteEntity> nopointRoutes = trip.getTripNopointRoutes().stream()
                .map(TripNopointRouteEntity::getNopointRoute).collect(Collectors.toList());
        List<AttachmentEntity> attachments = trip.getAttachments();

        if (pointRoutes.isEmpty() && nopointRoutes.isEmpty()) {
            throw new GOTException("Tylko wycieczka z dodanymi odcinkami może być przesłana do weryfikacji",
                    ExceptionType.MISSING_DATA);
        }
        if (attachments.isEmpty()) {
            throw new GOTException("Tylko wycieczka udokumentowana załącznikami może być przesłana do weryfikacji",
                    ExceptionType.MISSING_DATA);
        }
    }

    /**
     * @param trip TripEntity object
     * @throws GOTException when tourist badge norm where requested trip is included has other trips in verification
     *                      process which have repeated routes comparing to given trip
     */
    private void checkNoRepeatedRoutesInNorm(TripEntity trip) {
        List<Integer> pointRoutesIds = trip.getTripPointRoutes().stream()
                .map(TripPointRouteEntity::getPointRoute).map(PointRouteEntity::getPointRouteId)
                .collect(Collectors.toList());
        List<Integer> nopointRoutesIds = trip.getTripNopointRoutes().stream()
                .map(TripNopointRouteEntity::getNopointRoute).map(NopointRouteEntity::getNopointRouteId)
                .collect(Collectors.toList());

        List<TripEntity> sameNormTrips = tripRepository.findAllByBadgeNorm(trip.getBadgeNorm());
        sameNormTrips.remove(trip);
        sameNormTrips = sameNormTrips.stream().filter(tripEntity -> !tripEntity.getVerificationStatus().equals(1))
                .collect(Collectors.toList());

        List<Integer> sameNormPointRoutesIds = sameNormTrips.stream().map(TripEntity::getTripPointRoutes)
                .flatMap(List::stream).map(TripPointRouteEntity::getPointRoute)
                .map(PointRouteEntity::getPointRouteId).collect(Collectors.toList());
        List<Integer> sameNormNopointRoutesIds = sameNormTrips.stream().map(TripEntity::getTripNopointRoutes)
                .flatMap(List::stream).map(TripNopointRouteEntity::getNopointRoute)
                .map(NopointRouteEntity::getNopointRouteId).collect(Collectors.toList());

        if (!Collections.disjoint(sameNormPointRoutesIds, pointRoutesIds) ||
                !Collections.disjoint(sameNormNopointRoutesIds, nopointRoutesIds)) {
            throw new GOTException("Wycieczka zawiera odcinki, które już są w procesie weryfikacyjnym w ramach tej" +
                    "samej normy na odznakę albo zostały w ramach niej już zweryfikowane"
                    , ExceptionType.REPEATED_DATA);
        }
    }
}
