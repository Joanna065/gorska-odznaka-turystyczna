package gotback.services;

import gotback.dto.NormDecisionResponseDTO;
import gotback.dto.NormDecisionUpdateDTO;
import gotback.entities.BadgeNormEntity;
import gotback.entities.NormDecisionEntity;
import gotback.exceptions.ExceptionType;
import gotback.exceptions.GOTException;
import gotback.repositories.BadgeNormRepository;
import gotback.repositories.NormDecisionRepository;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@FieldDefaults(level = AccessLevel.PRIVATE)
@RunWith(MockitoJUnitRunner.class)
public class NormDecisionServiceTest {
    NormDecisionEntity normDecision;
    BadgeNormEntity badgeNorm;
    Integer testId;

    @Mock
    NormDecisionRepository normDecisionRepository;
    @Mock
    BadgeNormRepository badgeNormRepository;
    @InjectMocks
    NormDecisionService service;

    @Before
    public void setUp() {
        testId = 1;
        badgeNorm = BadgeNormEntity.builder().normId(1).isVerified(false).build();
        normDecision = NormDecisionEntity.builder().normDecisionId(1).badgeNorm(badgeNorm).build();
    }

    @Test
    public void shouldReturnNormIdWhenGetByIdSuccessful() {
        //given
        NormDecisionResponseDTO expectedNormDecision = NormDecisionResponseDTO.payloadOf(normDecision);
        when(normDecisionRepository.findById(testId)).thenReturn(Optional.of(normDecision));
        //when
        NormDecisionResponseDTO actualNormDecision = service.getNormDecisionById(testId);
        //then
        assertThat(actualNormDecision).isEqualTo(expectedNormDecision);
        verify(normDecisionRepository, times(1)).findById(testId);
    }

    @Test
    public void shouldReturnOnlyUnverifiedNormsWhenGetAllSuccessful() {
        //given
        BadgeNormEntity badgeNorm1 = BadgeNormEntity.builder().normId(2).isVerified(false).build();
        BadgeNormEntity badgeNorm2 = BadgeNormEntity.builder().normId(3).isVerified(false).build();
        NormDecisionEntity normDecision1 = NormDecisionEntity.builder().normDecisionId(2).badgeNorm(badgeNorm1)
                .responseDate(Date.valueOf("2019-12-20")).build();
        NormDecisionEntity normDecision2 = NormDecisionEntity.builder().normDecisionId(3).badgeNorm(badgeNorm2).build();

        List<NormDecisionResponseDTO> expectedNorms = new ArrayList<>();
        expectedNorms.add(NormDecisionResponseDTO.payloadOf(normDecision2));

        List<NormDecisionEntity> normDecisions = new ArrayList<>();
        normDecisions.add(normDecision1);
        normDecisions.add(normDecision2);
        when(normDecisionRepository.findAll()).thenReturn(normDecisions);
        //when
        List<NormDecisionResponseDTO> actualNorms = service.getAllUnverifiedNormDecisions();
        //then
        assertThat(actualNorms).isEqualTo(expectedNorms);
        verify(normDecisionRepository, times(1)).findAll();
    }

    @Test
    public void shouldReturnNormIdWhenVerificationSuccessful() {
        //given
        NormDecisionUpdateDTO updateDto = NormDecisionUpdateDTO.builder().isAccepted(true).build();
        when(normDecisionRepository.findById(testId)).thenReturn(Optional.of(normDecision));
        when(badgeNormRepository.findById(testId)).thenReturn(Optional.of(badgeNorm));
        //when
        Integer responseId = service.verifyNormDecision(testId, updateDto);
        //then
        assertThat(responseId).isEqualTo(normDecision.getNormDecisionId());
        verify(normDecisionRepository, times(1)).save(any(NormDecisionEntity.class));
        verify(badgeNormRepository, times(1)).save(any(BadgeNormEntity.class));
    }

    @Test
    public void shouldThrowGOTExceptionWhenNormDecisionIdNotExistsInVerification() {
        //given
        NormDecisionUpdateDTO updateDto = NormDecisionUpdateDTO.builder().isAccepted(true).build();
        when(normDecisionRepository.findById(0)).thenReturn(Optional.empty());
        //when & then
        GOTException exc = assertThrows(GOTException.class, () ->
                service.verifyNormDecision(0, updateDto));
        ExceptionType excType = exc.getExceptionType();
        assertThat(excType).isEqualTo(ExceptionType.INVALID_RESOURCE_ID);
        verify(normDecisionRepository, times(0)).save(any(NormDecisionEntity.class));
        verify(badgeNormRepository, times(0)).save(any(BadgeNormEntity.class));
    }

    @Test
    public void shouldThrowGOTExceptionWhenDecisionAlreadyVerified() {
        //given
        NormDecisionUpdateDTO updateDto = NormDecisionUpdateDTO.builder().isAccepted(true).build();
        normDecision.setResponseDate(Date.valueOf("2019-12-20"));
        when(normDecisionRepository.findById(testId)).thenReturn(Optional.of(normDecision));
        //when & then
        GOTException exc = assertThrows(GOTException.class, () ->
                service.verifyNormDecision(testId, updateDto));
        ExceptionType excType = exc.getExceptionType();
        assertThat(excType).isEqualTo(ExceptionType.ALREADY_VERIFIED_NORM);
        verify(normDecisionRepository, times(0)).save(any(NormDecisionEntity.class));
        verify(badgeNormRepository, times(0)).save(any(BadgeNormEntity.class));
    }

    @Test
    public void shouldThrowGOTExceptionWhenBadgeNormIdNotExistsInVerification() {
        //given
        NormDecisionUpdateDTO updateDto = NormDecisionUpdateDTO.builder().isAccepted(true).build();
        when(normDecisionRepository.findById(testId)).thenReturn(Optional.of(normDecision));
        //when & then
        GOTException exc = assertThrows(GOTException.class, () ->
                service.verifyNormDecision(testId, updateDto));
        ExceptionType excType = exc.getExceptionType();
        assertThat(excType).isEqualTo(ExceptionType.INVALID_RESOURCE_ID);
        verify(normDecisionRepository, times(0)).save(any(NormDecisionEntity.class));
        verify(badgeNormRepository, times(0)).save(any(BadgeNormEntity.class));
    }
}
