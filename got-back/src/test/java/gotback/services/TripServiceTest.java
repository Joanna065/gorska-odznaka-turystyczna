package gotback.services;

import gotback.dto.TripCreateDTO;
import gotback.dto.TripResponseDTO;
import gotback.dto.TripUpdateDTO;
import gotback.entities.*;
import gotback.exceptions.ExceptionType;
import gotback.exceptions.GOTException;
import gotback.repositories.*;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@FieldDefaults(level = AccessLevel.PRIVATE)
@RunWith(MockitoJUnitRunner.class)
public class TripServiceTest {
    TripEntity trip;
    TouristEntity tourist;
    BadgeNormEntity badgeNorm;
    TouristBadgeEntity touristBadge;
    LeaderEntity leader;
    TripDecisionEntity tripDecision;
    Integer testId;

    @Mock
    TripRepository tripRepository;
    @Mock
    TouristRepository touristRepository;
    @Mock
    BadgeNormRepository badgeNormRepository;
    @Mock
    LeaderRepository leaderRepository;
    @Mock
    TripDecisionRepository tripDecisionRepository;
    @InjectMocks
    TripService service;

    @Before
    public void setUp() throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        testId = 1;
        tourist = TouristEntity.builder().touristId(1).isDisabled(false).build();
        touristBadge = TouristBadgeEntity.builder().touristBadgeId(1).tourist(tourist)
                .badge(BadgeEntity.builder().badgeId(1).type("w góry")
                        .degree("srebrny").requiredPointAmount(30).build()).build();
        badgeNorm = BadgeNormEntity.builder().normId(1).isVerified(false).requiredPointAmount(10)
                .touristBadge(touristBadge).build();

        List<AttachmentEntity> attachments = new ArrayList<>();
        attachments.add(AttachmentEntity.builder().attachmentId(1).filepath("/trip/tourist1").trip(trip).build());

        MountainGroupEntity mountainGroup = MountainGroupEntity.builder().groupId(1).build();
        MountainSubgroupEntity mountainSubgroup = MountainSubgroupEntity.builder().subgroupId(1)
                .mountainGroup(mountainGroup).build();
        PointRouteEntity pointRoute1 = PointRouteEntity.builder().pointRouteId(1).mountainSubgroup(mountainSubgroup)
                .points(15).build();
        PointRouteEntity pointRoute2 = PointRouteEntity.builder().pointRouteId(2).mountainSubgroup(mountainSubgroup)
                .points(15).build();
        List<TripPointRouteEntity> tripPointRoutes = new ArrayList<>();
        tripPointRoutes.add(TripPointRouteEntity.builder().tripId(testId).pointRoute(pointRoute1)
                .walkDate(java.sql.Date.valueOf("2019-12-16")).points(15).build());
        tripPointRoutes.add(TripPointRouteEntity.builder().tripId(testId).pointRoute(pointRoute2)
                .walkDate(java.sql.Date.valueOf("2019-12-17")).points(15).build());
        List<TripNopointRouteEntity> tripNopointRoutes = new ArrayList<>();
        trip = TripEntity.builder().tripId(testId).name("Test trip name").description("Great trip")
                .verificationStatus(1).isLeaderPresent(false).tripDecision(tripDecision).badgeNorm(badgeNorm)
                .tourist(tourist).attachments(attachments).tripPointRoutes(tripPointRoutes)
                .tripNopointRoutes(tripNopointRoutes).build();

        Set<MountainGroupEntity> leaderGroups = new HashSet<>();
        leaderGroups.add(mountainGroup);
        leader = LeaderEntity.builder().cardNr(12345).mountaingGroups(leaderGroups).build();

        Date date = dateFormat.parse("2019-12-20");
        long time = date.getTime();
        Timestamp sendDate = new Timestamp(time);
        tripDecision = TripDecisionEntity.builder().tripDecisionId(1).isAccepted(false).justification(null)
                .sendDate(sendDate).acceptanceDate(null).leader(leader).build();
    }

    @Test
    public void shouldReturnListIfGetAllTrips() {
        //given
        List<TripEntity> trips = new ArrayList<>();
        trips.add(trip);
        List<TripResponseDTO> expectedTrips = trips.stream()
                .map(TripResponseDTO::payloadOf).collect(Collectors.toList());
        // when
        doReturn(trips).when(tripRepository).findAll();
        List<TripResponseDTO> actualTrips = service.getAllTrips();
        // then
        assertThat(actualTrips).isEqualTo(expectedTrips);
        verify(tripRepository, times(1)).findAll();
    }

    @Test
    public void shouldReturnTripIfGetById() {
        //given
        TripResponseDTO expectedTrip = TripResponseDTO.payloadOf(trip);
        doReturn(Optional.of(trip)).when(tripRepository).findById(testId);
        // when
        TripResponseDTO actualTrip = service.getTripById(testId);
        // then
        assertThat(actualTrip).isEqualTo(expectedTrip);
        verify(tripRepository, times(1)).findById(testId);
    }

    @Test
    public void shouldReturnNullifGetTripByInvalidId() {
        //given
        when(tripRepository.findById(0)).thenReturn(Optional.empty());
        // when
        TripResponseDTO actualTrip = service.getTripById(0);
        // then
        assertThat(actualTrip).isEqualTo(null);
        verify(tripRepository, times(1)).findById(0);
    }

    @Test
    public void shouldReturnTripIdWhenUpdateSuccessful() {
        //given
        TripUpdateDTO updateDto = TripUpdateDTO.builder().name("Changed name trip test").build();
        doReturn(Optional.of(trip)).when(tripRepository).findById(testId);
        trip.setName("Changed name trip test");
        when(tripRepository.save(any(TripEntity.class))).thenReturn(trip);
        //when
        Integer responseId = service.updateTrip(testId, updateDto);
        //then
        assertThat(responseId).isEqualTo(trip.getTripId());
        verify(tripRepository, times(1)).save(any(TripEntity.class));
    }

    @Test
    public void shouldThrowGOTExceptionWhenInvalidIdForUpdate() {
        //given
        TripUpdateDTO updateDto = TripUpdateDTO.builder().name("Changed name test").build();
        doReturn(Optional.empty()).when(tripRepository).findById(0);
        //when & then
        GOTException exception = assertThrows(GOTException.class, () -> {
            service.updateTrip(0, updateDto);
        });
        ExceptionType excType = exception.getExceptionType();
        assertThat(excType).isEqualTo(ExceptionType.INVALID_RESOURCE_ID);
        verify(tripRepository, times(0)).save(any(TripEntity.class));
    }

    @Test
    public void shouldReturnTripIdWhenCreateSuccessful() {
        //given
        TripCreateDTO createDto = TripCreateDTO.builder().name("New Trip").description("Hello new trip")
                .touristId(1).badgeNormId(1).build();

        TripEntity newTrip = TripEntity.builder().tripId(2).name(createDto.getName())
                .description(createDto.getDescription()).tourist(tourist).badgeNorm(badgeNorm).build();

        when(touristRepository.findById(createDto.getTouristId())).thenReturn(Optional.of(tourist));
        when(badgeNormRepository.findById(createDto.getBadgeNormId())).thenReturn(Optional.of(badgeNorm));
        when(tripRepository.save(any(TripEntity.class))).thenReturn(newTrip);
        //when
        Integer actualResponse = service.addTrip(createDto);
        //then
        assertThat(actualResponse).isEqualTo(newTrip.getTripId());
        verify(touristRepository, times(1)).findById(createDto.getTouristId());
        verify(tripRepository, times(1)).save(any(TripEntity.class));
    }

    @Test
    public void shouldThrowGOTExceptionWhenAddTripWithInvalidTouristId() {
        //given
        TripCreateDTO createDto = TripCreateDTO.builder().name("New Trip").description("Hello new trip")
                .touristId(1).badgeNormId(1).build();
        when(touristRepository.findById(createDto.getTouristId())).thenReturn(Optional.empty());
        //when & then
        GOTException exc = assertThrows(GOTException.class, () ->
                service.addTrip(createDto));
        ExceptionType excType = exc.getExceptionType();
        assertThat(excType).isEqualTo(ExceptionType.INVALID_RESOURCE_ID);
        verify(tripRepository, times(0)).save(any(TripEntity.class));
    }

    @Test
    public void shouldThrowGOTExceptionWhenAddTripWithInvalidBadgeNormId() {
        //given
        TripCreateDTO createDto = TripCreateDTO.builder().name("New Trip").description("Hello new trip")
                .touristId(1).badgeNormId(0).build();
        //when & then
        GOTException exc = assertThrows(GOTException.class, () ->
                service.addTrip(createDto));
        ExceptionType excType = exc.getExceptionType();
        assertThat(excType).isEqualTo(ExceptionType.INVALID_RESOURCE_ID);
        verify(tripRepository, times(0)).save(any(TripEntity.class));
    }

    @Test
    public void shouldThrowGOTExceptionWhenAddTripWithBadgeNormNotBelongingToTourist() {
        //given
        TouristEntity testTourist = TouristEntity.builder().touristId(20).isDisabled(false).build();
        TouristBadgeEntity touristBadge = TouristBadgeEntity.builder().touristBadgeId(20).tourist(testTourist).build();
        BadgeNormEntity badgeNorm = BadgeNormEntity.builder().normId(1).isVerified(false).requiredPointAmount(10)
                .touristBadge(touristBadge).build();

        TripCreateDTO createDto = TripCreateDTO.builder().name("New Trip").description("Hello new trip")
                .touristId(1).badgeNormId(20).build();
        when(touristRepository.findById(createDto.getTouristId())).thenReturn(Optional.of(tourist));
        when(badgeNormRepository.findById(createDto.getBadgeNormId())).thenReturn(Optional.of(badgeNorm));
        //when & then
        GOTException exc = assertThrows(GOTException.class, () ->
                service.addTrip(createDto));
        ExceptionType excType = exc.getExceptionType();
        assertThat(excType).isEqualTo(ExceptionType.WRONG_BELONGING);
        verify(tripRepository, times(0)).save(any(TripEntity.class));
    }

    @Test
    public void shouldReturnTripIdWhenDeleteSuccessful() {
        //given
        when(tripRepository.findById(trip.getTripId())).thenReturn(Optional.of(trip));
        // when
        Integer responseId = service.deleteTrip(trip.getTripId());
        // then
        verify(tripRepository, times(1)).deleteById(trip.getTripId());
        assertThat(responseId).isEqualTo(trip.getTripId());
    }

    @Test
    public void shouldThrowGOTExceptionWhenDeleteInvalidId() {
        //given
        doReturn(Optional.empty()).when(tripRepository).findById(0);
        //when & then
        GOTException exc = assertThrows(GOTException.class, () ->
                service.deleteTrip(0));
        ExceptionType excType = exc.getExceptionType();
        assertThat(excType).isEqualTo(ExceptionType.INVALID_RESOURCE_ID);
        verify(tripRepository, times(0)).deleteById(0);
    }

    @Test
    public void shouldThrowGOTExceptionWhenDeleteWithVerificationStatusDifferentFromOne() {
        //given
        trip.setVerificationStatus(2);
        when(tripRepository.findById(trip.getTripId())).thenReturn(Optional.of(trip));
        //when & then
        GOTException exc = assertThrows(GOTException.class, () ->
                service.deleteTrip(trip.getTripId()));
        ExceptionType excType = exc.getExceptionType();
        assertThat(excType).isEqualTo(ExceptionType.INVALID_DELETE);
        verify(tripRepository, times(0)).deleteById(trip.getTripId());
    }

    @Test
    public void shouldReturnTripsIdsWhenDeleteChosenSuccessful() {
        //given
        TripEntity trip1 = TripEntity.builder().tripId(1).verificationStatus(1).build();
        TripEntity trip2 = TripEntity.builder().tripId(2).verificationStatus(1).build();
        List<Integer> expectedDeletedIds = new ArrayList<>();
        expectedDeletedIds.add(1);
        expectedDeletedIds.add(2);
        when(tripRepository.findById(1)).thenReturn(Optional.of(trip1));
        when(tripRepository.findById(2)).thenReturn(Optional.of(trip2));
        //when
        List<Integer> actualDeletedIds = service.deleteChosenTrips(expectedDeletedIds);
        //then
        assertThat(actualDeletedIds).isEqualTo(actualDeletedIds);
        verify(tripRepository, times(1)).deleteByTripIdIn(expectedDeletedIds);
    }

    @Test
    public void shouldThrowGOTExceptionWhenDeleteAmongChosenVerificationStatusDiffFromOne() {
        //given
        TripEntity trip1 = TripEntity.builder().tripId(1).verificationStatus(1).build();
        TripEntity trip2 = TripEntity.builder().tripId(2).verificationStatus(3).build();
        List<Integer> expectedDeletedIds = new ArrayList<>();
        expectedDeletedIds.add(1);
        expectedDeletedIds.add(2);
        when(tripRepository.findById(1)).thenReturn(Optional.of(trip1));
        when(tripRepository.findById(2)).thenReturn(Optional.of(trip2));
        //when & then
        GOTException exc = assertThrows(GOTException.class, () ->
                service.deleteChosenTrips(expectedDeletedIds));
        ExceptionType excType = exc.getExceptionType();
        assertThat(excType).isEqualTo(ExceptionType.INVALID_DELETE);
        verify(tripRepository, times(0)).deleteByTripIdIn(expectedDeletedIds);
    }

    @Test
    public void shouldThrowGOTExceptionWhenInvalidIdsAmongChosen() {
        //given
        TripEntity trip1 = TripEntity.builder().tripId(1).verificationStatus(1).build();
        List<Integer> expectedDeletedIds = new ArrayList<>();
        expectedDeletedIds.add(1);
        expectedDeletedIds.add(2);
        when(tripRepository.findById(1)).thenReturn(Optional.of(trip1));
        when(tripRepository.findById(2)).thenReturn(Optional.empty());
        //when & then
        GOTException exc = assertThrows(GOTException.class, () ->
                service.deleteChosenTrips(expectedDeletedIds));
        ExceptionType excType = exc.getExceptionType();
        assertThat(excType).isEqualTo(ExceptionType.INVALID_RESOURCE_ID);
        verify(tripRepository, times(0)).deleteByTripIdIn(expectedDeletedIds);
    }

    @Test
    public void shouldReturnTripIdWhenVerificationSuccessfulNoLeaderPresent() {
        //given
        List<LeaderEntity> leaders = new ArrayList<>();
        leaders.add(leader);
        when(tripRepository.findById(testId)).thenReturn(Optional.of(trip));
        when(leaderRepository.findAll()).thenReturn(leaders);
        //when
        Integer responseId = service.requestTripVerify(testId, null);
        //then
        assertThat(responseId).isEqualTo(trip.getTripId());
        verify(tripDecisionRepository, times(1)).save(any(TripDecisionEntity.class));
        verify(tripRepository, times(1)).save(any(TripEntity.class));
    }

    @Test
    public void shouldReturnTripIdWhenVerificationSuccessfulLeaderCardNrPresent() {
        //given
        Integer leaderCardNr = leader.getCardNr();
        when(tripRepository.findById(testId)).thenReturn(Optional.of(trip));
        when(leaderRepository.findById(leaderCardNr)).thenReturn(Optional.of(leader));
        //when
        Integer responseId = service.requestTripVerify(testId, leaderCardNr);
        //then
        assertThat(responseId).isEqualTo(trip.getTripId());
        verify(tripDecisionRepository, times(1)).save(any(TripDecisionEntity.class));
        verify(tripRepository, times(1)).save(any(TripEntity.class));
    }

    @Test
    public void shouldThrowGOTExceptionWhenNoLeaderWithRightsExistsInVerification() {
        //given
        MountainGroupEntity testMountainGroup = MountainGroupEntity.builder().groupId(0).build();
        Set<MountainGroupEntity> testLeaderGroups = new HashSet<>();
        testLeaderGroups.add(testMountainGroup);
        leader.setMountaingGroups(testLeaderGroups);
        List<LeaderEntity> leaders = new ArrayList<>();
        leaders.add(leader);
        when(tripRepository.findById(testId)).thenReturn(Optional.of(trip));
        when(leaderRepository.findAll()).thenReturn(leaders);
        //when & then
        GOTException exc = assertThrows(GOTException.class, () ->
                service.requestTripVerify(testId, null));
        ExceptionType excType = exc.getExceptionType();
        assertThat(excType).isEqualTo(ExceptionType.LEADER_RESOURCE_RIGHTS);
        verify(tripDecisionRepository, times(0)).save(any(TripDecisionEntity.class));
        verify(tripRepository, times(0)).save(any(TripEntity.class));
    }

    @Test
    public void shouldThrowGOTExceptionWhenLeaderWithPresentCardNrHasInvalidRightsInVerification() {
        //given
        MountainGroupEntity testMountainGroup = MountainGroupEntity.builder().groupId(0).build();
        Set<MountainGroupEntity> testLeaderGroups = new HashSet<>();
        testLeaderGroups.add(testMountainGroup);
        leader.setMountaingGroups(testLeaderGroups);
        when(tripRepository.findById(testId)).thenReturn(Optional.of(trip));
        when(leaderRepository.findById(leader.getCardNr())).thenReturn(Optional.of(leader));
        //when & then
        GOTException exc = assertThrows(GOTException.class, () ->
                service.requestTripVerify(testId, leader.getCardNr()));
        ExceptionType excType = exc.getExceptionType();
        assertThat(excType).isEqualTo(ExceptionType.INVALID_LEADER_RIGHTS);
        verify(tripDecisionRepository, times(0)).save(any(TripDecisionEntity.class));
        verify(tripRepository, times(0)).save(any(TripEntity.class));
    }

    @Test
    public void shouldThrowGOTExceptionWhenLeaderWithPresentCardNrNotExists() {
        //given
        when(tripRepository.findById(testId)).thenReturn(Optional.of(trip));
        when(leaderRepository.findById(9876)).thenReturn(Optional.empty());
        //when & then
        GOTException exc = assertThrows(GOTException.class, () ->
                service.requestTripVerify(testId, 9876));
        ExceptionType excType = exc.getExceptionType();
        assertThat(excType).isEqualTo(ExceptionType.INVALID_RESOURCE_ID);
        verify(tripDecisionRepository, times(0)).save(any(TripDecisionEntity.class));
        verify(tripRepository, times(0)).save(any(TripEntity.class));
    }

    @Test
    public void shouldThrowGOTExceptionWhenAttachmentsMissingInVerification() {
        //given
        Integer leaderCardNr = leader.getCardNr();
        trip.setAttachments(new ArrayList<>());
        when(tripRepository.findById(testId)).thenReturn(Optional.of(trip));
        //when & then
        GOTException exc = assertThrows(GOTException.class, () ->
                service.requestTripVerify(testId, leaderCardNr));
        ExceptionType excType = exc.getExceptionType();
        assertThat(excType).isEqualTo(ExceptionType.MISSING_DATA);
        verify(tripDecisionRepository, times(0)).save(any(TripDecisionEntity.class));
        verify(tripRepository, times(0)).save(any(TripEntity.class));
    }

    @Test
    public void shouldThrowGOTExceptionWhenRoutesMissingInVerification() {
        //given
        Integer leaderCardNr = leader.getCardNr();
        trip.setTripPointRoutes(new ArrayList<>());
        trip.setTripNopointRoutes(new ArrayList<>());
        when(tripRepository.findById(testId)).thenReturn(Optional.of(trip));
        //when & then
        GOTException exc = assertThrows(GOTException.class, () ->
                service.requestTripVerify(testId, leaderCardNr));
        ExceptionType excType = exc.getExceptionType();
        assertThat(excType).isEqualTo(ExceptionType.MISSING_DATA);
        verify(tripDecisionRepository, times(0)).save(any(TripDecisionEntity.class));
        verify(tripRepository, times(0)).save(any(TripEntity.class));
    }

    @Test
    public void shouldThrowGOTExceptionWhenDailyPointsNormInvalidInVerification() {
        //given
        Integer leaderCardNr = leader.getCardNr();
        List<TripPointRouteEntity> tripPointRoutes = trip.getTripPointRoutes();
        for (TripPointRouteEntity route : tripPointRoutes) {
            route.getPointRoute().setPoints(15);
            route.setWalkDate(java.sql.Date.valueOf("2019-12-15"));
        }
        when(tripRepository.findById(testId)).thenReturn(Optional.of(trip));
        // when & then
        GOTException exc = assertThrows(GOTException.class, () ->
                service.requestTripVerify(testId, leaderCardNr));
        ExceptionType excType = exc.getExceptionType();
        assertThat(excType).isEqualTo(ExceptionType.INVALID_DAILY_POINTS_NORM);
        verify(tripDecisionRepository, times(0)).save(any(TripDecisionEntity.class));
        verify(tripRepository, times(0)).save(any(TripEntity.class));
    }
}
