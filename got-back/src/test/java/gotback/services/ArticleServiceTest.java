package gotback.services;

import gotback.dto.ArticleCreateDTO;
import gotback.dto.ArticleResponseDTO;
import gotback.dto.ArticleUpdateDTO;
import gotback.entities.ArticleEntity;
import gotback.entities.EmployeeEntity;
import gotback.exceptions.ExceptionType;
import gotback.exceptions.GOTException;
import gotback.repositories.ArticleRepository;
import gotback.repositories.EmployeeRepository;
import gotback.util.ModelMapperUtil;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.dao.EmptyResultDataAccessException;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@FieldDefaults(level = AccessLevel.PRIVATE)
@RunWith(MockitoJUnitRunner.class)
public class ArticleServiceTest {
    ArticleEntity article;
    Integer testId;

    @Mock
    ArticleRepository articleRepository;
    @Mock
    EmployeeRepository employeeRepository;
    @InjectMocks
    ArticleService service;

    @Before
    public void setUp() throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = dateFormat.parse("2019-12-20");
        long time = date.getTime();
        Timestamp addDate = new Timestamp(time);
        Timestamp updateDate = new Timestamp(time);

        testId = 1;
        article = ArticleEntity.builder().articleId(testId).title("Title of article")
                .addDate(addDate).text("Lorem ipsum").keyWords("#test").updateDate(updateDate).build();
    }

    @Test
    public void shouldReturnListIfGetAllArticles() {
        //given
        List<ArticleEntity> articles = new ArrayList<>();
        articles.add(article);
        List<ArticleResponseDTO> expectedArticles = articles.stream()
                .map(el -> ModelMapperUtil.map(el, ArticleResponseDTO.class))
                .collect(Collectors.toList());
        // when
        doReturn(articles).when(articleRepository).findAll();
        List<ArticleResponseDTO> actualArticles = service.getAllArticles();
        // then
        assertThat(actualArticles).isEqualTo(expectedArticles);
        verify(articleRepository, times(1)).findAll();
    }

    @Test
    public void shouldReturnArticleIfGetById() {
        //given
        ArticleResponseDTO expectedArticle = ModelMapperUtil.map(article, ArticleResponseDTO.class);
        doReturn(Optional.of(article)).when(articleRepository).findById(testId);
        // when
        ArticleResponseDTO actualArticle = service.getArticleById(testId);
        // then
        assertThat(actualArticle).isEqualTo(expectedArticle);
        verify(articleRepository, times(1)).findById(testId);
    }

    @Test
    public void shouldReturnNullifGetArticleByInvalidId() {
        //given
        when(articleRepository.findById(0)).thenReturn(Optional.empty());
        // when
        ArticleResponseDTO actualArticle = service.getArticleById(0);
        // then
        assertThat(actualArticle).isEqualTo(null);
        verify(articleRepository, times(1)).findById(0);
    }

    @Test
    public void shouldReturnArticleIdWhenUpdateSuccessful() {
        //given
        ArticleUpdateDTO updateDto = ArticleUpdateDTO.builder().title("Changed title test").build();
        doReturn(Optional.of(article)).when(articleRepository).findById(testId);
        article.setTitle("Changed title test");
        when(articleRepository.save(any(ArticleEntity.class))).thenReturn(article);
        //when
        Integer responseId = service.updateArticle(testId, updateDto);
        //then
        assertThat(responseId).isEqualTo(article.getArticleId());
        verify(articleRepository, times(1)).save(any(ArticleEntity.class));
    }

    @Test
    public void shouldThrowGOTExceptionWhenInvalidIdForUpdate() {
        //given
        ArticleUpdateDTO updateDto = ArticleUpdateDTO.builder().title("Changed title test").build();
        doReturn(Optional.empty()).when(articleRepository).findById(0);
        //when & then
        GOTException exception = assertThrows(GOTException.class, () -> {
            service.updateArticle(0, updateDto);
        });
        ExceptionType excType = exception.getExceptionType();
        assertThat(excType).isEqualTo(ExceptionType.INVALID_RESOURCE_ID);
        verify(articleRepository, times(0)).save(any(ArticleEntity.class));
    }

    @Test
    public void shouldReturnArticleIdWhenCreateSuccessful() {
        //given
        EmployeeEntity employee = EmployeeEntity.builder().employeeId(1).build();
        ArticleCreateDTO createDto = ArticleCreateDTO.builder().title("New Article").text("Hello new article")
                .keyWords("#test").employeeId(1).build();
        ArticleEntity newArticle = ArticleEntity.builder().articleId(2).title(createDto.getTitle())
                .text(createDto.getText()).keyWords(createDto.getKeyWords()).employeeEntity(employee).build();
        when(employeeRepository.findById(createDto.getEmployeeId())).thenReturn(Optional.of(employee));
        when(articleRepository.save(any(ArticleEntity.class))).thenReturn(newArticle);
        //when
        Integer actualResponse = service.addArticle(createDto);
        //then
        assertThat(actualResponse).isEqualTo(newArticle.getArticleId());
        verify(employeeRepository, times(1)).findById(createDto.getEmployeeId());
        verify(articleRepository, times(1)).save(any(ArticleEntity.class));
    }

    @Test
    public void shouldThrowGOTExceptionWhenAddArticleWithInvalidEmployeeId() {
        //given
        ArticleCreateDTO createDto = ArticleCreateDTO.builder().title("New Article").text("Hello new article")
                .keyWords("test").employeeId(1).build();
        when(employeeRepository.findById(createDto.getEmployeeId())).thenReturn(Optional.empty());
        //when & then
        GOTException exc = assertThrows(GOTException.class, () ->
                service.addArticle(createDto));
        ExceptionType excType = exc.getExceptionType();
        assertThat(excType).isEqualTo(ExceptionType.INVALID_RESOURCE_ID);
        verify(articleRepository, times(0)).save(any(ArticleEntity.class));
    }

    @Test
    public void shouldReturnArticleIdWhenDeleteSuccessful() {
        //given
        when(articleRepository.findById(article.getArticleId())).thenReturn(Optional.of(article));
        // when
        Integer responseDto = service.deleteArticle(article.getArticleId());
        // then
        verify(articleRepository, times(1)).deleteById(article.getArticleId());
        assertThat(responseDto).isEqualTo(article.getArticleId());
    }

    @Test
    public void shouldThrowEmptyDataAccessExceptionWhenDeleteInvalidId() {
        //given
        doThrow(GOTException.class).when(articleRepository).deleteById(0);
        //when & then
        GOTException exc = assertThrows(GOTException.class, () ->
                service.deleteArticle(0));
        ExceptionType excType = exc.getExceptionType();
        assertThat(excType).isEqualTo(ExceptionType.INVALID_RESOURCE_ID);
        verify(articleRepository, times(1)).findById(0);
        verify(articleRepository, times(0)).deleteById(0);
    }

    @Test
    public void shouldReturnArticlesIdsWhenDeleteChosenSuccessful() {
        //given
        ArticleEntity article1 = ArticleEntity.builder().articleId(1).build();
        ArticleEntity article2 = ArticleEntity.builder().articleId(2).build();
        List<Integer> expectedDeletedIds = new ArrayList<>();
        expectedDeletedIds.add(1);
        expectedDeletedIds.add(2);
        when(articleRepository.findById(1)).thenReturn(Optional.of(article1));
        when(articleRepository.findById(2)).thenReturn(Optional.of(article2));
        //when
        List<Integer> actualDeletedIds = service.deleteChosenArticles(expectedDeletedIds);
        //then
        assertThat(actualDeletedIds).isEqualTo(actualDeletedIds);
        verify(articleRepository, times(1)).deleteByArticleIdIn(expectedDeletedIds);
    }

    @Test
    public void shouldThrowGOTExceptionWhenInvalidIdsAMongChosenToDelete() {
        //given
        ArticleEntity article1 = ArticleEntity.builder().articleId(1).build();
        List<Integer> expectedDeletedIds = new ArrayList<>();
        expectedDeletedIds.add(1);
        expectedDeletedIds.add(2);
        when(articleRepository.findById(1)).thenReturn(Optional.of(article1));
        when(articleRepository.findById(2)).thenReturn(Optional.empty());
        //when & then
        GOTException exc = assertThrows(GOTException.class, () ->
                service.deleteChosenArticles(expectedDeletedIds));
        ExceptionType excType = exc.getExceptionType();
        assertThat(excType).isEqualTo(ExceptionType.INVALID_RESOURCE_ID);
        verify(articleRepository, times(0)).deleteByArticleIdIn(expectedDeletedIds);
    }
}
