package gotback.controllers;

import gotback.dto.ArticleResponseDTO;
import gotback.exceptions.ExceptionType;
import gotback.exceptions.GOTException;
import gotback.services.ArticleService;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@FieldDefaults(level = AccessLevel.PRIVATE)
@RunWith(SpringRunner.class)
@WebMvcTest(ArticleController.class)
public class ArticleControllerTest {
    @Autowired
    MockMvc mvc;

    @MockBean
    ArticleService service;
    ArticleResponseDTO article;

    @Before
    public void setUp() throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = dateFormat.parse("2019-12-20");
        long time = date.getTime();
        Timestamp addDate = new Timestamp(time);
        Timestamp updateDate = new Timestamp(time);
        article = ArticleResponseDTO.builder().articleId(1).title("Title of article")
                .addDate(addDate).text("Lorem ipsum").keyWords("#test").updateDate(updateDate).build();
    }

    @Test
    public void shouldReturnArrayIfArticlesPresent() throws Exception {
        List<ArticleResponseDTO> allArticles = Collections.singletonList(article);
        given(service.getAllArticles()).willReturn(allArticles);

        mvc.perform(get("/articles")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].articleId", Matchers.is(article.getArticleId())));
    }

    @Test
    public void shouldReturnEmptyArrayIfNoArticlesPresent() throws Exception {
        List<ArticleResponseDTO> emptyList = new ArrayList<>();
        given(service.getAllArticles()).willReturn(emptyList);

        mvc.perform(get("/articles")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(0)))
                .andExpect(jsonPath("$", Matchers.is(emptyList)));
    }

    @Test
    public void shouldReturnArticleIfGetValidId() throws Exception {
        Integer id = 1;
        given(service.getArticleById(id)).willReturn(article);

        mvc.perform(get("/articles/{id}", id)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.articleId").value(id));
        verify(service, times(1)).getArticleById(1);
        verifyNoMoreInteractions(service);
    }

    @Test
    public void shouldReturnNullIfGetInvalidId() throws Exception {
        Integer id = 0;
        given(service.getArticleById(id)).willReturn(null);
        MvcResult result = mvc.perform(get("/articles/{id}", id)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();
        assert result.getResponse().getContentAsString().equals("");
    }

    @Test
    public void shouldReturn200IfValidUpdate() throws Exception {
        mvc.perform(patch("/articles/{id}", 1)
                .content("{\"title\":\"Updated title\"}")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8"))
                .andExpect(status().isOk());
    }

    @Test
    public void shouldReturn400IfValidationFailsOnUpdate() throws Exception {
        mvc.perform(patch("/articles/{id}", 1)
                .content("{\"keyWords\":\"test\"}")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldReturn201IfArticleCreated() throws Exception {
        mvc.perform(post("/articles")
                .content("{\"title\":\"New article test\",\"text\":\"hello world\"," +
                        "\"keyWords\":\"#test\", \"employeeId\":1}")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8"))
                .andExpect(status().isCreated());
    }

    @Test
    public void shouldReturn200IfDeleteArticleSuccessful() throws Exception {
        given(service.deleteArticle(1)).willReturn(1);
        mvc.perform(delete("/articles/{id}", 1)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void shouldReturn400IfInvalidIdDelete() throws Exception {
        given(service.deleteArticle(1)).willThrow(EmptyResultDataAccessException.class);
        mvc.perform(delete("/articles/{id}", 1))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldReturn200IfValidListIdsDelete() throws Exception {
        List<Integer> listIds = new ArrayList<>();
        listIds.add(1);
        listIds.add(2);
        given(service.deleteChosenArticles(listIds)).willReturn(listIds);
        mvc.perform(delete("/articles/resources?ids=1,2")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.[*]", is(listIds)));
    }

    @Test
    public void shouldReturn400IfInvalidListIdsDelete() throws Exception {
        List<Integer> listIds = new ArrayList<>();
        listIds.add(1);
        listIds.add(2);
        given(service.deleteChosenArticles(listIds))
                .willThrow(new GOTException("Among articles ids are some invalid ones",
                        ExceptionType.INVALID_RESOURCE_ID));
        mvc.perform(delete("/articles/resources?ids=1,2")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.excType", is("INVALID_RESOURCE_ID")));
    }
}
