package gotback.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import gotback.dto.TripCreateDTO;
import gotback.dto.TripResponseDTO;
import gotback.dto.TripUpdateDTO;
import gotback.exceptions.ExceptionType;
import gotback.exceptions.GOTException;
import gotback.services.TripService;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@FieldDefaults(level = AccessLevel.PRIVATE)
@RunWith(SpringRunner.class)
@WebMvcTest(TripController.class)
public class TripControllerTest {
    @Autowired
     MockMvc mvc;

    @MockBean
    TripService service;
    TripResponseDTO trip;
    ObjectMapper mapper = new ObjectMapper();
    ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();

    @Before
    public void setUp() {
        trip = TripResponseDTO.builder().tripId(1).name("Beskidy śląskie").build();
    }

    @Test
    public void shouldReturnArrayIfArticlesPresent() throws Exception {
        List<TripResponseDTO> allTrips = Collections.singletonList(trip);
        given(service.getAllTrips()).willReturn(allTrips);

        mvc.perform(get("/trips")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].tripId", Matchers.is(trip.getTripId())));
    }

    @Test
    public void shouldReturnEmptyArrayIfNoArticlesPresent() throws Exception {
        List<TripResponseDTO> emptyTrips = new ArrayList<>();
        given(service.getAllTrips()).willReturn(emptyTrips);

        mvc.perform(get("/trips")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(0)))
                .andExpect(jsonPath("$", Matchers.is(emptyTrips)));
    }

    @Test
    public void shouldReturnArticleIfGetValidId() throws Exception {
        Integer id = 1;
        given(service.getTripById(id)).willReturn(trip);

        mvc.perform(get("/trips/{id}", id)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.tripId").value(id));
    }

    @Test
    public void shouldReturnNullIfGetInvalidId() throws Exception {
        Integer id = 0;
        given(service.getTripById(id)).willReturn(null);
        MvcResult result = mvc.perform(get("/trips/{id}", id)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();
        assert result.getResponse().getContentAsString().equals("");
    }

    @Test
    public void shouldReturn200IfValidUpdate() throws Exception {
        TripUpdateDTO updateDTO = TripUpdateDTO.builder().name("Updated trip name").build();

        mvc.perform(patch("/trips/{id}", 1)
                .content(ow.writeValueAsString(updateDTO))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void shouldReturn201IfTripCreated() throws Exception {
        TripCreateDTO createDTO = TripCreateDTO.builder().name("New trip").description("hello world")
                .touristId(1).badgeNormId(1).build();
        mvc.perform(post("/trips")
                .content(ow.writeValueAsString(createDTO))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8"))
                .andExpect(status().isCreated());
    }

    @Test
    public void shouldReturn200IfDeleteArticleSuccessful() throws Exception {
        given(service.deleteTrip(1)).willReturn(1);
        mvc.perform(delete("/trips/{id}", 1)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$", is(1)));
    }

    @Test
    public void shouldReturn400IfInvalidIdDelete() throws Exception {
        given(service.deleteTrip(1)).willThrow(EmptyResultDataAccessException.class);
        mvc.perform(delete("/trips/{id}", 1))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldReturn200IfValidListIdsDelete() throws Exception {
        List<Integer> listIds = new ArrayList<>();
        listIds.add(1);
        listIds.add(2);
        given(service.deleteChosenTrips(listIds)).willReturn(listIds);
        mvc.perform(delete("/trips/resources?ids=1,2")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.[*]", is(listIds)));
    }

    @Test
    public void shouldReturn400IfInvalidListIdsDelete() throws Exception {
        List<Integer> listIds = new ArrayList<>();
        listIds.add(1);
        listIds.add(2);
        given(service.deleteChosenTrips(listIds))
                .willThrow(new GOTException("Among trips ids are some invalid ones",
                        ExceptionType.INVALID_RESOURCE_ID));
        mvc.perform(delete("/trips/resources?ids=1,2")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.excType", is("INVALID_RESOURCE_ID")));
    }

    @Test
    public void shouldVerifyTripIfValidConditions() throws Exception {
        given(service.requestTripVerify(1, 12345)).willReturn(1);
        mvc.perform(get("/trips/{id}/verify", 1)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

}
