import {Component, OnInit} from '@angular/core';
import {SideNavItem} from './menu-model/SideNavItem';
import {SideNavSection} from './menu-model/SideNavSection';
import {Role} from '../utilities/models/Role';
import {RoleService} from '../utilities/services/role/role.service';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css']
})
export class SidenavComponent implements OnInit {

  private leaderSection: SideNavSection = {
    role: Role.Leader,
    title: 'PRZODOWNIK',
    items: [
      {name: 'Prośby o potwierdzenie wycieczek', url: 'trip-requests'}
    ]
  };

  private touristSection: SideNavSection = {
    role: Role.Tourist,
    title: 'TURYSTA',
    items: [
      { name: 'Wycieczki', url: '/trips'},
      { name: 'Moje odznaki', url: '/my-badges'},
      { name: 'Moje dane', url: '/my-data'}
    ]};

  private employeeSection: SideNavSection = {
    role: Role.Employee,
    title: 'PRACOWNIK',
    items: [
      { name: 'Prośby o potwierdzenie norm', url: '/norms'},
      { name: 'Trasy punktowane', url: 'routes'},
      { name: 'Aktualności', url: '/articles'},
      { name: 'Moje dane', url: 'my-data'}
    ]
  };

  private infoSection: SideNavSection = {
    title: 'INFO',
    items: [
      {name: 'Trasy punktowane', url: '/routes'},
      {name: 'Aktualności', url: '/articles'},
      {name: 'Odznaki', url: '/badges'},
      {name: 'Regulamin GOT', url: '/terms'}
    ]};

  selectedItem: SideNavItem = {name: 'Wycieczki', url: '/trips'};

  role: Role;

  constructor(private roleService: RoleService) { }

  ngOnInit() {
    // subscribe to role changes
    this.roleService.currentRole.asObservable().subscribe({
      next: role => {
        this.role = role;
      }
    });
  }

  getMenuSections(): SideNavSection[] {
    switch (this.role) {
      case Role.Guest:
        return [this.infoSection];
      case Role.Leader:
        return [this.leaderSection, this.touristSection, this.infoSection];
      case Role.Tourist:
        return [this.touristSection, this.infoSection];
      case Role.Employee:
        return [this.employeeSection, this.infoSection];
    }
  }

  clearRole() {
    this.roleService.setNewRole(Role.Guest);
  }

}
