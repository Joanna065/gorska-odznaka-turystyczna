import {SideNavItem} from './SideNavItem';
import {Role} from '../../utilities/models/Role';

export interface SideNavSection {
  role?: Role;
  title: string;
  items: SideNavItem[];
}
