import {Component, OnInit} from '@angular/core';
import {Role} from '../utilities/models/Role';
import {Router} from '@angular/router';
import {RoleService} from '../utilities/services/role/role.service';

@Component({
  selector: 'app-choose-role',
  templateUrl: './choose-role.component.html',
  styleUrls: ['./choose-role.component.css']
})
export class ChooseRoleComponent implements OnInit {

  numberOfRoleToChoose: number;

  constructor(private roleService: RoleService, private router: Router) { }

  ngOnInit() {
  }

  chooseRole() {
    switch (this.numberOfRoleToChoose) {
      case Role.Guest:
        this.roleService.setNewRole(Role.Guest);
        break;
      case Role.Tourist:
        this.roleService.setNewRole(Role.Tourist);
        break;
      case Role.Leader:
        this.roleService.setNewRole(Role.Leader);
        break;
      case Role.Employee:
        this.roleService.setNewRole(Role.Employee);
        break;
    }

    this.router.navigateByUrl('/landing');
  }
}
