import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {TripService} from '../trip-service/trip.service';
import {Router} from '@angular/router';
import {HttpErrorResponse} from '@angular/common/http';
import {GOTException} from '../../utilities/models/GOTException';

@Component({
  selector: 'app-new-trip',
  templateUrl: './new-trip.component.html',
  styleUrls: ['./new-trip.component.css']
})
export class NewTripComponent implements OnInit {

  newTripForm: FormGroup;

  get name() { return this.newTripForm.get('name'); }

  constructor(private router: Router, private tripService: TripService) { }

  private static getNewFormGroup(): FormGroup {
    return new FormGroup({
      name: new FormControl('', Validators.required),
      description: new FormControl('')
    });
  }

  ngOnInit() {
    this.newTripForm = NewTripComponent.getNewFormGroup();
  }

  addNewTrip() {
    this.tripService.addTrip({
      badgeNormId: 1,
      name: this.newTripForm.value.name,
      description: this.newTripForm.value.description,
      touristId: 1
    }).subscribe({
      next: () => {
        this.router.navigateByUrl('/trips').then(() => {
          this.tripService.openSnackBar('success', 'Dodano nową wycieczkę!');
        });
      },
      error: (error: HttpErrorResponse) => {
        this.tripService.openSnackBar('error', (error.error as GOTException).message);
      }
    });
  }

}
