import {NoPointRoute, PointRoute} from './route';
import {MountainGroup} from './point';

/**
 * Compares two Date objects.
 * @param date1 First date object to compare.
 * @param date2 Second date object to compare.
 * @return 0 if the two dates are equal.
 *         1 if the first date is greater than second.
 *         -1 if the first date is less than second.
 */
export function compareDates(date1: Date, date2: Date): number {
  // With Date object we can compare dates them using the >, <, <= or >=.
  // The ==, !=, ===, and !== operators require to use date.getTime(),
  // so we need to create a new instance of Date with 'new Date()'
  const d1 = new Date(date1);
  const d2 = new Date(date2);

  // Check if the dates are equal
  const same = d1.getTime() === d2.getTime();
  if (same) {
    return 0;
  }

  // Check if the first is greater than second
  if (d1 > d2) {
    return 1;
  }

  // Check if the first is less than second
  if (d1 < d2) {
    return -1;
  }
}

export interface TripResponseDTO {
  tripId: number;
  name: string;
  description: string;
  createDate: string;
  isLeaderPresent: boolean;
  verificationStatus: number;
  pointSum: number;
  attachments: Attachement[];
  tripPointRoutes: TripPointRouteResponseDTO[];
  tripNopointRoutes: TripNoPointRouteResponseDTO[];
}

export class Trip {

  constructor(dto: TripResponseDTO) {
    this.tripId = dto.tripId;
    this.name = dto.name;
    this.description = dto.description;
    this.createDate = new Date(dto.createDate);
    this.isLeaderPresent = dto.isLeaderPresent;
    this.verificationStatus = dto.verificationStatus;
    this.pointSum = dto.pointSum;
    this.attachments = dto.attachments;
    this.tripPointRoutes = dto.tripPointRoutes ? dto.tripPointRoutes
      .map(routeDTO => new TripPointRoute(routeDTO)) : [];

    this.tripNopointRoutes = dto.tripNopointRoutes ? dto.tripNopointRoutes
      .map(routeDTO => new TripNopointRoute(routeDTO)) : [];
  }

  tripId: number;
  name: string;
  description: string;
  createDate: Date;
  isLeaderPresent: boolean;
  verificationStatus: number;
  pointSum: number;
  attachments: Attachement[];
  tripPointRoutes: TripPointRoute[];
  tripNopointRoutes: TripNopointRoute[];

  get mountainGroup(): MountainGroup | null {
    const mountainGroups: Set<MountainGroup> = new Set<MountainGroup>();

    if (this.tripPointRoutes.length > 0) {
      this.tripPointRoutes.forEach(route => mountainGroups.add(route.pointRoute.mountainSubgroup.mountainGroup));
    }

    if (this.tripNopointRoutes.length > 0) {
      this.tripNopointRoutes.forEach(route => mountainGroups.add(route.nopointRoute.mountainSubgroup.mountainGroup));
    }

    return mountainGroups.values().next().value ? mountainGroups.values().next().value : null;
  }

  get verificationStatusString(): string {
    switch (this.verificationStatus) {
      case VerificationStatus.NONE:
        return '';
      case VerificationStatus.UNVERIFIED:
        return 'NIEZWERYFIKOWANA';
      case VerificationStatus.AWAITING:
        return 'OCZEKUJĄCA';
      case VerificationStatus.CONFIRMED:
        return 'ZWERYFIKOWANA';
      case VerificationStatus.REJECTED:
        return 'ODRZUCONA';
    }
  }

  get verificationStatusColor(): string {
    switch (this.verificationStatus) {
      case VerificationStatus.NONE:
        return '#303030';
      case VerificationStatus.UNVERIFIED:
        return '#303030';
      case VerificationStatus.AWAITING:
        return '#816C06';
      case VerificationStatus.CONFIRMED:
        return '#265c1d';
      case VerificationStatus.REJECTED:
        return '#470000';
    }
  }

  get mountainAndDatesText(): string {
    let result = '';

    if (this.mountainGroup) {
      result += this.mountainGroup.name;
      result += ', ';
    }

    if (this.dateOfFirstRoute && this.dateOfLastRoute) {
      const firstDate = this.dateOfFirstRoute;
      const lastDate = this.dateOfLastRoute;

      if (compareDates(firstDate, lastDate) === 0) {
        result += firstDate.toLocaleDateString();
      } else {
        result += firstDate.toLocaleDateString();
        result += ' - ';
        result += lastDate.toLocaleDateString();
      }
    }

    return result;
  }

  get dateOfFirstRoute(): Date | null {
    let earliestDate: Date = null;
    if (this.tripPointRoutes.length > 0) {
      this.tripPointRoutes.sort((a: TripPointRoute, b: TripPointRoute) => {
        return compareDates(a.walkDate, b.walkDate);
      });
      earliestDate = this.tripPointRoutes[0].walkDate;
    }

    if (this.tripNopointRoutes.length > 0) {
      this.tripNopointRoutes.sort((a: TripNopointRoute, b: TripNopointRoute) => {
        return compareDates(a.walkDate, b.walkDate);
      });

      if (compareDates(earliestDate, this.tripNopointRoutes[0].walkDate) === 1) {
        earliestDate = this.tripNopointRoutes[0].walkDate;
      }
    }

    return earliestDate;
  }

  get dateOfLastRoute(): Date | null {
    let latestDate: Date = null;

    if (this.tripPointRoutes.length > 0) {
      this.tripPointRoutes.sort((a: TripPointRoute, b: TripPointRoute) => {
        return compareDates(a.walkDate, b.walkDate) * (-1);
      });
      latestDate = this.tripPointRoutes[0].walkDate;
    }

    if (this.tripNopointRoutes.length > 0) {
      this.tripNopointRoutes.sort((a: TripNopointRoute, b: TripNopointRoute) => {
        return compareDates(a.walkDate, b.walkDate) * (-1);
      });

      if (compareDates(latestDate, this.tripNopointRoutes[0].walkDate) === -1) {
        latestDate = this.tripNopointRoutes[0].walkDate;
      }
    }

    return latestDate;
  }

  get numberOfDays(): number {
    if (this.dateOfFirstRoute === null || this.dateOfLastRoute === null) {
      return 0;
    } else {
      return Math.round((this.dateOfLastRoute.valueOf() - this.dateOfFirstRoute.valueOf()) / 86400000) + 1;
    }
  }

  get canBeEdited(): boolean {
    return this.verificationStatus === VerificationStatus.UNVERIFIED;
  }

  get hasRoutes(): boolean {
    return (this.tripNopointRoutes.length + this.tripPointRoutes.length) > 0;
  }
}

export interface TripUpdateDTO {
  name: string;
  description: string;
}

export interface TripCreateDTO {
  badgeNormId: number;
  description: string;
  name: string;
  touristId: number;
}

export enum VerificationStatus {
  NONE, UNVERIFIED, AWAITING, CONFIRMED, REJECTED
}

export interface Attachement {
  attachementId: number;
  filepath: string;
}

// TODO: common interface for routes

export interface TripPointRouteResponseDTO {
  lp: number;
  walkDate: string;
  pointRoute: PointRoute;
  points: number;
}

export class TripPointRoute {
  constructor(dto: TripPointRouteResponseDTO) {
    this.lp = dto.lp;
    this.walkDate = new Date(dto.walkDate);
    this.pointRoute = dto.pointRoute;
    this.points = dto.points;
  }

  lp: number;
  walkDate: Date;
  pointRoute: PointRoute;
  points: number;

  get heightDiff(): number {
    return this.pointRoute.endPoint.height - this.pointRoute.startPoint.height;
  }
}

export interface TripNoPointRouteResponseDTO {
  lp: number;
  walkDate: string;
  nopointRoute: NoPointRoute;
}

export class TripNopointRoute {
  constructor(dto: TripNoPointRouteResponseDTO) {
    this.lp = dto.lp;
    this.walkDate = new Date(dto.walkDate);
    this.nopointRoute = dto.nopointRoute;
  }

  lp: number;
  walkDate: Date;
  nopointRoute: NoPointRoute;
}


