export interface MountainSubgroup {
  subgroupId: number;
  name: string;
  mountainGroup: MountainGroup;
}

export interface MountainGroup {
  groupId: number;
  name: string;
}

export interface Point {
  pointId: number;
  name: string;
  longitude: number;
  latitude: number;
  height: number;
}
