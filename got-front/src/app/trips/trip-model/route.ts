import {MountainSubgroup, Point} from './point';

export interface PointRoute {
  pointRouteId: number;
  points: number;
  length: number;
  isAvailable: boolean;
  mountainSubgroup: MountainSubgroup;
  startPoint: Point;
  endPoint: Point;
}

export interface NoPointRoute {
  nopointRouteId: number;
  points: number;
  length: number;
  heightDiff: number;
  mountainSubgroup: MountainSubgroup;
  startPoint: Point;
  endPoint: Point;
}
