import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Trip, TripCreateDTO, TripResponseDTO, TripUpdateDTO} from '../trip-model/trip';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {MatSnackBar} from '@angular/material';
import {SnackBarComponent} from '../../utilities/components/snack-bar/snack-bar.component';

@Injectable({
  providedIn: 'root'
})
export class TripService {

  constructor(private http: HttpClient, private snackBar: MatSnackBar) { }

  private baseUrl = 'http://localhost:8080/trips';

  public static getErrorMessage(errorStatus: number): string {
    let result;
    switch (errorStatus) {
      case 400:
        result = 'Wystąpił błąd.';
        break;
      case 401:
        result = 'Unauthorized.';
        break;
      case 402:
        result = 'Forbidden.';
        break;
      case 404:
        result = 'Not found.';
        break;
      default:
        result = 'Wystąpił błąd.';
        break;
    }
    return result;
  }

  public getAllTrips(): Observable<HttpResponse<Trip[]>> {
    return this.http.get<TripResponseDTO[]>(this.baseUrl, {observe: 'response'}).pipe(
      map(response => {
        return new HttpResponse<Trip[]>({
          body: response.body.map(tripDTO => new Trip(tripDTO)),
          headers: response.headers,
          status: response.status,
          statusText: response.statusText
        });
      })
    );
  }

  public getTripById(id: number): Observable<HttpResponse<Trip>> {
    return this.http.get<TripResponseDTO>(this.baseUrl + `/${id}`, {observe: 'response'}).pipe(
      map(response => {
        return new HttpResponse<Trip>({
          body: new Trip(response.body),
          headers: response.headers,
          status: response.status,
          statusText: response.statusText
        });
      })
    );
  }

  public addTrip(dto: TripCreateDTO): Observable<HttpResponse<any>> {
    return this.http.post(this.baseUrl, dto,  {observe: 'response'});
  }

  public updateTrip(id: number, dto: TripUpdateDTO): Observable<HttpResponse<Trip>> {
    return this.http.patch<TripResponseDTO>(this.baseUrl + `/${id}`, dto, {observe: 'response'}).pipe(
      map(response => {
        return new HttpResponse<Trip>({
          body: new Trip(response.body),
          headers: response.headers,
          status: response.status,
          statusText: response.statusText
        });
      })
    );
  }

  public deleteTrip(tripId: number): Observable<HttpResponse<Trip>> {
    return this.http.delete<TripResponseDTO>(this.baseUrl + `/${tripId}`, {observe: 'response'}).pipe(
      map(response => {
        return new HttpResponse<Trip>({
          body: new Trip(response.body),
          headers: response.headers,
          status: response.status,
          statusText: response.statusText
        });
      })
    );
  }

  public deleteTrips(tripIds: number[]): Observable<HttpResponse<number[]>> {
    let url = this.baseUrl + '/resources?';
    tripIds.forEach(tripId => {
      url += `ids=${tripId}&`;
    });

    return this.http.delete<number[]>(url,  {observe: 'response'});
  }

  public requestTripVerification(tripId: number, leaderCardNumber: number): Observable<HttpResponse<any>> {
    let url = `${this.baseUrl}/${tripId}/verify`;

    if (leaderCardNumber) {
      url += `?leaderCardNr=${leaderCardNumber}`;
    }

    return this.http.get<TripResponseDTO>(url, {observe: 'response'});
  }

  openSnackBar(requestStatus: string, requestMessage: string) {
    this.snackBar.openFromComponent(SnackBarComponent,
      {
        duration: 3500, data: {status: requestStatus, message: requestMessage},
        panelClass: requestStatus === 'success' ? 'snack-bar-green' : 'snack-bar-red'
      });
  }
}
