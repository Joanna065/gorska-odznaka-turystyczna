import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmDeletingTripsComponent } from './confirm-deleting-trips.component';

describe('ConfirmDeletingTripsComponent', () => {
  let component: ConfirmDeletingTripsComponent;
  let fixture: ComponentFixture<ConfirmDeletingTripsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmDeletingTripsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmDeletingTripsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
