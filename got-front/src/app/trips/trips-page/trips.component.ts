import {AfterViewChecked, Component, OnInit, QueryList, ViewChildren} from '@angular/core';
import {MatDialog} from '@angular/material';
import {ConfirmDeletingTripsComponent} from '../confirm-deleting-trips/confirm-deleting-trips.component';
import {Trip} from '../trip-model/trip';
import {TripService} from '../trip-service/trip.service';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {TripCardComponent} from '../trip-card/trip-card.component';
import {Observer} from 'rxjs';

@Component({
  selector: 'app-trips',
  templateUrl: './trips.component.html',
  styleUrls: ['./trips.component.css']
})
export class TripsComponent implements OnInit, AfterViewChecked {

  constructor(private tripService: TripService, public dialog: MatDialog) { }

  errorHasOccured = false;
  errorMessage: string;
  massEditing = false;
  selectedTrips: number[] = [];

  @ViewChildren(TripCardComponent) tripCards: QueryList<TripCardComponent>;
  trips: Trip[];

  tripsObserver: Observer<HttpResponse<Trip[]>> = {
    next: (response: HttpResponse<Trip[]>) => {
      this.trips = response.body;
      console.log(this.trips);
      this.errorHasOccured = false;
    },
    error: (error: HttpErrorResponse) => {
      console.log('error has occured:');
      console.log(error);
      this.errorHasOccured = true;
      this.errorMessage = TripService.getErrorMessage(error.status);
    },
    complete: () => {}
  };

  ngOnInit() {
    this.tripService.getAllTrips().subscribe(this.tripsObserver);
  }

  // TODO: very bad hack - this gets executed multiple times!
  ngAfterViewChecked() {
    console.log('ng after view checked begin');
    this.tripCards.forEach(tripCard => {
      // attach listeners only if there isn't one yet
      if (tripCard.checked.observers.length === 0) {
        tripCard.checked.subscribe((change) => {
          const indexOfChangedNumber = this.selectedTrips.indexOf(change.tripId);

          // if the article has been selected and isn't added yet, add it
          if (change.isSelected) {
            if (indexOfChangedNumber < 0) {
              this.selectedTrips.push(change.tripId);
            }
          } else { // if the article has been deselected and has already been added, delete it
            if (indexOfChangedNumber >= 0) {
              this.selectedTrips.splice(indexOfChangedNumber, 1);
            }
          }
        });
      }
    });
  }

  openDialogToConfirmDeletingTrips(): void {
    const dialogRef = this.dialog.open(ConfirmDeletingTripsComponent, {width: '600px'});

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.deleteSelectedTrips();
      } else {
        this.tripService.openSnackBar('success', 'Anulowano usunięcie wycieczek');
        this.unsetMassEditing();
      }
    });
  }

  setMassEditing() {
    this.massEditing = true;
  }

  unsetMassEditing() {
    this.massEditing = false;
    this.selectedTrips = [];
  }

  deleteSelectedTrips() {
    this.tripService.deleteTrips(this.selectedTrips).subscribe({
      next: () => {
        this.tripService.openSnackBar('success', 'Usunięto wycieczki!');
        this.unsetMassEditing();
        this.tripService.getAllTrips().subscribe(this.tripsObserver);
      },
      error: (response: HttpErrorResponse) => {
        this.tripService.openSnackBar('error', response.message);
        this.unsetMassEditing();
      }
    });
  }

}
