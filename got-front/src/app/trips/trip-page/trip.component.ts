import {Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material';
import {AskForConfirmationComponent} from './ask-for-confirmation/ask-for-confirmation.component';
import {Role} from '../../utilities/models/Role';
import {RoleService} from '../../utilities/services/role/role.service';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {TripService} from '../trip-service/trip.service';
import {Trip} from '../trip-model/trip';
import {GOTException} from '../../utilities/models/GOTException';
import {Observer} from 'rxjs';

@Component({
  selector: 'app-trip',
  templateUrl: './trip.component.html',
  styleUrls: ['./trip.component.css']
})
export class TripComponent implements OnInit {

  constructor(private route: ActivatedRoute,
              private router: Router,
              private roleService: RoleService,
              private tripService: TripService,
              public dialog: MatDialog) { }

  trip: Trip;
  hasBeenChanged = false;

  editingName = false;
  newName: string;

  editingDescription = false;
  newDescription: string;

  photoIndexes: number[];
  currentRole: Role;

  hasErrorOccured = false;
  errorMessage: string;

  // hack to use enum in component's HTML
  Roles: typeof Role = Role;

  private tripObserver: Observer<HttpResponse<Trip>> = {
    next: (response: HttpResponse<Trip>) => {
      console.log('trip observer');

      this.trip = response.body;

      this.trip.tripPointRoutes = this.trip.tripPointRoutes.sort((a, b) => a.lp - b.lp);

      this.newName = this.trip.name;
      this.newDescription = this.trip.description;
    },
    error: (error: HttpErrorResponse) => {
      this.hasErrorOccured = true;
      this.errorMessage = TripService.getErrorMessage(error.status);
    },
    complete: () => {}
  };

  private static choosePhotoIndexes(): number[] {
    const indexes = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    const result = [];

    for (let i = 0; i < 4; i++) {
      const indexToAdd = (Math.random() * (indexes.length - 1)) + 1;
      result.push(indexes.splice(indexToAdd, 1));
    }

    return result;
  }

  ngOnInit() {
    console.log(this.route);
    this.route.paramMap.subscribe(params => {

      this.tripService.getTripById(Number(params.get('id'))).subscribe(this.tripObserver);
    });

    this.roleService.currentRole.asObservable().subscribe(role => this.currentRole = role);
    this.photoIndexes = TripComponent.choosePhotoIndexes();
  }

  beginEditingName() {
    this.editingName = true;
  }

  saveNewName() {
    this.tripService.updateTrip(this.trip.tripId, {name: this.newName, description: this.newDescription}).subscribe({
      next: () => {
        this.tripService.openSnackBar('success', 'Zapisano nazwę wycieczki!');
        this.editingName = false;
        this.trip.name = this.newName;
      },
      error: () => {
        this.tripService.openSnackBar('error', 'Nie udało się zapisać nazwy wycieczki.');
        this.cancelEditingName();
      }
    });
  }

  cancelEditingName() {
    this.editingName = false;
    this.newName = this.trip.name;
  }

  beginEditingDescription() {
    this.editingDescription = true;
  }

  saveNewDescription() {
    this.tripService.updateTrip(this.trip.tripId, {name: this.newName, description: this.newDescription}).subscribe({
      next: () => {
        this.tripService.openSnackBar('success', 'Zapisano opis wycieczki!');
        this.editingDescription = false;
        this.trip.description = this.newDescription;
      },
      error: () => {
        this.tripService.openSnackBar('error', 'Nie udało się zapisać opisu wycieczki.');
        this.cancelEditingName();
      }
    });
  }

  cancelEditingDescription() {
    this.editingDescription = false;
    this.newDescription = this.trip.description;
  }

  deleteTrip() {
    this.tripService.deleteTrip(this.trip.tripId).subscribe({
      next: () => {
        this.router.navigateByUrl('/trips').then(() => {
          this.tripService.openSnackBar('success', 'Usunięto wycieczkę!');
        });
      },
      error: (error: HttpErrorResponse) => {
        this.tripService.openSnackBar('error', TripService.getErrorMessage(error.status));
      }
    });
  }

  openDialogToAskForVerification(): void {
    console.log('open dialog');
    const dialogRef = this.dialog.open(AskForConfirmationComponent, {width: '800px'});

    dialogRef.afterClosed().subscribe( (result: number | null) => {
      console.log('close dialog');
      if (result !== null) {
        this.tripService.requestTripVerification(this.trip.tripId, result).subscribe({
          next: () => {
            this.tripService.openSnackBar('success', 'Przesłano wycieczkę do weryfikacji!');
            this.tripService.getTripById(this.trip.tripId).subscribe(this.tripObserver);
          },
          error: (error: HttpErrorResponse) => {
            console.log(error);
            this.tripService.openSnackBar('error', (error.error as GOTException).message);
          }
        });
      }
    });
  }

  navigateBack() {
    window.history.back();
  }

}
