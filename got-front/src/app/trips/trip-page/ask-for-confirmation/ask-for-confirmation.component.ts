import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-ask-for-confirmation',
  templateUrl: './ask-for-confirmation.component.html',
  styleUrls: ['./ask-for-confirmation.component.css']
})
export class AskForConfirmationComponent implements OnInit {

  public przodownikWasPresent;
  public przodownikIdNumber: number;

  constructor() {  }

  ngOnInit() {
  }
}
