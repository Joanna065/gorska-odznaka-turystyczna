import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Trip} from '../trip-model/trip';
import {MatCheckboxChange} from '@angular/material';

@Component({
  selector: 'app-trip-card',
  templateUrl: './trip-card.component.html',
  styleUrls: ['./trip-card.component.css']
})
export class TripCardComponent implements OnInit {

  @Input() trip: Trip;
  @Input() massEditing: boolean;

  @Output() checked: EventEmitter<{tripId: number, isSelected: boolean}> = new EventEmitter<{tripId: number, isSelected: boolean}>();

  photoIndexes: number[];

  constructor() { }

  private static choosePhotoIndexes(): number[] {
    const indexes = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    const result = [];

    for (let i = 0; i < 4; i++) {
      const indexToAdd = (Math.random() * (indexes.length - 1)) + 1;
      result.push(indexes.splice(indexToAdd, 1));
    }

    return result;
  }

  ngOnInit() {
    this.photoIndexes = TripCardComponent.choosePhotoIndexes();
  }

  sendTripCardChangeEvent(change: MatCheckboxChange) {
    this.checked.emit({tripId: this.trip.tripId, isSelected: change.checked});
  }
}
