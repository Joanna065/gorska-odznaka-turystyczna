import {Injectable} from '@angular/core';
import {BehaviorSubject, Subject} from 'rxjs';
import {Role} from '../../models/Role';

@Injectable({
  providedIn: 'root'
})
export class RoleService {

  public currentRole: Subject<Role> = new BehaviorSubject<Role>(Role.Guest);

  constructor() {
  }

  public setNewRole(role: Role) {
    this.currentRole.next(role);
  }

  public getCurrentEmployeeId(): number {
    return 1;
  }
}
