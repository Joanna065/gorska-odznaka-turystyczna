import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {TripsComponent} from '../../trips/trips-page/trips.component';
import {TripComponent} from '../../trips/trip-page/trip.component';
import {NewTripComponent} from '../../trips/new-trip/new-trip.component';
import {ArticlesComponent} from '../../articles/articles-page/articles.component';
import {ArticleComponent} from '../../articles/article-page/article.component';
import {EditArticleComponent} from '../../articles/edit-article/edit-article.component';
import {NormsComponent} from '../../norms/norm-requests/norms.component';
import {NormComponent} from '../../norms/norm-request/norm.component';
import {ChooseRoleComponent} from '../../choose-role/choose-role.component';
import {LandingComponent} from '../../landing/landing.component';

const appRoutes: Routes = [
  {path: 'trips', component: TripsComponent},
  {path: 'trip/:id', component: TripComponent},
  {path: 'new-trip', component: NewTripComponent},
  {path: 'articles', component: ArticlesComponent},
  {path: 'article/:number', component: ArticleComponent},
  {path: 'edit-article/:action/:number', component: EditArticleComponent},
  {path: 'norms', component: NormsComponent},
  {path: 'norm/:number', component: NormComponent},
  {path: '', component: ChooseRoleComponent},
  {path: '**', component: LandingComponent}
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(
      appRoutes
    )
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
