export enum Role {
  Guest,
  Tourist,
  Leader,
  Employee
}
