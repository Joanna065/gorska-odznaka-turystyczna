export interface GOTException {
  message: string;
  excType: string;
}
