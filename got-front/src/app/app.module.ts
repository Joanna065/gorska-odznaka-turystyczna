import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  MatButtonModule,
  MatCardModule,
  MatCheckboxModule,
  MatDialogModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatRadioModule,
  MatSelectModule,
  MatSidenavModule,
  MatSnackBarModule,
  MatToolbarModule
} from '@angular/material';
import {SidenavComponent} from './sidenav/sidenav.component';
import {TripsComponent} from './trips/trips-page/trips.component';
import {TripCardComponent} from './trips/trip-card/trip-card.component';
import {TripComponent} from './trips/trip-page/trip.component';
import {ArticlesComponent} from './articles/articles-page/articles.component';
import {ArticleCardComponent} from './articles/article-card/article-card.component';
import {ArticleComponent} from './articles/article-page/article.component';
import {NormsComponent} from './norms/norm-requests/norms.component';
import {NormCardComponent} from './norms/norm-card/norm-card.component';
import {LandingComponent} from './landing/landing.component';
import {AskForConfirmationComponent} from './trips/trip-page/ask-for-confirmation/ask-for-confirmation.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NewTripComponent} from './trips/new-trip/new-trip.component';
import {NormComponent} from './norms/norm-request/norm.component';
import {RejectNormComponent} from './norms/norm-request/reject-norm/reject-norm.component';
import {ConfirmDeletingTripsComponent} from './trips/confirm-deleting-trips/confirm-deleting-trips.component';
import {EditArticleComponent} from './articles/edit-article/edit-article.component';
import {ChooseRoleComponent} from './choose-role/choose-role.component';
import {AppRoutingModule} from './utilities/app-routing/app-routing.module';
import {SnackBarComponent} from './utilities/components/snack-bar/snack-bar.component';

@NgModule({
  declarations: [
    AppComponent,
    SidenavComponent,
    TripsComponent,
    TripCardComponent,
    TripComponent,
    ArticlesComponent,
    ArticleCardComponent,
    ArticleComponent,
    NormsComponent,
    NormCardComponent,
    LandingComponent,
    AskForConfirmationComponent,
    NewTripComponent,
    NormComponent,
    RejectNormComponent,
    ConfirmDeletingTripsComponent,
    EditArticleComponent,
    ChooseRoleComponent,
    SnackBarComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatSidenavModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatDialogModule,
    MatRadioModule,
    FormsModule,
    MatSelectModule,
    MatSnackBarModule,
    AppRoutingModule,
    MatCheckboxModule,
    ReactiveFormsModule
  ],
  entryComponents: [
    AskForConfirmationComponent,
    RejectNormComponent,
    ConfirmDeletingTripsComponent,
    SnackBarComponent
  ],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
