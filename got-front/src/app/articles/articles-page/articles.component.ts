import {AfterViewChecked, Component, OnInit, QueryList, ViewChildren} from '@angular/core';
import {ArticleService} from '../article-service/article.service';
import {Article} from '../article-model/article';
import {RoleService} from '../../utilities/services/role/role.service';
import {Role} from '../../utilities/models/Role';
import {Router} from '@angular/router';
import {ArticleCardComponent} from '../article-card/article-card.component';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {Observer} from 'rxjs';

@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.css']
})
export class ArticlesComponent implements OnInit, AfterViewChecked {

  @ViewChildren(ArticleCardComponent) articleCards: QueryList<ArticleCardComponent>;
  articles: Article[];
  currentRole: Role;

  errorHasOccured = false;
  errorMessage: string;
  massEditing = false;
  selectedArticles: number[] = [];

  // hack in order to use enum in component's HTML
  Roles = Role;

  articlesObserver: Observer<HttpResponse<Article[]>> = {
    next: (response: HttpResponse<Article[]>) => {
      this.articles = response.body;
      this.errorHasOccured = false;
    },
    error: (error: HttpErrorResponse) => {
      this.errorHasOccured = true;
      this.errorMessage = ArticleService.getErrorMessage(error.status);
    },
    complete: () => {}
  };

  constructor(private router: Router, private roleService: RoleService, private articleService: ArticleService) { }

  ngOnInit() {

    this.articleService.getAllArticles().subscribe(this.articlesObserver);

    this.roleService.currentRole.asObservable().subscribe(role => this.currentRole = role);
  }

  // TODO: very bad hack - this gets executed multiple times!
  ngAfterViewChecked() {
    console.log('ng after view checked begin');
    this.articleCards.forEach(articleCard => {
      // attach listeners only if there isn't one yet
      if (articleCard.checked.observers.length === 0) {
        articleCard.checked.subscribe((change) => {
          const indexOfChangedNumber = this.selectedArticles.indexOf(change.articleNumber);

          // if the article has been selected and isn't added yet, add it
          if (change.isSelected) {
            if (indexOfChangedNumber < 0) {
              this.selectedArticles.push(change.articleNumber);
            }
          } else { // if the article has been deselected and has already been added, delete it
            if (indexOfChangedNumber >= 0) {
              this.selectedArticles.splice(indexOfChangedNumber, 1);
            }
          }
        });
      }
    });
  }

  navigateToAdding() {
    this.router.navigateByUrl('/edit-article/add/');
  }

  setMassEditing() {
    this.massEditing = true;
  }

  unsetMassEditing() {
    this.massEditing = false;
    this.selectedArticles = [];
  }

  deleteSelectedArticles() {
    this.articleService.deleteArticles(this.selectedArticles).subscribe({
      next: () => {
        this.articleService.openSnackBar('success', 'Usunięto artykuły!');
        this.unsetMassEditing();
        this.articleService.getAllArticles().subscribe(this.articlesObserver);
      },
      error: err => {
        this.articleService.openSnackBar('error', 'Wystąpił błąd podczas usuwania artykułów');
        this.unsetMassEditing();
        this.articleService.getAllArticles().subscribe(this.articlesObserver);
      }
    });
  }
}
