export interface ArticleResponseDTO {
  employeeId: number;
  articleId: number;
  title: string;
  addDate: string;
  text: string;
  keyWords: string;
  updateDate: string;
}

export interface Article {
  articleId: number;
  title: string;
  addDate: Date;
  text: string;
  keyWords: string;
  updateDate: Date;
}

export interface ArticleCreateDTO {
  employeeId: number;
  title: string;
  keyWords: string;
  text: string;
}
