import {Component, OnInit} from '@angular/core';
import {Article, ArticleCreateDTO} from '../article-model/article';
import {ArticleService} from '../article-service/article.service';
import {ActivatedRoute, Router} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {RoleService} from '../../utilities/services/role/role.service';

@Component({
  selector: 'app-edit-article',
  templateUrl: './edit-article.component.html',
  styleUrls: ['./edit-article.component.css']
})
export class EditArticleComponent implements OnInit {

  hasErrorOccured = false;
  errorMessage: string;

  editedArticleNumber: number = null;
  articleForm: FormGroup;

  action: string;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private roleService: RoleService,
              private articleService: ArticleService) { }

  private static getNewFormGroup(response?: HttpResponse<Article>): FormGroup {
    const keywordsRegex = '#[a-zA-Z][a-zA-Z0-9]*(, #[a-zA-Z][a-zA-Z0-9]*)*$';

    if (response !== undefined) {
      return new FormGroup({
        title: new FormControl(response.body.title, Validators.required),
        keywords: new FormControl(response.body.keyWords, Validators.pattern(keywordsRegex)),
        content: new FormControl(response.body.text, Validators.required)
      });
    } else {
      return new FormGroup({
        title: new FormControl('', Validators.required),
        keywords: new FormControl('', Validators.pattern(keywordsRegex)),
        content: new FormControl('', Validators.required)
      });
    }
  }

  get title() { return this.articleForm.get('title'); }

  get keywords() { return this.articleForm.get('keywords'); }

  get content() { return this.articleForm.get('content'); }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.editedArticleNumber = Number(params.get('number'));
      this.action = params.get('action');

      if (this.action === 'edit') {
        this.articleService.getArticleById(this.editedArticleNumber).subscribe(
          {
            next: (response: HttpResponse<Article>) => {
              this.articleForm = EditArticleComponent.getNewFormGroup(response);
            },
            error: (error: HttpErrorResponse) => {
              this.hasErrorOccured = true;
              this.errorMessage = error.statusText;
            }
          });
      } else if (this.action === 'add') {
        this.articleForm = EditArticleComponent.getNewFormGroup();
      }
    });
  }

  saveEditedArticle() {
    const articleToSave: ArticleCreateDTO = {
      employeeId: this.roleService.getCurrentEmployeeId(),
      title: this.articleForm.value.title,
      keyWords: this.articleForm.value.keywords,
      text: this.articleForm.value.content
    };

    this.articleService.updateArticle(this.editedArticleNumber, articleToSave).subscribe({
      error: () => this.routeToArticlesWithSnackbar('error', 'Wystąpił błąd podczas zapisywania artykułu.'),
      complete: () => this.routeToArticlesWithSnackbar('success', 'Zapisano artykuł!')
    });
  }

  addNewArticle() {
    const articleToSave: ArticleCreateDTO = {
      employeeId: this.roleService.getCurrentEmployeeId(),
      title: this.articleForm.value.title,
      keyWords: this.articleForm.value.keywords,
      text: this.articleForm.value.content
    };

    this.articleService.addArticle(articleToSave).subscribe({
      error: () => this.routeToArticlesWithSnackbar('error', 'Dodanie artykułu nie powiodło się.'),
      complete: () => this.routeToArticlesWithSnackbar('success', 'Dodano artykuł!')
    });
  }

  routeToArticlesWithSnackbar(requestStatus: string, requestMessage: string) {
    this.router.navigateByUrl('/articles').then(() => this.articleService.openSnackBar(requestStatus, requestMessage));
  }
}
