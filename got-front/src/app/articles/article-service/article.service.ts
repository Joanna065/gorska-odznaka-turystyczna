import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Article, ArticleCreateDTO, ArticleResponseDTO} from '../article-model/article';
import {MatSnackBar} from '@angular/material';
import {SnackBarComponent} from '../../utilities/components/snack-bar/snack-bar.component';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {

  constructor(private http: HttpClient, private snackBar: MatSnackBar) { }

  baseUrl = 'http://localhost:8080/articles';

  public static getErrorMessage(errorStatus: number): string {
    let result;
    switch (errorStatus) {
      case 400:
        result = 'Wystąpił błąd.';
        break;
      case 401:
        result = 'Unauthorized.';
        break;
      case 402:
        result = 'Forbidden.';
        break;
      case 404:
        result = 'Not found.';
        break;
      default:
        result = 'Wystąpił błąd.';
        break;
    }
    return result;
  }

  private static decodeArticle(json: ArticleResponseDTO): Article {
    return Object.assign({}, json, {
      addDate: new Date(json.addDate),
      updateDate: new Date(json.updateDate)
    });
  }

  private static encodeArticle(article: ArticleCreateDTO) {
    return Object.assign({}, article, {
      employeeId: 1
    });
  }

  getAllArticles(): Observable<HttpResponse<Article[]>> {
    return this.http.get<ArticleResponseDTO[]>(this.baseUrl, {observe: 'response'}).pipe(
      map(response => {
        return new HttpResponse<Article[]>({
          body: response.body.map(ArticleService.decodeArticle),
          headers: response.headers,
          status: response.status,
          statusText: response.statusText
        });
      })
    );
  }

  getArticleById(id: number): Observable<HttpResponse<Article>> {
    return this.http.get<ArticleResponseDTO>(this.baseUrl + `/${id}`, {observe: 'response'}).pipe(
      map((response: HttpResponse<ArticleResponseDTO>) => {
        return new HttpResponse<Article>({
          body: ArticleService.decodeArticle(response.body),
          headers: response.headers,
          status: response.status,
          statusText: response.statusText
        });
      })
    );
  }

  updateArticle(articleId: number, article: ArticleCreateDTO): Observable<HttpResponse<any>> {
    return this.http.patch(
      this.baseUrl + `/${articleId}`,
      ArticleService.encodeArticle(article),
      {
        headers: {'content-type': 'application/json'},
        observe: 'response'
      });
  }

  addArticle(article: ArticleCreateDTO): Observable<HttpResponse<any>> {
    return this.http.post(this.baseUrl,
                          ArticleService.encodeArticle(article),
                  {headers: {'content-type': 'application/json'},
                           observe: 'response'});
  }

  deleteArticles(articleIds: number[]): Observable<HttpResponse<any>> {
    let url = this.baseUrl + '/resources?';
    articleIds.forEach(articleId => {
      url += `ids=${articleId}&`;
    });

    return this.http.delete(url, {observe: 'response'});
  }

  openSnackBar(requestStatus: string,  requestMessage: string) {
    this.snackBar.openFromComponent(SnackBarComponent,
      {
        duration: 3500, data: {status: requestStatus, message: requestMessage},
        panelClass: requestStatus === 'success' ? 'snack-bar-green' : 'snack-bar-red'
      });
  }
}
