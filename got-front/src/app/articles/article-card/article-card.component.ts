import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Article} from '../article-model/article';
import {MatCheckboxChange} from '@angular/material';

@Component({
  selector: 'app-article-card',
  templateUrl: './article-card.component.html',
  styleUrls: ['./article-card.component.css']
})
export class ArticleCardComponent implements OnInit {

  @Input() massEditing: boolean;
  @Input() article: Article;

  @Output() checked: EventEmitter<{ articleNumber: number, isSelected: boolean }>
    = new EventEmitter<{ articleNumber: number, isSelected: boolean }>();

  constructor() { }

  ngOnInit() {
  }

  sendArticleCardChangeEvent(change: MatCheckboxChange) {
    this.checked.emit({articleNumber: this.article.articleId, isSelected: change.checked});
  }

}
