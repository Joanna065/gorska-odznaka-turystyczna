import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ArticleService} from '../article-service/article.service';
import {Article} from '../article-model/article';
import {Role} from '../../utilities/models/Role';
import {RoleService} from '../../utilities/services/role/role.service';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {

  article: Article;

  hasErrorOccured = false;
  errorMessage: string;

  currentRole: Role;
  // hack to use enum in component's HTML
  Roles = Role;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private articleService: ArticleService,
              private roleService: RoleService) {
  }

  ngOnInit() {
    console.log('init article component');
    this.route.paramMap.subscribe(params => {

      this.articleService.getArticleById(Number(params.get('number'))).subscribe({
        next: (response: HttpResponse<Article>) => {
          this.article = response.body;
        },
        error: (error: HttpErrorResponse) => {
          this.hasErrorOccured = true;
          this.errorMessage = ArticleService.getErrorMessage(error.status);
        }
      });
    });

    this.roleService.currentRole.asObservable().subscribe(role => this.currentRole = role);
  }

  deleteArticle() {
    this.articleService.deleteArticles([this.article.articleId]).subscribe(
      {
        next: () => this.routeToArticlesWithSnackbar('success', 'Usunięto artykuł!'),
        error: () => this.routeToArticlesWithSnackbar('error', 'Wystąpił błąd podczas usuwania artykułu!')
      });
  }

  routeToArticlesWithSnackbar(requestStatus: string, requestMessage: string) {
    this.router.navigateByUrl('/articles').then(() => this.articleService.openSnackBar(requestStatus, requestMessage));
  }


  navigateBack() {
    window.history.back();
  }
}
