import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NormCardComponent } from './norm-card.component';

describe('NormCardComponent', () => {
  let component: NormCardComponent;
  let fixture: ComponentFixture<NormCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NormCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NormCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
