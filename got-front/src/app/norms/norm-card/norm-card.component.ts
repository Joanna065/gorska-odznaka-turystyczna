import {Component, Input, OnInit} from '@angular/core';
import {NormDecision} from '../norm-model/classes';

@Component({
  selector: 'app-norm-card',
  templateUrl: './norm-card.component.html',
  styleUrls: ['./norm-card.component.css']
})
export class NormCardComponent implements OnInit {

  @Input() norm: NormDecision;

  constructor() { }

  ngOnInit() {
  }

}
