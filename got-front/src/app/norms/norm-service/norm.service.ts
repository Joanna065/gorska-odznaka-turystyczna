import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {NormDecision} from '../norm-model/classes';
import {NormDecisionResponseDTO, NormDecisionUpdateDTO} from '../norm-model/DTOs';
import {map} from 'rxjs/operators';
import {SnackBarComponent} from '../../utilities/components/snack-bar/snack-bar.component';
import {MatSnackBar} from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class NormService {

  private baseUrl = 'http://localhost:8080/normDecisions';

  constructor(private http: HttpClient, private snackBar: MatSnackBar) { }

  // TODO: repeats in other services
  public static getErrorMessage(errorStatus: number): string {
    let result;
    switch (errorStatus) {
      case 400:
        result = 'Wystąpił błąd.';
        break;
      case 401:
        result = 'Unauthorized.';
        break;
      case 402:
        result = 'Forbidden.';
        break;
      case 404:
        result = 'Not found.';
        break;
      default:
        result = 'Wystąpił błąd.';
        break;
    }
    return result;
  }

  public getAllNorms(): Observable<HttpResponse<NormDecision[]>> {
    return this.http.get<NormDecisionResponseDTO[]>(this.baseUrl, {observe: 'response'}).pipe(
      map(response => {
        return new HttpResponse<NormDecision[]>({
          body: response.body.map(normDTO => new NormDecision(normDTO)),
          headers: response.headers,
          status: response.status,
          statusText: response.statusText
        });
      })
    );
  }

  public getNormById(id: number): Observable<HttpResponse<NormDecision>> {
    return this.http.get<NormDecisionResponseDTO>(`${this.baseUrl}/${id}`, {observe: 'response'}).pipe(
      map(response => {
        return new HttpResponse<NormDecision>({
          body: new NormDecision(response.body),
          headers: response.headers,
          status: response.status,
          statusText: response.statusText
        });
      })
    );
  }

  public verifyNormById(id: number, dto: NormDecisionUpdateDTO): Observable<HttpResponse<any>> {
    return this.http.patch(`${this.baseUrl}/${id}/verify`, dto, {observe: 'response'});
  }

  // TODO: repeats in all services
  openSnackBar(requestStatus: string, requestMessage: string) {
    this.snackBar.openFromComponent(SnackBarComponent,
      {
        duration: 3500, data: {status: requestStatus, message: requestMessage},
        panelClass: requestStatus === 'success' ? 'snack-bar-green' : 'snack-bar-red'
      });
  }
}
