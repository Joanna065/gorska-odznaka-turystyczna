import {TripResponseDTO} from '../../trips/trip-model/trip';

export interface NormDecisionResponseDTO {
  normDecisionId: number;
  badgeNorm: BadgeNormResponseDTO;
  isAccepted: boolean;
  addDate: string; // again, described in docs as string
  justification: string;
  responseDate: string;
}

export interface NormDecisionUpdateDTO {
  isAccepted: boolean;
  justification?: string;
}

export interface BadgeNormResponseDTO {
  normId: number;
  attainmentDate: string;
  gainedPoints: number;
  isVerified: boolean;
  requiredPointAmount: number;
  touristBadge: TouristBadgeResponseDTO;
  trips: TripResponseDTO[];
}

export interface TouristBadgeResponseDTO {
  touristBadgeId: number;
  startAttainDate: string; // is written in docs as number, WTF?
  attainmentDate: string;
  badge: BadgeResponseDTO;
}

export interface BadgeResponseDTO {
  badgeId: number;
  type: string;
  degree: string;
  requiredPointAmount: string;
  description: string;
  additionalRequirements: string;
}
