import {BadgeNormResponseDTO, BadgeResponseDTO, NormDecisionResponseDTO, TouristBadgeResponseDTO} from './DTOs';
import {compareDates, Trip} from '../../trips/trip-model/trip';

export class NormDecision {
  normDecisionId: number;
  badgeNorm: BadgeNorm;
  isAccepted: boolean;
  addDate: Date;
  justification: string;
  responseDate: Date;

  constructor(dto: NormDecisionResponseDTO) {
    this.normDecisionId = dto.normDecisionId;
    this.badgeNorm = new BadgeNorm(dto.badgeNorm);
    this.isAccepted = dto.isAccepted;
    this.addDate = new Date(dto.addDate);
    this.justification = dto.justification;
    this.responseDate = new Date(dto.responseDate);
  }

  get dateOfFirstTrip(): Date {
    const dates = this.badgeNorm.trips.map(trip => trip.dateOfFirstRoute).sort(compareDates);
    return dates[0];
  }

  get dateOfLastTrip(): Date {
    const dates = this.badgeNorm.trips.map(trip => trip.dateOfLastRoute).sort(compareDates);
    return dates[dates.length - 1];
  }
}

export class BadgeNorm {
  normId: number;
  touristBadge: TouristBadge;
  requiredPointAmount: number;
  gainedPoints: number;
  attainmentDate: Date;
  isVerified: boolean;
  trips: Trip[];

  constructor(dto: BadgeNormResponseDTO) {
    this.normId = dto.normId;
    this.touristBadge = new TouristBadge(dto.touristBadge);
    this.requiredPointAmount = dto.requiredPointAmount;
    this.gainedPoints = dto.gainedPoints;
    this.attainmentDate = new Date(dto.attainmentDate);
    this.isVerified = dto.isVerified;
    this.trips = dto.trips.map<Trip>(tripDTO => new Trip(tripDTO));
  }
}

export class TouristBadge {
  public touristBadgeId: number;
  public startAttainDate: Date;
  public attainmentDate: Date;
  public badge: Badge;

  constructor(dto: TouristBadgeResponseDTO) {
    this.touristBadgeId = dto.touristBadgeId;
    this.startAttainDate = new Date(dto.startAttainDate);
    this.attainmentDate = new Date(dto.attainmentDate);
    this.badge = new Badge(dto.badge);
  }
}

export class Badge {
  badgeId: number;
  type: string;
  degree: string;
  requiredPointAmount: string;
  description: string;
  additionalRequirements: string;

  constructor(dto: BadgeResponseDTO) {
    this.badgeId = dto.badgeId;
    this.type = dto.type;
    this.degree = dto.degree;
    this.requiredPointAmount = dto.requiredPointAmount;
    this.description = dto.description;
    this.additionalRequirements = dto.additionalRequirements;
  }
}

