import {Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material';
import {RejectNormComponent} from './reject-norm/reject-norm.component';
import {ActivatedRoute, Router} from '@angular/router';
import {NormDecision} from '../norm-model/classes';
import {NormService} from '../norm-service/norm.service';
import {HttpErrorResponse} from '@angular/common/http';
import {GOTException} from '../../utilities/models/GOTException';

@Component({
  selector: 'app-norm',
  templateUrl: './norm.component.html',
  styleUrls: ['./norm.component.css']
})
export class NormComponent implements OnInit {

  norm: NormDecision;

  hasErrorOccured = false;
  errorMessage: string;

  normObserver = {
    next: response => {
      this.norm = response.body;
    },
    error: (error: HttpErrorResponse) => {
      this.hasErrorOccured = true;
      this.errorMessage = NormService.getErrorMessage(error.status);
    }
  };

  constructor(private route: ActivatedRoute, private router: Router, private normService: NormService, public dialog: MatDialog) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.normService.getNormById(Number(params.get('number'))).subscribe(this.normObserver);
    });
  }

  openDialogToGiveReasonForRejection(): void {
    const dialogRef = this.dialog.open(RejectNormComponent, {width: '600px'});

    dialogRef.afterClosed().subscribe( reasonForRejection => {
      if (reasonForRejection !== null) {
        this.normService.verifyNormById(this.norm.normDecisionId, {isAccepted: false, justification: reasonForRejection}).subscribe({
          next: () => {
            this.router.navigateByUrl('/norms').then(() => {
              this.normService.openSnackBar('success', 'Odrzucono normę!');
            });
          },
          error: (error: HttpErrorResponse) => {
            this.normService.openSnackBar('error', (error.error as GOTException).message);
          }
        });
      }
    });
  }

  verifyNorm() {
    this.normService.verifyNormById(this.norm.normDecisionId, {isAccepted: true}).subscribe({
      next: () => {
        this.router.navigateByUrl('/norms').then(() => {
          this.normService.openSnackBar('success', 'Potwierdzono normę!');
        });
      },
      error: (error: HttpErrorResponse) => {
        this.normService.openSnackBar('error', (error.error as GOTException).message);
      }
    });
  }

  navigateBack() {
    window.history.back();
  }

}
