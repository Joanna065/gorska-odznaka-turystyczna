import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {RejectNormComponent} from './reject-norm.component';

describe('RejectNormComponent', () => {
  let component: RejectNormComponent;
  let fixture: ComponentFixture<RejectNormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RejectNormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RejectNormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
