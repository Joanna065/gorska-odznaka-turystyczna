import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-reject-norm',
  templateUrl: './reject-norm.component.html',
  styleUrls: ['./reject-norm.component.css']
})
export class RejectNormComponent implements OnInit {

  reasonForRejection: string;

  constructor() { }

  ngOnInit() {}
}
