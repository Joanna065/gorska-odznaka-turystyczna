import {Component, OnInit} from '@angular/core';
import {NormService} from '../norm-service/norm.service';
import {NormDecision} from '../norm-model/classes';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-norms',
  templateUrl: './norms.component.html',
  styleUrls: ['./norms.component.css']
})
export class NormsComponent implements OnInit {

  unverifiedNorms: NormDecision[];

  hasErrorOccured = false;
  errorMesssage: string;

  constructor(private normService: NormService) { }

  ngOnInit() {
    this.normService.getAllNorms().subscribe({
      next: response => {
        this.unverifiedNorms = response.body;
      },
      error: (error: HttpErrorResponse) => {
        this.hasErrorOccured = true;
        this.errorMesssage = NormService.getErrorMessage(error.status);
      }
    });
  }

}
