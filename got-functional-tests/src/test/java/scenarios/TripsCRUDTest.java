package scenarios;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import pom.roles.RolesPage;
import pom.trips.TripPage;
import pom.trips.TripsPage;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

public class TripsCRUDTest {

    private TripsPage tripsPage;
    private TripPage tripPage;

    private WebDriver driver;

    @Before
    public void setUp() {
        System.setProperty("webdriver.gecko.driver", "C:/Programy/GeckoDriver/geckodriver.exe");
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(7, TimeUnit.SECONDS);
        driver.get("http://localhost:4200");

        tripsPage = new RolesPage(this.driver)
                            .chooseRole(RolesPage.Role.TOURIST)
                            .sidenav.navigateToTrips();
    }

    @After
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void testTripsCRUD() {

        int numberOfTripsBeforeAdding = tripsPage.getNumberOfTripCards();

        tripsPage = tripsPage. navigateToAddingTrip()
                            .fillTripData("test trip", "test description")
                            .submitTripData();


        int numberOfTripsAfterAdding = tripsPage.getNumberOfTripCards();

        assertEquals("Number of trips after adding should be 1 greater than before adding",
                numberOfTripsBeforeAdding + 1, numberOfTripsAfterAdding);

        tripPage = tripsPage.viewTripWithTitle("test trip");

        tripPage = tripPage.editTitle("edited test trip").editDescription("edited test description");

        assertEquals("Trip title should change after editing",
                "edited test trip", tripPage.getTitle());
        assertEquals("Trip description should change after editing",
                "edited test description", tripPage.getDescription());

        tripsPage = tripPage.deleteTrip();

        int numberOfTripsAfterDeleting = tripsPage.getNumberOfTripCards();

        assertEquals("Number of trips after deleting should be the same as before adding",
                numberOfTripsBeforeAdding, numberOfTripsAfterDeleting);
    }
}
