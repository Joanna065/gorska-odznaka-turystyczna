package scenarios;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import pom.articles.ArticlePage;
import pom.articles.ArticlesPage;
import pom.roles.RolesPage;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

public class ArticlesCRUDTest {

    private WebDriver driver;
    private ArticlesPage articlesPage;
    private ArticlePage articlePage;

    @Before
    public void setUp() {
        System.setProperty("webdriver.gecko.driver", "C:/Programy/GeckoDriver/geckodriver.exe");
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(7, TimeUnit.SECONDS);
        driver.get("http://localhost:4200");

        this.articlesPage = new RolesPage(this.driver)
                                            .chooseRole(RolesPage.Role.EMPLOYEE)
                                            .sidenav.navigateToArticles();
    }

    @After
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void articlesCRUDTest() {

        int numberOfArticlesBeforeAdding = articlesPage.getNumberOfArticleCards();

        articlesPage = articlesPage.clickAddArticle()
                .fillArticleData("test title", "#test, #keywords", "test content of the article")
                .submitArticleData();

        int numberOfArticlesAfterAdding = articlesPage.getNumberOfArticleCards();

        assertEquals("Number of articles after adding should be 1 greater than before adding",
                numberOfArticlesBeforeAdding + 1, numberOfArticlesAfterAdding);

        articlePage = articlesPage.readArticleWithTitle("test title");

        Assert.assertEquals("Title of article should be equal to the one added",
                "test title", articlePage.getTitle());

        Assert.assertEquals("Date of article should be equal to today",
                LocalDate.now().format(DateTimeFormatter.ofPattern("d.MM.yyyy")), articlePage.getUpdateDate());

        Assert.assertEquals("Keywords of article should be equal to the ones added",
                "#test, #keywords", articlePage.getKeywords());

        Assert.assertEquals("Content of the article should be equal to the one added",
                "test content of the article", articlePage.getContent());

        articlePage = articlePage
                              .editArticle()
                              .fillArticleData(
                                      "edited test title",
                                      "#edit, #test, #keywords",
                                      "edited test content of the article")
                              .submitArticleData()
                              .readArticleWithTitle("edited test title");

        Assert.assertEquals("Edited title of article should be equal to the one added",
                "edited test title", articlePage.getTitle());
        Assert.assertEquals("Edited keywords of article should be equal to the ones added",
                "#edit, #test, #keywords", articlePage.getKeywords());
        Assert.assertEquals("Edited content of the article should be equal to the one added",
                "edited test content of the article", articlePage.getContent());

        articlesPage = articlePage.deleteArticle();

        int numberOfArticlesAfterDeleting = articlesPage.getNumberOfArticleCards();

        assertEquals("Number of articles after deleting should be the same as before adding",
                numberOfArticlesBeforeAdding, numberOfArticlesAfterDeleting);
    }
}
