package pom.articles;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pom.core.Page;

import java.util.List;
import java.util.stream.Collectors;

public class ArticlesPage extends Page {
    private WebElement addArticleButton;
    private WebElement selectManyButton;

    private final List<ArticleCard> articleCards;

    public ArticlesPage(WebDriver driver) {
        super(driver);

        this.addArticleButton = this.findButtonByText("Dodaj artykuł");
        this.selectManyButton = this.findButtonByText("Zaznacz wiele");

        this.articleCards = this.driver.findElements(By.tagName("app-article-card"))
                                    .stream()
                                    .map(cardElement -> new ArticleCard(this.driver, cardElement))
                                    .collect(Collectors.toList());
    }

    public EditArticlePage clickAddArticle() {
        this.addArticleButton.click();
        return new EditArticlePage(this.driver);
    }

    public ArticlePage readArticleWithTitle(String articleTitle) {
        this.articleCards.stream()
                .filter(articleCard -> articleCard.getTitle().equals(articleTitle))
                .findFirst()
                .ifPresent(ArticleCard::readArticle);
        return new ArticlePage(this.driver);
    }

    public int getNumberOfArticleCards() {
        return articleCards.size();
    }
}
