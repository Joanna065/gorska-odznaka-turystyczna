package pom.articles;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pom.core.Page;

import java.util.List;

public class EditArticlePage extends Page {
    private WebElement saveArticleButton;
    private WebElement cancelEditingButton;

    private WebElement titleInput;
    private WebElement keywordsInput;
    private WebElement contentInput;

    public EditArticlePage(WebDriver driver) {
        super(driver);

        this.saveArticleButton = this.findButtonByText("Zapisz");
        this.cancelEditingButton = this.findButtonByText("Anuluj");

        List<WebElement> formInputs = this.driver.findElements(By.tagName("mat-form-field"));
        this.titleInput = formInputs.get(0).findElement(By.tagName("input"));
        this.keywordsInput = formInputs.get(1).findElement(By.tagName("input"));
        this.contentInput = formInputs.get(2).findElement(By.tagName("textarea"));
    }

    public EditArticlePage fillArticleData(String title, String keywords, String content) {
        return this.fillTitle(title)
                .fillKeywords(keywords)
                .fillContent(content);
    }

    public EditArticlePage fillTitle(String title) {
        this.titleInput.clear();
        this.titleInput.sendKeys(title);
        return this;
    }

    public EditArticlePage fillKeywords(String keywords) {
        this.keywordsInput.clear();
        this.keywordsInput.sendKeys(keywords);
        return this;
    }

    public EditArticlePage fillContent(String content) {
        this.contentInput.clear();
        this.contentInput.sendKeys(content);
        return this;
    }

    public ArticlesPage submitArticleData() {
        this.saveArticleButton.click();
        return new ArticlesPage(this.driver);
    }

    public ArticlesPage cancelEditing() {
        this.cancelEditingButton.click();
        return new ArticlesPage(this.driver);
    }

}
