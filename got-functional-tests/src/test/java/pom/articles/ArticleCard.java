package pom.articles;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pom.core.Element;

public class ArticleCard extends Element {

    private WebElement title;
    private WebElement readButton;

    public ArticleCard(WebDriver driver, WebElement cardElement) {
        super(driver, cardElement);

        this.title = this.mainElement.findElement(By.tagName("mat-card-title"));
        this.readButton = this.mainElement.findElement(By.tagName("button"));
    }

    public String getTitle() {
        return this.title.getText();
    }

    public ArticlePage readArticle() {
        this.readButton.click();

        return new ArticlePage(this.driver);
    }
}
