package pom.articles;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pom.core.Page;

public class ArticlePage extends Page {
    private WebElement backButton;
    private WebElement deleteButton;
    private WebElement editButton;

    private WebElement title;
    private WebElement updateDate;
    private WebElement keywords;
    private WebElement content;

    public ArticlePage(WebDriver driver) {
        super(driver);

        this.backButton = this.findButtonByText("Wróć");
        this.deleteButton = this.findButtonByText("Usuń artykuł");
        this.editButton = this.findButtonByText("Edytuj artykuł");

        this.title = this.driver.findElement(By.className("title"));
        this.updateDate = this.driver.findElement(By.className("latest-update"));
        this.keywords = this.driver.findElement(By.className("keywords"));
        this.content = this.driver.findElement(By.className("article"));
    }

    public String getTitle() {
        return this.title.getText();
    }

    public String getUpdateDate() {
        return this.updateDate.getText();
    }

    public String getKeywords() {
        return this.keywords.getText();
    }

    public String getContent() {
        return this.content.getText();
    }

    public ArticlesPage goBack() {
        this.backButton.click();

        return new ArticlesPage(this.driver);
    }

    public ArticlesPage deleteArticle() {
        this.deleteButton.click();

        return new ArticlesPage(this.driver);
    }

    public EditArticlePage editArticle() {
        this.editButton.click();

        return new EditArticlePage(this.driver);
    }

}
