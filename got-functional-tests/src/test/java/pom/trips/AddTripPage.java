package pom.trips;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pom.core.Page;

public class AddTripPage extends Page {
    private WebElement cancelButton;
    private WebElement addTripButton;
    private WebElement nameInput;
    private WebElement descriptionInput;

    public AddTripPage(WebDriver driver) {
        super(driver);

        this.cancelButton = this.findButtonByText("Anuluj");
        this.addTripButton = this.findButtonByText("Dodaj wycieczkę");
        this.nameInput = this.driver.findElement(By.tagName("input"));
        this.descriptionInput = this.driver.findElement(By.tagName("textarea"));
    }

    public AddTripPage fillName(String name) {
        this.nameInput.sendKeys(name);
        return this;
    }

    public AddTripPage fillDescription(String description) {
        this.descriptionInput.sendKeys(description);
        return this;
    }

    public AddTripPage fillTripData(String name, String description) {
        return this.fillName(name).fillDescription(description);
    }

    public TripsPage submitTripData() {
        this.addTripButton.click();
        return new TripsPage(this.driver);
    }
}
