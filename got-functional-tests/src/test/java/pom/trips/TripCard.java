package pom.trips;

import org.jetbrains.annotations.Nullable;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pom.core.Element;

import java.util.List;

public class TripCard extends Element {
    private WebElement title;
    private WebElement status;

    @Nullable
    private WebElement mountainGroupAndDates;

    private WebElement points;
    private WebElement detailsButton;

    public TripCard(WebDriver driver, WebElement element) {
        super(driver, element);

        this.title = this.mainElement.findElement(By.tagName("mat-card-title"));
        this.status = this.mainElement.findElement(By.className("trip-card-status"));

        try {
            this.mountainGroupAndDates = this.mainElement.findElement(By.className("trip-card-group-and-dates"));
        } catch (NoSuchElementException e) {
            this.mountainGroupAndDates = null;
        }

        List<WebElement> tripPointsAndDetails = this.mainElement.findElement(By.className("trip-card-points-and-details"))
                                                  .findElements(By.tagName("div"));
        this.points = tripPointsAndDetails.get(0);
        this.detailsButton = tripPointsAndDetails.get(1);
    }

    public String getTitle() {
        return title.getText();
    }

    public String getStatus() {
        return status.getText();
    }

    public String getMountainGroupAndDates() {
        return mountainGroupAndDates != null ? mountainGroupAndDates.getText() : "";
    }

    public String getPoints() {
        return points.getText();
    }

    public TripPage viewDetails() {
        detailsButton.click();
        return new TripPage(this.driver);
    }

}
