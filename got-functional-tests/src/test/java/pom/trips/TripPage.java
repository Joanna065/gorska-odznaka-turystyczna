package pom.trips;

import org.jetbrains.annotations.Nullable;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pom.core.Page;

public class TripPage extends Page {
    private boolean unverified;

    private WebElement backButton;

    @Nullable
    private WebElement deleteButton;

    @Nullable
    private WebElement askForVerificationButton;

    private WebElement title;
    private WebElement editTitleButton;
    private WebElement status;
    private WebElement mountainGroupAndDates;

    @Nullable
    private WebElement days;

    //public List<RouteCard> routeCards;
    private WebElement editDescriptionButton;
    private WebElement description;

    public TripPage(WebDriver driver) {
        super(driver);


        this.title = this.driver.findElement(By.className("name")).findElement(By.tagName("div"));
        this.editTitleButton = this.driver.findElement(By.className("name")).findElement(By.tagName("mat-icon"));
        this.status = this.driver.findElement(By.className("status"));

        if(this.status.getText().equals("NIEZWERYFIKOWANA")) {
            this.unverified = true;
            this.deleteButton = this.findButtonByText("Usuń wycieczkę");
            this.askForVerificationButton = this.findButtonByText("Poproś o weryfikację wycieczki");
        }
        else {
            this.unverified = false;
        }
        this.backButton = this.findButtonByText("Wróć");

        this.mountainGroupAndDates = this.driver.findElement(By.className("group-and-dates"));

        try {
            this.days = this.driver.findElement(By.className("days"));
        } catch (NoSuchElementException e) {
            this.days = null;
        }

        /*this.routeCards = this.driver.findElements(By.tagName("mat-card"))
                                  .stream()
                                  .map(cardElement -> new RouteCard(this.driver, cardElement))
                                  .collect(Collectors.toList());*/

        this.description = this.driver.findElement(By.className("description"));
        if(this.description.getText().equals("")) {
            this.editDescriptionButton = this.findButtonByText("Dodaj opis");
        }
        else {
            this.editDescriptionButton = this.findButtonByText("Edytuj opis");
        }
    }

    public String getTitle() {
        return title.getText();
    }

    public String getStatus() {
        return status.getText();
    }

    public String getMountainGroupAndDates() {
        return mountainGroupAndDates.getText();
    }

    public String getDays() {
        return days != null ? days.getText() : "";
    }

    public TripPage editTitle(String newTitle) {
        this.editTitleButton.click();

        WebElement editTitleInput = this.driver.findElement(By.tagName("input"));
        editTitleInput.clear();
        editTitleInput.sendKeys(newTitle);

        this.findButtonByText("Zapisz").click();

        return new TripPage(this.driver);
    }

    public TripPage editDescription(String newDescription) {
        this.editDescriptionButton.click();

        WebElement editDescriptionInput = this.driver.findElement(By.tagName("textarea"));
        editDescriptionInput.clear();
        editDescriptionInput.sendKeys(newDescription);

        this.findButtonByText("Zapisz").click();

        return new TripPage(this.driver);
    }

    public String getDescription() {
        return description.getText();
    }

    public TripsPage navigateBackToTrips() {
        this.backButton.click();
        return new TripsPage(this.driver);
    }

    public TripsPage deleteTrip() {
        if(unverified) {
            assert this.deleteButton != null;
            this.deleteButton.click();
            return new TripsPage(this.driver);
        }
        else {
            throw new UnsupportedOperationException("Can't delete a trip that's already verified or sent to verification");
        }
    }

    public TripPage askForTripVerification() {
        if(unverified) {
            //TODO
            return new TripPage(this.driver);
        }
        else {
            throw new UnsupportedOperationException("Can't ask to verify a trip that's already verified or set to verification");
        }
    }
}
