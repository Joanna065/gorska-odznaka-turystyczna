package pom.trips;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pom.core.Page;

import java.util.List;
import java.util.stream.Collectors;

public class TripsPage extends Page {

    private WebElement addTripButton;
    private WebElement selectManyButton;
    private List<TripCard> tripCards;

    public TripsPage(WebDriver driver) {
        super(driver);

        this.addTripButton = this.findButtonByText("Dodaj wycieczkę");
        this.selectManyButton = this.findButtonByText("Zaznacz wiele");

        this.tripCards = this.driver.findElements(By.tagName("app-trip-card"))
                                 .stream()
                                 .map(cardElement -> new TripCard(this.driver, cardElement))
                                 .collect(Collectors.toList());
    }

    public int getNumberOfTripCards() {
        return tripCards.size();
    }

    public AddTripPage navigateToAddingTrip() {
        this.addTripButton.click();
        return new AddTripPage(this.driver);
    }

    public TripPage viewTripWithTitle(String title) {
        this.tripCards.stream()
                .filter(tripCard -> tripCard.getTitle().equals(title))
                .findFirst()
                .ifPresent(TripCard::viewDetails);
        return new TripPage(this.driver);
    }
}
