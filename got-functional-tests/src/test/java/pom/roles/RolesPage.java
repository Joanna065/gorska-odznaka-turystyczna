package pom.roles;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pom.landing.LandingPage;

import java.util.List;

public class RolesPage {
    public enum Role {
        GUEST("Gość"),
        TOURIST("Turysta"),
        LEADER("Przodownik"),
        EMPLOYEE("Pracownik");

        public final String roleName;

        Role(String roleName) {
            this.roleName = roleName;
        }
    }

    private WebDriver driver;

    private WebElement roleSelect;
    private WebElement selectRoleButton;

    public RolesPage(WebDriver driver) {
        this.driver = driver;

        this.roleSelect = this.driver.findElement(By.tagName("mat-select"));
        this.selectRoleButton = this.driver.findElement(By.tagName("button"));
    }

    public LandingPage chooseRole(Role role) {
        roleSelect.click();

        List<WebElement> roleOptions = driver.findElements(By.tagName("mat-option"));
        for(WebElement option: roleOptions) {
            if(option.getText().equals(role.roleName)) {
                option.click();
                break;
            }
        }

        selectRoleButton.click();

        return new LandingPage(this.driver);
    }
}
