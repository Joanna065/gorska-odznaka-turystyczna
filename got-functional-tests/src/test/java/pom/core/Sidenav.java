package pom.core;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pom.articles.ArticlesPage;
import pom.norms.NormsPage;
import pom.trips.TripsPage;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Sidenav {

    private WebDriver driver;
    public Map<String, SidenavSection> sections = new HashMap<>();

    public Sidenav(WebDriver driver) {
        this.driver = driver;

        List<WebElement> sideNavSections = driver.findElements(By.className("side-nav-section"));

        sideNavSections.stream().map(SidenavSection::new).forEach(section -> this.sections.put(section.getSectionTitle(), section));
    }

    public void navigate(String sectionTitle, String itemTitle) {
        this.sections.get(sectionTitle).items.get(itemTitle).click();
    }

    public ArticlesPage navigateToArticles() {
        this.sections.get("INFO").items.get("Aktualności").click();
        return new ArticlesPage(this.driver);
    }

    public TripsPage navigateToTrips() {
        this.sections.get("TURYSTA").items.get("Wycieczki").click();
        return new TripsPage(this.driver);
    }

    public NormsPage navigateToNormRequests() {
        this.sections.get("PRACOWNIK").items.get("Prośby o potwierdzenie norm").click();
        return new NormsPage(this.driver);
    }
}
