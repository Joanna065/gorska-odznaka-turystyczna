package pom.core;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Element {
    protected WebDriver driver;
    protected WebElement mainElement;

    public Element(WebDriver driver, WebElement mainElement) {
        this.driver = driver;
        this.mainElement = mainElement;
    }

    protected WebElement findButtonByText(String buttonText) {
        return this.mainElement.findElement(By.xpath("//button[contains(.,'" + buttonText + "')]"));
    }
}
