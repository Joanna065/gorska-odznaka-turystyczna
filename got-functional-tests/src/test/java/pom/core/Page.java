package pom.core;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Page {

    protected WebDriver driver;

    public Sidenav sidenav;

    public Page(WebDriver driver) {
        this.driver = driver;
        this.sidenav = new Sidenav(driver);
    }

    public void navigate(String sectionTitle, String itemTitle) {
        this.sidenav.navigate(sectionTitle, itemTitle);
    }

    protected WebElement findButtonByText(String buttonText) {
        return this.driver.findElement(By.xpath("//button[contains(.,'" + buttonText + "')]"));
    }

}
