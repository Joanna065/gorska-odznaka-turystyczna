package pom.core;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.HashMap;
import java.util.Map;

public class SidenavSection {

    private WebElement sectionTitle;

    public Map<String, WebElement> items = new HashMap<>();

    public SidenavSection(WebElement sideNavSection) {
        this.sectionTitle = sideNavSection.findElement(By.className("side-nav-section-title"));

        sideNavSection.findElements(By.className("side-nav-section-item")).forEach(item -> this.items.put(item.getText(), item));
    }

    public String getSectionTitle() {
        return this.sectionTitle.getText();
    }
}
