package pom.norms;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pom.core.Page;
import pom.trips.TripCard;

import java.util.List;
import java.util.stream.Collectors;

public class NormPage extends Page {
    private WebElement backButton;
    private WebElement rejectButton;
    private WebElement acceptButton;

    private WebElement title;
    private WebElement dates;
    private List<TripCard> tripCards;

    public NormPage(WebDriver driver) {
        super(driver);

        this.backButton = this.findButtonByText("Wróć");
        this.rejectButton = this.findButtonByText("Odrzuć normę");
        this.acceptButton = this.findButtonByText("Potwierdź normę");

        this.title = this.driver.findElement(By.className("name"));
        this.dates = this.driver.findElement(By.className("dates"));
        this.tripCards = this.driver.findElements(By.tagName("app-trip-card"))
                                 .stream()
                                 .map(cardElement -> new TripCard(this.driver, cardElement))
                                 .collect(Collectors.toList());
    }

    public String getTitle() {
        return title.getText();
    }

    public String getDates() {
        return dates.getText();
    }

    public NormsPage navigateBack() {
        this.backButton.click();
        return new NormsPage(this.driver);
    }

    public NormsPage rejectNorm(String justification) {
        this.rejectButton.click();

        //TODO: reject norm dialog class
        WebElement rejectNormDialog = this.driver.findElement(By.tagName("app-reject-norm"));
        rejectNormDialog.findElement(By.tagName("textarea")).sendKeys(justification);
        rejectNormDialog.findElement(By.xpath("//button[contains(.,'Prześlij')]")).click();
        return new NormsPage(this.driver);
    }

    public NormsPage acceptNorm() {
        this.acceptButton.click();
        return new NormsPage(this.driver);
    }
}
