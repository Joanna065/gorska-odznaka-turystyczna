package pom.norms;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pom.core.Element;

public class NormCard extends Element {
    private WebElement title;
    private WebElement dates;

    //TODO
    /*private List<NormTripCard> normTripCards;*/
    private WebElement detailsButton;

    public NormCard(WebDriver driver, WebElement element) {
        super(driver, element);

        this.title = this.mainElement.findElement(By.tagName("mat-card-title"));
        this.dates = this.mainElement.findElement(By.className("dates"));
        /*this.normTripCards = this.mainElement.findElements(By.tagName("mat-card"))
                                     .stream()
                                     .map(normCardElement -> new NormTripCard(this.driver, normCardElement))
                                     .collect(Collectors.toList());*/

        this.detailsButton = this.findButtonByText("Szczegóły");
    }

    public String getTitle() {
        return title.getText();
    }

    public String getDates() {
        return dates.getText();
    }

    public NormPage viewNormDetails() {
        this.detailsButton.click();
        return new NormPage(this.driver);
    }

}
