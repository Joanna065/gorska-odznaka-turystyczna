package pom.norms;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pom.core.Page;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class NormsPage extends Page {
    private List<NormCard> normCards;

    public NormsPage(WebDriver driver) {
        super(driver);

        this.normCards = this.driver.findElements(By.tagName("app-norm-card"))
                                 .stream()
                                 .map(cardElement -> new NormCard(this.driver, cardElement))
                                 .collect(Collectors.toList());
    }

    public int getNumberOfNorms() {
        return normCards.size();
    }

    public NormPage viewNormWithTitle(String normTitle) {
        Optional<NormCard> result = this.normCards.stream()
                .filter(normCard -> normCard.getTitle().equals(normTitle))
                .findFirst();

        if(result.isPresent()) {
            return result.get().viewNormDetails();
        }
        else {
            throw new IllegalArgumentException("Norm with a given title couldn't be found");
        }
    }
}
