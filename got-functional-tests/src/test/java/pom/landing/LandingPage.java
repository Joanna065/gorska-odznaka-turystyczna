package pom.landing;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pom.core.Page;

public class LandingPage extends Page {
    private WebElement landingImage;

    public LandingPage(WebDriver driver) {
        super(driver);

        this.landingImage = this.driver.findElement(By.tagName("img"));
    }

    public boolean isImagePresent() {
        return this.landingImage.isDisplayed();
    }
}
