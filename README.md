# Górska Odznaka Turystyczna

University project about collecting points for GOT PTTK badges

Requirements: java 11

1. Clone repository and create local database from
mysql script `full script.sql` included in directory got-database.

2. Set properties in `application.properties`:
    spring.datasource.url=
    spring.datasource.username=
    spring.datasource.password=

3. To run application execute `mvn spring-boot:run` in directory got-back.
   Make sure earlier that your `JAVA_HOME` and `MAVEN_HOME` path are set 
   in environment variables. Remeber to use java 11 version.

4. To run tests execute `mvn test`.

5. After running app see API documentation go to 
   `http://localhost:8080/swagger-ui.html#/`.

6. Documentation to backend code is placed in folder javadoc. Open file `index.hmtl`.

7. To see JDepend raport go to `target/site` and open `index.html`. Click on 
   Project Reports -> JDepend.

8. To see SonarQube report go to 
   `https://docs.sonarqube.org/latest/setup/get-started-2-minutes/` and follow 
    quick installation guide. Create new project in opened SonarQube for this 
    project. 



